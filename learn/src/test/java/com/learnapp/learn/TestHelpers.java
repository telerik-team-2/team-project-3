package com.learnapp.learn;

import com.learnapp.learn.domains.access_types.AccessType;
import com.learnapp.learn.domains.account_types.AccountType;
import com.learnapp.learn.domains.account_types.interfaces.AccountTypeService;
import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.accounts.wrappers.AccountCreateParameters;
import com.learnapp.learn.domains.courses.Course;
import com.learnapp.learn.domains.courses.statuses.Status;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class TestHelpers {

    public static Account createAdminAccount(int id) {
        Account account = new Account();

        AccountType accountType = new AccountType();
        accountType.setAccountTypeId(AccountType.ADMIN_ID);

        Set<AccessType> accessTypeSet = new HashSet<>();
        AccessType adminAccessType = new AccessType();
        adminAccessType.setAccessId(AccessType.ADMIN_ACCESS_ID);
        accessTypeSet.add(adminAccessType);

        account.setAccountId(id);
        account.setEmail("test@mail.com");
        account.setPassword("pass");
        account.setAccountType(accountType);
        account.setAccessTypeSet(accessTypeSet);

        return account;
    }

    public static Account createStudentAccount(int id) {
        Account account = new Account();

        AccountType accountType = new AccountType();
        accountType.setAccountTypeId(AccountType.STUDENT_ID);

        Set<AccessType> accessTypeSet = new HashSet<>();
        AccessType adminAccessType = new AccessType();
        adminAccessType.setAccessId(AccessType.STUDENT_ACCESS_ID);
        accessTypeSet.add(adminAccessType);

        account.setAccountId(id);
        account.setEmail("test@mail.com");
        account.setPassword("pass");
        account.setAccountType(accountType);
        account.setAccessTypeSet(accessTypeSet);

        return account;
    }

    public static AccountType createStudentAccountType() {
        AccountType studentAccountType = new AccountType();
        studentAccountType.setAccountTypeId(AccountType.STUDENT_ID);
        studentAccountType.setName("student");
        return studentAccountType;
    }

    public static AccountCreateParameters createStudentAccountCreateParameters(AccountTypeService accountTypeService) {
        AccountCreateParameters accountCreateParameters = new AccountCreateParameters(accountTypeService);
        accountCreateParameters.setAccountTypeService(accountTypeService);
        accountCreateParameters.setAccountTypeId(AccountType.STUDENT_ID);
        accountCreateParameters.setEmail("test@mail.com");
        accountCreateParameters.setPassword("password");
        accountCreateParameters.setFirstName("John");
        accountCreateParameters.setLastName("Doe");
        accountCreateParameters.setPhoneNumber("00000");

        return accountCreateParameters;
    }

    public static Account addAccessToAccount(Account account, AccessType... accessTypes) {
        Set<AccessType> accessTypeSet = new HashSet<>(account.getAccessTypeSet());
        accessTypeSet.addAll(Arrays.asList(accessTypes));
        account.setAccessTypeSet(accessTypeSet);
        return account;
    }

    public static AccessType createAccess(int accessId) {
        AccessType accessType = new AccessType();
        accessType.setAccessId(accessId);
        return accessType;
    }

    public static Account createAccount() {
        Account account = new Account();
        account.setAccessTypeSet(new HashSet<>());
        return account;
    }

    public static Course createCourse() {
        return new Course();
    }

    public static Status createStatus(int statusId) {
        Status status = new Status();
        status.setStatusId(statusId);
        return status;
    }
}
