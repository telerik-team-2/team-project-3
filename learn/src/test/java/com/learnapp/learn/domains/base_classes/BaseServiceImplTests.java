package com.learnapp.learn.domains.base_classes;

import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.base_classes.interfaces.BaseRepository;
import com.learnapp.learn.domains.base_classes.interfaces.CreateParametersHolder;
import com.learnapp.learn.domains.base_classes.interfaces.UpdateParametersHolder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class BaseServiceImplTests {

    @Mock
    BaseRepository<Object> baseRepository;

    @InjectMocks
    BaseServiceImpl<Object> service;

    @Test
    public void createAuthorized_Should_CreateObject() {
        Account loggedUser = new Account();
        CreateParametersHolder<Object> createParametersHolder = createCreateParametersHolderWrapper();

        service.createAuthorized(loggedUser, createParametersHolder);
        Mockito.verify(baseRepository, Mockito.times(1))
                .create(Mockito.any(Object.class));
    }

    @Test
    public void create_Should_CreateObject() {
        CreateParametersHolder<Object> createParametersHolder = createCreateParametersHolderWrapper();

        service.create(createParametersHolder);
        Mockito.verify(baseRepository, Mockito.times(1))
                .create(Mockito.any(Object.class));
    }

    @Test
    public void updateAuthorized_Should_UpdateObject() {
        Account loggedUser = new Account();
        int objectId = 1;
        UpdateParametersHolder<Object> updateParametersHolder = createUpdateParametersHolderWrapper();

        Object object = new Object();

        Mockito.when(baseRepository.getById(objectId))
                .thenReturn(object);

        service.updateAuthorized(loggedUser, objectId, updateParametersHolder);
        Mockito.verify(baseRepository, Mockito.times(1))
                .update(Mockito.any(Object.class));
    }

    @Test
    public void update_Should_UpdateObject() {
        Object object = new Object();
        UpdateParametersHolder<Object> updateParametersHolder = createUpdateParametersHolderWrapper();

        service.update(object, updateParametersHolder);
        service.update(object);
        Mockito.verify(baseRepository, Mockito.times(2))
                .update(Mockito.any(Object.class));
    }

    @Test
    public void deleteAuthorized_Should_DeleteObject() {
        Account loggedUser = new Account();
        int objectId = 1;

        Object object = new Object();

        Mockito.when(baseRepository.getById(objectId))
                .thenReturn(object);

        service.deleteAuthorized(loggedUser, objectId);
        Mockito.verify(baseRepository, Mockito.times(1))
                .delete(Mockito.any(Object.class));
    }

    @Test
    public void delete_Should_DeleteObject() {
        Object object = new Object();

        service.delete(object);
        Mockito.verify(baseRepository, Mockito.times(1))
                .delete(Mockito.any(Object.class));
    }

    private CreateParametersHolder<Object> createCreateParametersHolderWrapper() {
        return Object::new;
    }

    private UpdateParametersHolder<Object> createUpdateParametersHolderWrapper() {
        return objectToUpdate -> {};
    }
}
