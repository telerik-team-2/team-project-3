package com.learnapp.learn.domains.accounts;

import com.learnapp.learn.TestHelpers;
import com.learnapp.learn.domains.access_types.AccessType;
import com.learnapp.learn.domains.account_types.interfaces.AccountTypeService;
import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.accounts.AccountServiceImpl;
import com.learnapp.learn.domains.accounts.interfaces.AccountRepository;
import com.learnapp.learn.domains.accounts.interfaces.AccountService;
import com.learnapp.learn.domains.file_management.FileManager;
import com.learnapp.learn.exceptions.EntityNotFoundException;
import com.learnapp.learn.exceptions.UnauthorizedAccessException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class AccountServiceTests {

    @Mock
    AccountRepository mockRepository;

    @Mock
    AccountTypeService mockAccountTypeService;

    @Mock
    AccountService mockAccountService;

    @InjectMocks
    AccountServiceImpl service;

    @Test
    public void getByEmail_Should_ReturnAccount_When_MatchExists() {
        //Arrange
        Account admin = TestHelpers.createAdminAccount(1);

        Mockito.when(mockRepository.getByEmail(admin.getEmail()))
                .thenReturn(admin);

        //Act
        Account result = service.getByEmail(admin.getEmail());

        //Assert
        Assertions.assertEquals(admin.getAccountId(), result.getAccountId());
        Assertions.assertEquals(admin.getEmail(), result.getEmail());
    }

    @Test
    public void getProfilePictureAuthorized_Should_GetPicture_When_Authorized() {
        //Arrange
        Account student = TestHelpers.createStudentAccount(1);


        //Act
        //Assert
        Assertions.assertThrows(UnauthorizedAccessException.class,
                () -> service.getProfilePictureAuthorized(student, 2));
    }

//    @Test
//    public void getProfilePictureAuthorized_Should_ThrowException_When_Unauthorized() {
//        //Arrange
//        Account student = TestHelpers.createStudentAccount(1);
//
//        //Act
//        //Assert
//        Mockito.verify(mockAccountService, Mockito.times(1))
//                .getProfilePictureAuthorized(student, 1);
//    }

//    @Test
//    public void create_Should_CreateAccount_When_ValidParameters() {
//        //Arrange
//        AccountCreateParameters accountCreateParameters = TestHelpers.createStudentAccountCreateParameters(mockAccountTypeService);
//        Mockito.when(mockAccountTypeService.getById(AccountType.STUDENT_ID))
//                .thenReturn(TestHelpers.createStudentAccountType());
//
//        //Act
//        //TODO: NullPointerException
//        service.create(accountCreateParameters);
//
//        //Assert
//        Mockito.verify(mockRepository, Mockito.times(1))
//                .create(Mockito.any(Account.class));
//    }

//    @Test
//    public void addAccessTypes_Should_AddAccess_When_ValidParams() {
//        //Arrange
//        Account adminAccount = TestHelpers.createAdminAccount(1);
//        AccessType accessType = new AccessType();
//        accessType.setAccessId(AccessType.TEACHER_ACCESS_ID);
//        accessType.setName("teacher");
//        List<Integer> accessTypesList = new ArrayList<>();
//        accessTypesList.add(accessType.getAccessId());
//
//        //Act
//        //TODO: NullPointerException
//        service.addAccessTypes(adminAccount, 1, accessTypesList);
//
//        //Assert
//        Mockito.verify(mockRepository, Mockito.times(1))
//                .update(adminAccount);
//    }

    @Test
    public void addAccessTypes_Should_ThrowException_When_Unauthorized() {
        //Arrange
        Account student = TestHelpers.createStudentAccount(1);
        AccessType accessType = new AccessType();
        accessType.setAccessId(AccessType.TEACHER_ACCESS_ID);
        accessType.setName("teacher");
        List<Integer> accessTypesList = new ArrayList<>();
        accessTypesList.add(accessType.getAccessId());

        //Act
        //Assert
        Assertions.assertThrows(UnauthorizedAccessException.class,
                () -> service.addAccessTypes(student, 1, accessTypesList));
    }

    @Test
    public void updatePassword_Should_UpdatePassword_When_ValidRequest() {
        //Arrange
        Account admin = TestHelpers.createAdminAccount(1);
        String newPassword = "newpass";

        Mockito.when(mockRepository.getById(admin.getAccountId()))
                .thenReturn(admin);

        //Act
        service.updatePassword(admin, 1, newPassword);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(admin);
        Assertions.assertEquals(admin.getPassword(), newPassword);

    }

    @Test
    public void updatePassword_Should_ThrowException_When_Unauthorized() {
        //Arrange
        Account student = TestHelpers.createStudentAccount(1);
        String newPassword = "newpass";

        //Act
        //Assert
        Assertions.assertThrows(UnauthorizedAccessException.class,
                () -> service.updatePassword(student, 2, newPassword));

    }

    @Test
    public void updatePassword_Should_ThrowException_When_AccountNotFound() {
        //Arrange
        Account admin = TestHelpers.createAdminAccount(1);
        String newPassword = "newpass";

        Mockito.when(mockRepository.getById(2))
                .thenThrow(EntityNotFoundException.class);

        //Act
        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.updatePassword(admin, 2, newPassword));
    }

    @Test
    public void updateProfilePictureAuthorized_Should_ThrowException_When_Unauthorized() {
        //Arrange
        Account student = TestHelpers.createStudentAccount(1);
        File picture = new File("temp");

        //Act
        //Assert
        Assertions.assertThrows(UnauthorizedAccessException.class,
                () -> service.updateProfilePictureAuthorized(student, 2, picture));
    }

//    @Test
//    public void deleteAccessTypes_Should_DeleteAccess_When_ValidParams() {
//        //Arrange
//        Account adminAccount = TestHelpers.createAdminAccount(1);
//        Account studentAccount = TestHelpers.createStudentAccount(2);
//        AccessType accessType = new AccessType();
//        accessType.setAccessId(AccessType.TEACHER_ACCESS_ID);
//        accessType.setName("teacher");
//        List<Integer> accessTypesList = new ArrayList<>();
//        accessTypesList.add(accessType.getAccessId());
//
//        //Act
//        //TODO: NullPointerException
//        service.deleteAccessTypes(adminAccount, 2, accessTypesList);
//
//        //Assert
//        Mockito.verify(mockRepository, Mockito.times(1))
//                .update(studentAccount);
//    }


    @Test
    public void deleteAccessTypes_Should_ThrowException_When_Unauthorized() {
        //Arrange
        Account adminAccount = TestHelpers.createAdminAccount(1);
        Account studentAccount = TestHelpers.createStudentAccount(2);
        List<Integer> accessTypesList = new ArrayList<>();

        //Act
        //Assert
        Assertions.assertThrows(UnauthorizedAccessException.class,
                () -> service.deleteAccessTypes(studentAccount, adminAccount.getAccountId(), accessTypesList));
    }
}
