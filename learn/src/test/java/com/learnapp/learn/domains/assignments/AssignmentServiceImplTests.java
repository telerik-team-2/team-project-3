package com.learnapp.learn.domains.assignments;

import com.learnapp.learn.TestHelpers;
import com.learnapp.learn.domains.access_types.AccessType;
import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.assignments.interfaces.AssignmentRepository;
import com.learnapp.learn.domains.base_classes.interfaces.BaseGetAllParameters;
import com.learnapp.learn.domains.file_management.FileManager;
import com.learnapp.learn.domains.lectures.Lecture;
import com.learnapp.learn.exceptions.UnauthorizedAccessException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class AssignmentServiceImplTests {

    @Mock
    AssignmentRepository assignmentRepository;

    @Mock
    FileManager fileManager;

    @InjectMocks
    AssignmentServiceImpl service;

    private static BaseGetAllParameters baseGetAllParameters;
    private static AccessType teacherAccess;

    @BeforeAll
    public static void setupParameters() {
        baseGetAllParameters = (queryParameters, parameterKeysValues, sortParams) -> {};
        teacherAccess = TestHelpers.createAccess(AccessType.TEACHER_ACCESS_ID);
    }

    @Test
    public void getByCourse_ReturnsAssignmentsWithFiles() {
        Account loggedUser = TestHelpers.createAccount();
        loggedUser.setAccountId(1);

        Assignment assignmentWithFile = new Assignment();
        Lecture lecture1 = new Lecture();
        lecture1.setLectureId(1);
        assignmentWithFile.setLecture(lecture1);
        assignmentWithFile.setOwnerAccount(loggedUser);

        Assignment assignmentWithoutFile = new Assignment();
        Lecture lecture2 = new Lecture();
        lecture2.setLectureId(2);
        assignmentWithoutFile.setLecture(lecture2);
        assignmentWithoutFile.setOwnerAccount(loggedUser);

        List<Assignment> assignmentList = new ArrayList<>();
        assignmentList.add(assignmentWithFile);
        assignmentList.add(assignmentWithoutFile);

        Mockito.when(assignmentRepository.getAll(Mockito.any()))
                .thenReturn(assignmentList);

        Mockito.when(fileManager.checkAssignmentExists(loggedUser.getAccountId(), assignmentWithFile.getLecture().getLectureId()))
                .thenReturn(true);

        Mockito.when(fileManager.checkAssignmentExists(loggedUser.getAccountId(), assignmentWithoutFile.getLecture().getLectureId()))
                .thenReturn(false);

        List<Assignment> result = service.getByCourse(loggedUser, 1);

        Assertions.assertEquals(result.size(), 1);
        Assertions.assertEquals(result.get(0), assignmentWithFile);
    }

    @Test
    public void getAllAuthorized_Returns_When_TeacherAccess() {
        Account loggedUser =
                TestHelpers.addAccessToAccount(
                        TestHelpers.createAccount(),
                        teacherAccess);

        List<Assignment> assignments = new ArrayList<>();

        Mockito.when(assignmentRepository.getAll())
                .thenReturn(assignments);

        Mockito.when(assignmentRepository.getAll(Mockito.any()))
                .thenReturn(assignments);

        Assertions.assertEquals(service.getAllAuthorized(loggedUser), assignments);
        Assertions.assertEquals(service.getAllAuthorized(loggedUser, baseGetAllParameters), assignments);

        Mockito.verify(assignmentRepository, Mockito.times(1))
                .getAll();

        Mockito.verify(assignmentRepository, Mockito.times(1))
                .getAll(Mockito.any(BaseGetAllParameters.class));
    }

    @Test
    public void getAllAuthorized_Throws_When_NoTeacherAccess() {
        Account loggedUser = TestHelpers.createAccount();

        Assertions.assertThrows(UnauthorizedAccessException.class, () -> service.getAllAuthorized(loggedUser));
        Assertions.assertThrows(UnauthorizedAccessException.class, () -> service.getAllAuthorized(loggedUser, baseGetAllParameters));

        Mockito.verify(assignmentRepository, Mockito.times(0))
                .getAll();

        Mockito.verify(assignmentRepository, Mockito.times(0))
                .getAll(Mockito.any(BaseGetAllParameters.class));
    }

    @Test
    public void getByLecture_ReturnsLoggedAccountAssignmentByLecture() {
        Account loggedUser = TestHelpers.createAccount();
        int accountId = 8750;
        loggedUser.setAccountId(accountId);
        int lectureId = 9856;

        Assignment assignment = new Assignment();

        Mockito.when(assignmentRepository.getByLecture(accountId, lectureId))
                .thenReturn(assignment);

        Assertions.assertEquals(assignment, service.getByLecture(loggedUser, lectureId));

        Mockito.verify(assignmentRepository, Mockito.times(1))
                .getByLecture(accountId, lectureId);
    }

    @Test
    public void getFileAuthorized_ReturnsFIle_When_Owner() {
        Account loggedUser = TestHelpers.createAccount();
        int accountId = 8750;
        loggedUser.setAccountId(accountId);

        Lecture lecture = new Lecture();
        int lectureId = 9856;
        lecture.setLectureId(lectureId);

        Assignment assignment = new Assignment();
        int assignmentId = 578;
        assignment.setOwnerAccount(loggedUser);
        assignment.setLecture(lecture);

        Mockito.when(assignmentRepository.getById(assignmentId))
                .thenReturn(assignment);

        service.getFileAuthorized(loggedUser, assignmentId);

        Mockito.verify(fileManager, Mockito.times(1))
                .getAssignment(accountId, lectureId);
    }

    @Test
    public void getFileAuthorized_ReturnsFIle_When_TeacherAccess() {
        Account loggedUser =
                TestHelpers.addAccessToAccount(
                        TestHelpers.createAccount(),
                        teacherAccess);
        int accountId = 8750;
        loggedUser.setAccountId(accountId);

        Lecture lecture = new Lecture();
        int lectureId = 9856;
        lecture.setLectureId(lectureId);

        Assignment assignment = new Assignment();
        int assignmentId = 578;
        Account ownerAccount = TestHelpers.createAccount();
        int ownerAccountId = 4328;
        ownerAccount.setAccountId(ownerAccountId);
        assignment.setOwnerAccount(ownerAccount);
        assignment.setLecture(lecture);

        Mockito.when(assignmentRepository.getById(assignmentId))
                .thenReturn(assignment);

        service.getFileAuthorized(loggedUser, assignmentId);

        Mockito.verify(fileManager, Mockito.times(1))
                .getAssignment(ownerAccountId, lectureId);
    }

    @Test
    public void getFileAuthorized_Throws_When_NoTeacherAccessAndNotOwner() {
        Account loggedUser = TestHelpers.createAccount();
        int accountId = 8750;
        loggedUser.setAccountId(accountId);

        Lecture lecture = new Lecture();
        int lectureId = 9856;
        lecture.setLectureId(lectureId);

        Assignment assignment = new Assignment();
        int assignmentId = 578;
        Account ownerAccount = TestHelpers.createAccount();
        int ownerAccountId = 4328;
        ownerAccount.setAccountId(ownerAccountId);
        assignment.setOwnerAccount(ownerAccount);
        assignment.setLecture(lecture);

        Mockito.when(assignmentRepository.getById(assignmentId))
                .thenReturn(assignment);

        Assertions.assertThrows(UnauthorizedAccessException.class, () -> service.getFileAuthorized(loggedUser, assignmentId));
    }

    @Test
    public void getFile_ReturnsFileForAssignment() {
        Account account = TestHelpers.createAccount();
        int accountId = 8750;
        account.setAccountId(accountId);

        Lecture lecture = new Lecture();
        int lectureId = 9856;
        lecture.setLectureId(lectureId);

        Assignment assignment = new Assignment();
        assignment.setOwnerAccount(account);
        assignment.setLecture(lecture);

        File assignmentFile = new File("./temp");

        Mockito.when(fileManager.getAssignment(accountId, lectureId))
                .thenReturn(assignmentFile);

        Assertions.assertEquals(assignmentFile, service.getFile(assignment));
    }

    @Test
    public void updateFileAuthorized_UpdatesFile_When_OwnerAndNotGraded() {
        Account loggedUser = TestHelpers.createAccount();
        int accountId = 8750;
        loggedUser.setAccountId(accountId);

        Lecture lecture = new Lecture();
        int lectureId = 9856;
        lecture.setLectureId(lectureId);

        Assignment assignment = new Assignment();
        int assignmentId = 139875;
        assignment.setAssignmentId(assignmentId);
        assignment.setOwnerAccount(loggedUser);
        assignment.setLecture(lecture);

        File assignmentFile = new File("./temp");

        Mockito.when(assignmentRepository.getById(assignmentId))
                .thenReturn(assignment);

        service.updateFileAuthorized(loggedUser, assignmentId, assignmentFile);
    }

    @Test
    public void updateFileAuthorized_UpdatesFile_When_TeacherAndNotGraded() {
        Account loggedUser =
                TestHelpers.addAccessToAccount(
                        TestHelpers.createAccount(),
                        teacherAccess);
        int accountId = 8750;
        loggedUser.setAccountId(accountId);

        Lecture lecture = new Lecture();
        int lectureId = 9856;
        lecture.setLectureId(lectureId);

        Assignment assignment = new Assignment();
        int assignmentId = 139875;
        assignment.setAssignmentId(assignmentId);
        Account ownerAccount = TestHelpers.createAccount();
        ownerAccount.setAccountId(4328);
        assignment.setOwnerAccount(ownerAccount);
        assignment.setLecture(lecture);

        File assignmentFile = new File("./temp");

        Mockito.when(assignmentRepository.getById(assignmentId))
                .thenReturn(assignment);

        service.updateFileAuthorized(loggedUser, assignmentId, assignmentFile);
    }

    @Test
    public void updateFileAuthorized_Throws_When_NotOwnerAndNoTeacherAccess() {
        Account loggedUser = TestHelpers.createAccount();
        int accountId = 8750;
        loggedUser.setAccountId(accountId);

        Lecture lecture = new Lecture();
        int lectureId = 9856;
        lecture.setLectureId(lectureId);

        Assignment assignment = new Assignment();
        int assignmentId = 139875;
        assignment.setAssignmentId(assignmentId);
        Account ownerAccount = TestHelpers.createAccount();
        ownerAccount.setAccountId(4328);
        assignment.setOwnerAccount(ownerAccount);
        assignment.setLecture(lecture);

        File assignmentFile = new File("./temp");

        Mockito.when(assignmentRepository.getById(assignmentId))
                .thenReturn(assignment);

        Assertions.assertThrows(UnauthorizedAccessException.class, () -> service.updateFileAuthorized(loggedUser, assignmentId, assignmentFile));
    }

    @Test
    public void updateFileAuthorized_Throws_When_Graded() {
        Account loggedUser = TestHelpers.createAccount();
        int accountId = 8750;
        loggedUser.setAccountId(accountId);

        Lecture lecture = new Lecture();
        int lectureId = 9856;
        lecture.setLectureId(lectureId);

        Assignment assignment = new Assignment();
        int assignmentId = 139875;
        assignment.setAssignmentId(assignmentId);
        assignment.setOwnerAccount(loggedUser);
        assignment.setLecture(lecture);
        assignment.setGrade(89074);

        File assignmentFile = new File("./temp");

        Mockito.when(assignmentRepository.getById(assignmentId))
                .thenReturn(assignment);

        Assertions.assertThrows(IllegalArgumentException.class, () -> service.updateFileAuthorized(loggedUser, assignmentId, assignmentFile));
    }

    @Test
    public void updateGradeAuthorized_UpdatesGrade_When_TeacherAccess() {
        Account loggedUser =
                TestHelpers.addAccessToAccount(
                        TestHelpers.createAccount(),
                        teacherAccess);
        int accountId = 87;
        loggedUser.setAccountId(accountId);

        Assignment assignment = new Assignment();
        int assignmentId = 13;
        assignment.setAssignmentId(assignmentId);

        Lecture assignmentLecture = new Lecture();
        int lectureMaxGrade = 30;
        assignmentLecture.setMaxGrade(lectureMaxGrade);
        assignment.setLecture(assignmentLecture);

        Mockito.when(assignmentRepository.getById(assignmentId))
                .thenReturn(assignment);

        int newGrade = 24;
        Assertions.assertNull(assignment.getGrade());
        service.updateGradeAuthorized(loggedUser, assignmentId, newGrade);
        Assertions.assertEquals(assignment.getGrade(), newGrade);

        Mockito.verify(assignmentRepository, Mockito.times(1))
                .update(assignment);
    }

    @Test
    public void updateGradeAuthorized_UpdatesGradeToLectureMax_When_HigherGradeProvided() {
        Account loggedUser =
                TestHelpers.addAccessToAccount(
                        TestHelpers.createAccount(),
                        teacherAccess);
        int accountId = 87;
        loggedUser.setAccountId(accountId);

        Assignment assignment = new Assignment();
        int assignmentId = 13;
        assignment.setAssignmentId(assignmentId);

        Lecture assignmentLecture = new Lecture();
        int lectureMaxGrade = 10;
        assignmentLecture.setMaxGrade(lectureMaxGrade);
        assignment.setLecture(assignmentLecture);

        Mockito.when(assignmentRepository.getById(assignmentId))
                .thenReturn(assignment);

        int newGrade = 24;
        Assertions.assertTrue(assignment.getLecture().getMaxGrade() < newGrade);
        service.updateGradeAuthorized(loggedUser, assignmentId, newGrade);
        Assertions.assertEquals(assignment.getGrade(), lectureMaxGrade);
    }

    @Test
    public void updateGradeAuthorized_Throws_When_NoTeacherAccess() {
        Account loggedUser = TestHelpers.createAccount();
        int accountId = 87;
        loggedUser.setAccountId(accountId);

        Assignment assignment = new Assignment();
        int assignmentId = 13;
        assignment.setAssignmentId(assignmentId);
        assignment.setOwnerAccount(loggedUser);

        int newGrade = 24;
        Assertions.assertNull(assignment.getGrade());
        Assertions.assertThrows(UnauthorizedAccessException.class, () -> service.updateGradeAuthorized(loggedUser, assignmentId, newGrade));
        Assertions.assertNull(assignment.getGrade());
        Mockito.verify(assignmentRepository, Mockito.times(0))
                .update(assignment);
    }
}
