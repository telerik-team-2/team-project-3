package com.learnapp.learn.domains.courses;

import com.learnapp.learn.TestHelpers;
import com.learnapp.learn.domains.access_types.AccessType;
import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.accounts.interfaces.AccountService;
import com.learnapp.learn.domains.base_classes.interfaces.CreateParametersHolder;
import com.learnapp.learn.domains.base_classes.interfaces.UpdateParametersHolder;
import com.learnapp.learn.domains.courses.interfaces.CourseRepository;
import com.learnapp.learn.domains.courses.statuses.Status;
import com.learnapp.learn.domains.courses.statuses.interfaces.StatusService;
import com.learnapp.learn.domains.courses.topics.interfaces.TopicService;
import com.learnapp.learn.domains.courses.wrappers.CourseCreateParameters;
import com.learnapp.learn.domains.courses.wrappers.CourseGetAllParameters;
import com.learnapp.learn.exceptions.UnauthorizedAccessException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CourseServiceImplTests {

    @Mock
    CourseRepository courseRepository;

    @Mock
    AccountService accountService;

    @Mock
    StatusService statusService;

    @Mock
    TopicService topicService;

    @InjectMocks
    CourseServiceImpl service;

    @Test
    public void getAllAuthorized_ReturnsPublishedCourses_When_NoTeacherAccess() {
        Account loggedUser = TestHelpers.createAccount();
        CourseGetAllParameters courseGetAllParameters = new CourseGetAllParameters(accountService);

        Mockito.when(courseRepository.getAll(courseGetAllParameters))
                .thenReturn(null);

        Assertions.assertNull(courseGetAllParameters.getStatusId());

        service.getAllAuthorized(loggedUser, courseGetAllParameters);

        Mockito.verify(courseRepository, Mockito.times(1))
                .getAll(courseGetAllParameters);

        Assertions.assertEquals(courseGetAllParameters.getStatusId(), Status.PUBLISHED_COURSE_STATUS_ID);
    }

    @Test
    public void getAllAuthorized_ReturnsCoursesWithAnyStatus_When_TeacherAccess() {
        AccessType teacherAccess = TestHelpers.createAccess(AccessType.TEACHER_ACCESS_ID);
        Account loggedUser =
                TestHelpers.addAccessToAccount(
                        TestHelpers.createAccount(),
                        teacherAccess);
        CourseGetAllParameters courseGetAllParameters = new CourseGetAllParameters(accountService);

        Mockito.when(courseRepository.getAll(courseGetAllParameters))
                .thenReturn(null);

        Assertions.assertNull(courseGetAllParameters.getStatusId());

        service.getAllAuthorized(loggedUser, courseGetAllParameters);

        Mockito.verify(courseRepository, Mockito.times(1))
                .getAll(courseGetAllParameters);

        Assertions.assertNull(courseGetAllParameters.getStatusId());
    }

    @Test
    public void getById_Returns_When_StatusPublished() {
        Account loggedUser = TestHelpers.createAccount();
        int id = 1;
        Course course = TestHelpers.createCourse();
        course.setStatus(TestHelpers.createStatus(Status.PUBLISHED_COURSE_STATUS_ID));

        Mockito.when(courseRepository.getById(id))
                .thenReturn(course);

        Assertions.assertDoesNotThrow(
                () -> service.getByIdAuthorized(loggedUser, id));

        Mockito.verify(courseRepository, Mockito.times(1))
                .getById(id);
    }

    @Test
    public void getById_Returns_When_StatusNotPublishedAndTeacherAccess() {
        AccessType teacherAccess = TestHelpers.createAccess(AccessType.TEACHER_ACCESS_ID);
        Account loggedUser =
                TestHelpers.addAccessToAccount(
                        TestHelpers.createAccount(),
                        teacherAccess);
        int id = 1;
        Status draftStatus = TestHelpers.createStatus(Status.DRAFT_COURSE_STATUS_ID);
        Course course = TestHelpers.createCourse();
        course.setStatus(draftStatus);

        Mockito.when(courseRepository.getById(id))
                .thenReturn(course);

        Assertions.assertEquals(course.getStatus(), draftStatus);
        Assertions.assertTrue(loggedUser.getAccessTypeSet().contains(teacherAccess));
        Assertions.assertDoesNotThrow(
                () -> service.getByIdAuthorized(loggedUser, id));

        Mockito.verify(courseRepository, Mockito.times(1))
                .getById(id);
    }

    @Test
    public void getById_Throws_When_StatusNotPublishedAndNoTeacherAccess() {
        AccessType teacherAccess = TestHelpers.createAccess(AccessType.TEACHER_ACCESS_ID);
        Account loggedUser = TestHelpers.createAccount();
        int id = 1;
        Status draftStatus = TestHelpers.createStatus(Status.DRAFT_COURSE_STATUS_ID);
        Course course = TestHelpers.createCourse();
        course.setStatus(draftStatus);

        Mockito.when(courseRepository.getById(id))
                .thenReturn(course);

        Assertions.assertFalse(loggedUser.getAccessTypeSet().contains(teacherAccess));
        Assertions.assertThrows(UnauthorizedAccessException.class,
                () -> service.getByIdAuthorized(loggedUser, id));

        Mockito.verify(courseRepository, Mockito.times(1))
                .getById(id);
    }

    @Test
    public void createAuthorized_Creates_When_TeacherAccess() {
        Account loggedUser = TestHelpers.addAccessToAccount(
                TestHelpers.createAccount(),
                TestHelpers.createAccess(AccessType.TEACHER_ACCESS_ID));
        Course course = new Course();
        CreateParametersHolder<Course> courseCreateParameters = () -> course;
        Status draftStatus = new Status();
        draftStatus.setStatusId(Status.DRAFT_COURSE_STATUS_ID);

        Mockito.when(statusService.getById(Status.DRAFT_COURSE_STATUS_ID))
                .thenReturn(draftStatus);

        service.createAuthorized(loggedUser, courseCreateParameters);

        Mockito.verify(courseRepository, Mockito.times(1))
                .create(Mockito.any(Course.class));

        Assertions.assertEquals(course.getStatus().getStatusId(), Status.DRAFT_COURSE_STATUS_ID);
    }

    @Test
    public void createAuthorized_Throws_When_NoTeacherAccess() {
        Account loggedUser = TestHelpers.createAccount();
        CourseCreateParameters courseCreateParameters = new CourseCreateParameters(topicService, accountService);

        Assertions.assertThrows(UnauthorizedAccessException.class, () -> service.createAuthorized(loggedUser, courseCreateParameters));

        Mockito.verify(courseRepository, Mockito.times(0))
                .create(Mockito.any(Course.class));
    }

    @Test
    public void updateAuthorized_UpdatesCourse_When_TeacherAccess() {
        Account loggedUser = TestHelpers.addAccessToAccount(
                TestHelpers.createAccount(),
                TestHelpers.createAccess(AccessType.TEACHER_ACCESS_ID));
        int genericId = 1;
        UpdateParametersHolder<Course> courseUpdateParameters = (course) -> {};

        Mockito.when(courseRepository.getById(genericId))
                .thenReturn(new Course());

        service.updateAuthorized(loggedUser, genericId, courseUpdateParameters);

        Mockito.verify(courseRepository, Mockito.times(1))
                .update(Mockito.any(Course.class));
    }

    @Test
    public void updateAuthorized_Throws_When_NoTeacherAccess() {
        Account loggedUser = TestHelpers.createAccount();
        int courseId = 1;


        Mockito.when(courseRepository.getById(courseId))
                        .thenReturn(new Course());

        Assertions.assertThrows(UnauthorizedAccessException.class, () -> service.updateAuthorized(loggedUser, courseId, (course) -> {}));

        Mockito.verify(courseRepository, Mockito.times(0))
                .update(Mockito.any(Course.class));
    }

    @Test
    public void deleteAuthorized_DeletesCourse_When_TeacherAccess() {
        Account loggedUser =
                TestHelpers.addAccessToAccount(
                        TestHelpers.createAccount(),
                        TestHelpers.createAccess(AccessType.TEACHER_ACCESS_ID));
        int courseId = 1;

        Mockito.when(courseRepository.getById(courseId))
                .thenReturn(new Course());

        service.deleteAuthorized(loggedUser, courseId);

        Mockito.verify(courseRepository, Mockito.times(1))
                .delete(Mockito.any(Course.class));
    }

    @Test
    public void deleteAuthorized_Throws_When_NoTeacherAccess() {
        Account loggedUser = TestHelpers.createAccount();
        int courseId = 1;

        Mockito.when(courseRepository.getById(courseId))
                .thenReturn(new Course());

        Assertions.assertThrows(UnauthorizedAccessException.class, () -> service.deleteAuthorized(loggedUser, courseId));

        Mockito.verify(courseRepository, Mockito.times(0))
                .delete(Mockito.any(Course.class));
    }
}
