package com.learnapp.learn.domains.courses.course_ratings;

import com.learnapp.learn.TestHelpers;
import com.learnapp.learn.domains.access_types.AccessType;
import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.accounts.interfaces.AccountService;
import com.learnapp.learn.domains.assignments.Assignment;
import com.learnapp.learn.domains.assignments.interfaces.AssignmentService;
import com.learnapp.learn.domains.courses.Course;
import com.learnapp.learn.domains.courses.course_ratings.interfaces.CourseRatingRepository;
import com.learnapp.learn.domains.courses.course_ratings.wrappers.CourseRatingCreateParameters;
import com.learnapp.learn.domains.courses.interfaces.CourseService;
import com.learnapp.learn.domains.courses.ratings.Rating;
import com.learnapp.learn.domains.courses.ratings.interfaces.RatingService;
import com.learnapp.learn.domains.lectures.Lecture;
import com.learnapp.learn.exceptions.DuplicateEntityException;
import com.learnapp.learn.exceptions.UnauthorizedAccessException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@ExtendWith(MockitoExtension.class)
public class CourseRatingServiceImplTests {

    @Mock
    AccountService accountService;

    @Mock
    AssignmentService assignmentService;

    @Mock
    CourseService courseService;

    @Mock
    RatingService ratingService;

    @Mock
    CourseRatingRepository courseRatingRepository;

    @InjectMocks
    CourseRatingServiceImpl service;

    private static Account student;
    private static Account teacher;

    private static AccessType studentAccess;
    private static AccessType teacherAccess;

    private static Set<Lecture> lectureSet;

    @BeforeAll
    public static void setupParameters() {
        studentAccess = TestHelpers.createAccess(AccessType.STUDENT_ACCESS_ID);
        teacherAccess = TestHelpers.createAccess(AccessType.TEACHER_ACCESS_ID);
        student = TestHelpers.addAccessToAccount(
                TestHelpers.createAccount(),
                studentAccess);
        student.setAccountId(1);
        teacher = TestHelpers.addAccessToAccount(
                TestHelpers.createAccount(),
                studentAccess,
                teacherAccess);
        teacher.setAccountId(2);

        lectureSet = new HashSet<>();
        for (int i = 1; i < 10; i++) {
            Lecture lecture = new Lecture();
            lecture.setLectureId(i);
            lectureSet.add(lecture);
        }
    }

    @Test
    public void createAuthorized_Throws_When_InsufficientAssignmentCount() {
        CourseRatingCreateParameters courseRatingCreateParameters =
                new CourseRatingCreateParameters(courseService, accountService, ratingService);
        Account loggedUser = student;
        Course course = new Course();
        int courseId = 10;
        course.setCourseId(courseId);
        course.setLectures(new HashSet<>(lectureSet));
        courseRatingCreateParameters.setCourseId(courseId);

        List<Assignment> assignments = createAssignmentsList();
        assignments.remove(assignments.size() - 1);

        Mockito.when(assignmentService.getByCourse(loggedUser, courseId))
                .thenReturn(assignments);

        Mockito.when(courseService.getByIdAuthorized(loggedUser, courseId))
                .thenReturn(course);

        Assertions.assertThrows(UnauthorizedAccessException.class, () -> service.createAuthorized(loggedUser, courseRatingCreateParameters));
    }

    @Test
    public void createAuthorized_Throws_When_InsufficientGrade() {
        CourseRatingCreateParameters courseRatingCreateParameters =
                new CourseRatingCreateParameters(courseService, accountService, ratingService);
        Account loggedUser = student;
        Course course = new Course();
        int courseId = 10;
        course.setCourseId(courseId);
        course.setLectures(new HashSet<>(lectureSet));
        courseRatingCreateParameters.setCourseId(courseId);

        List<Assignment> assignments = createAssignmentsList();
        int courseGrade = 1;
        for (Assignment assignment : assignments) {
            int assignmentGrade = 2;
            assignment.setGrade(assignmentGrade);
            courseGrade += assignmentGrade;
        }
        course.setPassingGrade(courseGrade);

        Mockito.when(assignmentService.getByCourse(loggedUser, courseId))
                .thenReturn(assignments);

        Mockito.when(courseService.getByIdAuthorized(loggedUser, courseId))
                .thenReturn(course);

        Assertions.assertThrows(UnauthorizedAccessException.class, () -> service.createAuthorized(loggedUser, courseRatingCreateParameters));
    }

    @Test
    public void createAuthorized_Throws_When_AssignmentNotGraded() {
        CourseRatingCreateParameters courseRatingCreateParameters =
                new CourseRatingCreateParameters(courseService, accountService, ratingService);
        Account loggedUser = student;
        Course course = new Course();
        int courseId = 10;
        course.setCourseId(courseId);
        course.setLectures(new HashSet<>(lectureSet));
        courseRatingCreateParameters.setCourseId(courseId);

        course.setPassingGrade(40);
        List<Assignment> assignments = createAssignmentsList();

        Mockito.when(assignmentService.getByCourse(loggedUser, courseId))
                .thenReturn(assignments);

        Mockito.when(courseService.getByIdAuthorized(loggedUser, courseId))
                .thenReturn(course);

        Assertions.assertThrows(UnauthorizedAccessException.class, () -> service.createAuthorized(loggedUser, courseRatingCreateParameters));
    }

    @Test
    public void createAuthorized_Throws_When_RatingExists() {
        CourseRatingCreateParameters courseRatingCreateParameters =
                new CourseRatingCreateParameters(courseService, accountService, ratingService);
        Account loggedUser = student;
        Course course = new Course();
        int courseId = 10;
        course.setCourseId(courseId);
        course.setLectures(new HashSet<>(lectureSet));
        courseRatingCreateParameters.setCourseId(courseId);

        course.setPassingGrade(1);
        List<Assignment> assignments = createAssignmentsList();
        for (Assignment assignment : assignments) {
            assignment.setGrade(1);
        }

        CourseRating courseRating = new CourseRating();
        courseRating.setAccount(loggedUser);
        Set<CourseRating> courseRatingSet = new HashSet<>();
        courseRatingSet.add(courseRating);
        course.setCourseRatings(courseRatingSet);

        Mockito.when(assignmentService.getByCourse(loggedUser, courseId))
                .thenReturn(assignments);

        Mockito.when(courseService.getByIdAuthorized(loggedUser, courseId))
                .thenReturn(course);

        Assertions.assertThrows(DuplicateEntityException.class, () -> service.createAuthorized(loggedUser, courseRatingCreateParameters));
    }

    @Test
    public void createAuthorized_CreatesRating_When_RatingNotExistsAndCoursePassed() {
        CourseRatingCreateParameters courseRatingCreateParameters =
                new CourseRatingCreateParameters(courseService, accountService, ratingService);
        Account loggedUser = student;
        Course course = new Course();
        int courseId = 10;
        course.setCourseId(courseId);
        course.setLectures(new HashSet<>(lectureSet));
        courseRatingCreateParameters.setCourseId(courseId);

        course.setPassingGrade(1);
        List<Assignment> assignments = createAssignmentsList();
        for (Assignment assignment : assignments) {
            assignment.setGrade(1);
        }
        course.setCourseRatings(new HashSet<>());

        Mockito.when(assignmentService.getByCourse(loggedUser, courseId))
                .thenReturn(assignments);

        Mockito.when(courseService.getByIdAuthorized(loggedUser, courseId))
                .thenReturn(course);

        Mockito.when(accountService.getById(loggedUser.getAccountId()))
                .thenReturn(loggedUser);

        Rating rating = new Rating();
        rating.setRatingId(Rating.GOOD);
        Mockito.when(ratingService.getById(rating.getRatingId()))
                .thenReturn(rating);
        courseRatingCreateParameters.setAccountId(loggedUser.getAccountId());
        courseRatingCreateParameters.setRatingId(rating.getRatingId());

        service.createAuthorized(loggedUser, courseRatingCreateParameters);

        Mockito.verify(courseRatingRepository, Mockito.times(1))
                .create(Mockito.any(CourseRating.class));
    }

    private List<Assignment> createAssignmentsList() {
        List<Assignment> assignmentList = new LinkedList<>();
        for (int i = 1; i < 10; i++) {
            Assignment assignment = new Assignment();
            assignment.setAssignmentId(i);
            assignmentList.add(assignment);
        }
        return assignmentList;
    }
}
