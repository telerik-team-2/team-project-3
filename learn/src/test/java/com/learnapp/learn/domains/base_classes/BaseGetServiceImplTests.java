package com.learnapp.learn.domains.base_classes;

import java.util.ArrayList;
import java.util.List;

import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.base_classes.interfaces.BaseGetAllParameters;
import com.learnapp.learn.domains.base_classes.interfaces.BaseGetRepository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class BaseGetServiceImplTests {
    
    @Mock
    BaseGetRepository<Object> baseGetRepository;

    @InjectMocks
    BaseGetServiceImpl<Object> service;

    @Test
    public void getAllAuthorized_Should_ReturnList() {
        Account loggedUser = new Account();
        List<Object> objects = makeObjectsList();
        BaseGetAllParameters baseGetAllParameters = createBaseGetAllParametersWrapper();

        Mockito.when(baseGetRepository.getAll())
                .thenReturn(objects);
        Mockito.when(baseGetRepository.getAll(baseGetAllParameters))
                .thenReturn(objects);

        Assertions.assertEquals(objects, service.getAllAuthorized(loggedUser));
        Assertions.assertEquals(objects, service.getAllAuthorized(loggedUser, baseGetAllParameters));
    }

    @Test
    public void getAll_Should_ReturnList() {
        List<Object> objects = makeObjectsList();
        BaseGetAllParameters baseGetAllParameters = createBaseGetAllParametersWrapper();

        Mockito.when(baseGetRepository.getAll())
                .thenReturn(objects);
        Mockito.when(baseGetRepository.getAll(baseGetAllParameters))
                .thenReturn(objects);

        Assertions.assertEquals(objects, service.getAll());
        Assertions.assertEquals(objects, service.getAll(baseGetAllParameters));
    }

    @Test
    public void getByIdAuthorized_Should_ReturnObject() {
        Account loggedUser = new Account();
        Object object = new Object();
        int id = 1;

        Mockito.when(baseGetRepository.getById(id))
                .thenReturn(object);

        Assertions.assertEquals(object, service.getByIdAuthorized(loggedUser, id));
    }

    @Test
    public void getById_Should_ReturnObject() {
        Object object = new Object();
        int id = 1;

        Mockito.when(baseGetRepository.getById(id))
                .thenReturn(object);

        Assertions.assertEquals(object, service.getById(id));
    }

    private List<Object> makeObjectsList() {
        List<Object> objects = new ArrayList<>();
        objects.add(new Object());
        objects.add(new Object());
        objects.add(new Object());
        objects.add(new Object());
        objects.add(new Object());
        return objects;
    }

    private BaseGetAllParameters createBaseGetAllParametersWrapper() {
        return (queryParameters, parameterKeysValues, sortParams) -> {};
    }
}
