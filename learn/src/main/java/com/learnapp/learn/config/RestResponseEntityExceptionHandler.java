package com.learnapp.learn.config;

import com.learnapp.learn.exceptions.DuplicateEntityException;
import com.learnapp.learn.exceptions.EntityNotFoundException;
import com.learnapp.learn.exceptions.InvalidLoginException;
import com.learnapp.learn.exceptions.UnauthorizedAccessException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = { UnauthorizedAccessException.class, InvalidLoginException.class })
    private ResponseEntity<Object> unauthorizedAccess(RuntimeException e) {
        return new ResponseEntity<>(e.getMessage(), new HttpHeaders(), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(value = EntityNotFoundException.class)
    private ResponseEntity<Object> entityNotFound(RuntimeException e) {
        return new ResponseEntity<>(e.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = { DuplicateEntityException.class, IllegalArgumentException.class })
    private ResponseEntity<Object> duplicateEntity(RuntimeException e) {
        return new ResponseEntity<>(e.getMessage(), new HttpHeaders(), HttpStatus.CONFLICT);
    }
}
