package com.learnapp.learn.config;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
public class SpringBeansConfig {

    private final String emailHost;
    private final String emailUsername;
    private final String emailPassword;

    @Autowired
    public SpringBeansConfig(Environment environment) {
        emailHost = environment.getProperty("email.host");
        emailUsername = environment.getProperty("email.username");
        emailPassword = environment.getProperty("email.password");
    }

    @Bean
    public JavaMailSender mailSender() {
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();

        javaMailSender.setHost(emailHost);
        javaMailSender.setPort(587);

        javaMailSender.setUsername(emailUsername);
        javaMailSender.setPassword(emailPassword);

        Properties props = javaMailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        return javaMailSender;
    }
}
