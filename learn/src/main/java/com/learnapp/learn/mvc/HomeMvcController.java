package com.learnapp.learn.mvc;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import com.learnapp.learn.domains.AuthenticationHelpers;
import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.accounts.interfaces.AccountService;
import com.learnapp.learn.domains.verification_tokens.VerificationToken;
import com.learnapp.learn.domains.verification_tokens.interfaces.VerificationTokenService;
import com.learnapp.learn.exceptions.EntityNotFoundException;
import com.learnapp.learn.exceptions.InvalidLoginException;
import com.learnapp.learn.exceptions.UnauthorizedAccessException;
import com.learnapp.learn.mvc.dtos.LoginDto;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;

@Controller
@RequestMapping("/")
public class HomeMvcController extends BaseMvcController {

    private final AccountService accountService;
    private final VerificationTokenService verificationTokenService;
    private final AuthenticationHelpers authenticationHelpers;

    public HomeMvcController(AccountService accountService,
                             AuthenticationHelpers authenticationHelpers,
                             VerificationTokenService verificationTokenService) {
        super(accountService);
        this.accountService = accountService;
        this.authenticationHelpers = authenticationHelpers;
        this.verificationTokenService = verificationTokenService;
    }

    @GetMapping
    public String getIndex() {
        return "index";
    }

    @GetMapping("login")
    public String getLogin(Model model) {
        setupLogin(model);
        return "login";
    }

    @GetMapping("logout")
    public String logout(HttpSession session) {
        session.removeAttribute("loggedUserId");
        return "redirect:/";
    }

    @PostMapping("login")
    public String login(HttpSession session,
                        Model model,
                        @Valid @ModelAttribute("loginDto") LoginDto loginDto,
                        BindingResult errors) {
        setupLogin(model);
        try {
            Account loggedUser = 
                    authenticationHelpers.tryGetUser(loginDto.getEmailAddress(), authenticationHelpers.hashPassword(loginDto.getPassword()));
            session.setAttribute("loggedUserId", loggedUser.getAccountId());
            return "redirect:/";
        } catch (InvalidLoginException|UnauthorizedAccessException|EntityNotFoundException e) {
            model.addAttribute("loginError", e.getMessage());
            return "login";
        }
    }

    @GetMapping("/registrationConfirm")
    public String confirmRegistration (WebRequest webRequest,
                                       Model model,
                                       @RequestParam(name = "token") String token) {
        VerificationToken verificationToken;

        try {
            verificationToken = verificationTokenService.getByValue(token);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", "invalidToken");
            model.addAttribute("message", e.getMessage());
            return "verification";
        }

        try {
            accountService.verify(verificationToken);
        } catch (IllegalArgumentException e) {
            model.addAttribute("error", "invalidToken");
            model.addAttribute("message", e.getMessage());
            return "verification";
        }

        model.addAttribute("message", "Verification successful");

        return "verification";
    }

    private void setupLogin(Model model) {
        model.addAttribute("loginDto", new LoginDto());
    }
}
