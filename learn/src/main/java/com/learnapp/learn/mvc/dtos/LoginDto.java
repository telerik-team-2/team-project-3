package com.learnapp.learn.mvc.dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

public class LoginDto {
    
    @Email
    @NotNull
    private String emailAddress;

    @NotNull
    private String password;

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
