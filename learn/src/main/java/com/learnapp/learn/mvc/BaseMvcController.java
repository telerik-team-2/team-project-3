package com.learnapp.learn.mvc;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpSession;

import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.accounts.interfaces.AccountService;
import com.learnapp.learn.exceptions.EntityNotFoundException;

public class BaseMvcController {

    public static final String UNAUTHENTICATED_ACCESS_REDIRECT = "redirect:/";
    public static final String UNAUTHORIZED_ACCESS_REDIRECT = "redirect:/";

    protected final AccountService accountService;

    public BaseMvcController(AccountService accountService) {
        this.accountService = accountService;
    }

    @ModelAttribute("loggedUser")
    public Account getLoggedUser(Model model, HttpSession session) {
        model.addAttribute("hasLoggedUser", session.getAttribute("loggedUserId") != null);
        if (session.getAttribute("loggedUserId") == null) {
            return null;
        }
        return getSessionUser(session);
    }

    public boolean checkHasLoggedUser(Model model) {
        return getLoggedUser(model) != null;
    }

    public Account getLoggedUser(Model model) {
        return (Account) model.getAttribute("loggedUser");
    }

    private Account getSessionUser(HttpSession session) {
        Integer loggedUserId = getLoggedUserId(session);
        try {
            return accountService.getById(loggedUserId);
        } catch (EntityNotFoundException e) {
            session.removeAttribute("loggedUserId");
            return null;
        }
    }

    private Integer getLoggedUserId(HttpSession session) {
        return (Integer) session.getAttribute("loggedUserId");
    }
}
