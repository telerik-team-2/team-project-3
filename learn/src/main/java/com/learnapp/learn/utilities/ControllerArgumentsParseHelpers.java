package com.learnapp.learn.utilities;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ControllerArgumentsParseHelpers {

    public static final String INVALID_INTEGER_PROVIDED = "Provided value %s is not a valid integer.";

    public static int getInteger(String numberString) {
        char[] numberStringArray = numberString.toCharArray();
        for (char digit : numberStringArray) {
            if (digit < 48 || digit > 57) {
                throw new IllegalArgumentException(String.format(INVALID_INTEGER_PROVIDED, numberString));
            }
        }
        return Integer.parseInt(numberString);
    }
    //?sortBy=parameters1.asc,parameter2.desc
    public static List<String[]> parseSortParams(String sortParams) {
        return Arrays.stream(sortParams.split(","))
                .map(parameterPair -> parameterPair.split("\\."))
                .filter(parameterPairArr -> parameterPairArr.length > 1)
                .collect(Collectors.toList());
    }
}
