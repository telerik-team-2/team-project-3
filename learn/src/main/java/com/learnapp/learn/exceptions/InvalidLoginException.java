package com.learnapp.learn.exceptions;

public class InvalidLoginException extends RuntimeException{

    public InvalidLoginException(String message) {
        super(message);
    }

    @Override
    public void printStackTrace() {}
}
