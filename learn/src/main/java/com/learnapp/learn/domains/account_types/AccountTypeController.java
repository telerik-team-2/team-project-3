package com.learnapp.learn.domains.account_types;

import java.util.List;

import com.learnapp.learn.domains.account_types.interfaces.AccountTypeService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/account_types")
public class AccountTypeController {
    
    private final AccountTypeService accountTypeService;

    public AccountTypeController(AccountTypeService accountTypeService) {
        this.accountTypeService = accountTypeService;
    }

    @GetMapping
    public List<AccountType> getAll() {
        return accountTypeService.getAll();
    }
}
