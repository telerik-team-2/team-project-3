package com.learnapp.learn.domains.administration_requests;

import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.administration_request_statuses.AdministrationRequestStatus;
import com.learnapp.learn.domains.administration_request_types.AdministrationRequestType;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "administration_requests")
public class AdministrationRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "request_id")
    private Integer requestId;

    @ManyToOne
    @JoinColumn(name = "request_type_id")
    private AdministrationRequestType administrationRequestType;

    @ManyToOne
    @JoinColumn(name = "submission_account_id")
    private Account submissionAccount;

    @Column(name = "creation_date")
    private LocalDateTime creationDate;

    @ManyToOne
    @JoinColumn(name = "status_id")
    private AdministrationRequestStatus status;

    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    public AdministrationRequestType getAdministrationRequestType() {
        return administrationRequestType;
    }

    public void setAdministrationRequestType(AdministrationRequestType administrationRequestType) {
        this.administrationRequestType = administrationRequestType;
    }

    public Account getSubmissionAccount() {
        return submissionAccount;
    }

    public void setSubmissionAccount(Account submissionAccount) {
        this.submissionAccount = submissionAccount;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public AdministrationRequestStatus getStatus() {
        return status;
    }

    public void setStatus(AdministrationRequestStatus status) {
        this.status = status;
    }
}
