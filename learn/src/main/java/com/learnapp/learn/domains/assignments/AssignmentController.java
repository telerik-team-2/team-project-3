package com.learnapp.learn.domains.assignments;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import com.learnapp.learn.domains.AuthenticationHelpers;
import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.accounts.interfaces.AccountService;
import com.learnapp.learn.domains.assignments.interfaces.AssignmentService;

import com.learnapp.learn.domains.assignments.wrappers.AssignmentGetAllParameters;
import com.learnapp.learn.domains.lectures.interfaces.LectureService;
import com.learnapp.learn.utilities.ControllerArgumentsParseHelpers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/assignments")
public class AssignmentController {

    public final AuthenticationHelpers authenticationHelpers;
    public final AssignmentService assignmentService;
    public final AccountService accountService;
    public final LectureService lectureService;

    @Autowired
    public AssignmentController(AuthenticationHelpers authenticationHelpers,
                                AssignmentService assignmentService,
                                AccountService accountService,
                                LectureService lectureService) {
        this.authenticationHelpers = authenticationHelpers;
        this.assignmentService = assignmentService;
        this.accountService = accountService;
        this.lectureService = lectureService;
    }

    @GetMapping
    public List<Assignment> getAll(@RequestHeader HttpHeaders headers,
                                   @RequestParam(name = "lectureId", required = false) Integer lectureId,
                                   @RequestParam(name = "gradeMin", required = false) Integer gradeMin,
                                   @RequestParam(name = "gradeMax", required = false) Integer gradeMax,
                                   @RequestParam(name = "graded", required = false) Boolean graded) {
        Account loggedUser = authenticationHelpers.tryGetUser(headers);
        AssignmentGetAllParameters assignmentGetAllParameters = new AssignmentGetAllParameters();

        assignmentGetAllParameters.setLectureId(lectureId);
        assignmentGetAllParameters.setGradeMin(gradeMin);
        assignmentGetAllParameters.setGradeMax(gradeMax);
        assignmentGetAllParameters.setGraded(graded);

        return assignmentService.getAllAuthorized(loggedUser, assignmentGetAllParameters);
    }

    @GetMapping("/{id}")
    public Assignment getById(@RequestHeader HttpHeaders headers,
                              @PathVariable Integer id) {
        Account loggedUser = authenticationHelpers.tryGetUser(headers);
        return assignmentService.getByIdAuthorized(loggedUser, id);
    }

    @GetMapping(path = "/{id}/file",
                produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public byte[] getFile(@RequestHeader HttpHeaders headers,
                          @PathVariable Integer id) throws IOException {
        Account loggedUser = authenticationHelpers.tryGetUser(headers);
        File fileToReturn = assignmentService.getFileAuthorized(loggedUser, id);
        return Files.readAllBytes(fileToReturn.toPath());
    }

    @PutMapping("/{id}/file")
    public void updateFile(@RequestHeader HttpHeaders headers,
                           @PathVariable int id,
                           @RequestPart("assignmentFile") MultipartFile file) throws IOException {
        Account loggedUser = authenticationHelpers.tryGetUser(headers);
        File fileToSave = File.createTempFile("assignment", "file");
        file.transferTo(fileToSave);
        try {
            assignmentService.updateFileAuthorized(loggedUser, id, fileToSave);
        } finally {
            fileToSave.delete();
        }
    }

    @PutMapping("/{id}/grade")
    public void updateGrade(@RequestHeader HttpHeaders headers,
                            @PathVariable Integer id,
                            @RequestBody String gradeString) {
        Account loggedUser = authenticationHelpers.tryGetUser(headers);
        int grade = ControllerArgumentsParseHelpers.getInteger(gradeString);
        assignmentService.updateGradeAuthorized(loggedUser, id, grade);
    }
}
