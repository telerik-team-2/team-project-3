package com.learnapp.learn.domains.base_classes.interfaces;

import java.util.List;

public interface BaseGetRepository<T> {

    List<T> getAll();

    List<T> getAll(BaseGetAllParameters baseGetAllParameters);

    T getById(int objectId);

    T getByField(String fieldName, Object fieldValue);

    boolean checkEntityExists(String fieldName, Object fieldValue);

}
