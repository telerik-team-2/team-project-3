package com.learnapp.learn.domains.courses.ratings;

import javax.persistence.*;

@Entity
@Table(name = "course_ratings")
public class Rating {

    public static final int VERY_BAD = 1;
    public static final int BAD = 2;
    public static final int AVERAGE = 3;
    public static final int GOOD = 4;
    public static final int VERY_GOOD = 5;

    @Id
    @Column(name = "rating_id")
    private Integer ratingId;

    @Column(name = "name")
    private String name;

    public Integer getRatingId() {
        return ratingId;
    }

    public void setRatingId(Integer ratingId) {
        this.ratingId = ratingId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
