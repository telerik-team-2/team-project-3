package com.learnapp.learn.domains.accounts;

import com.learnapp.learn.domains.accounts.interfaces.AccountRepository;
import com.learnapp.learn.domains.base_classes.BaseRepositoryImpl;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AccountRepositoryImpl extends BaseRepositoryImpl<Account> implements AccountRepository {

    @Autowired
    public AccountRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory, Account.class);
    }

    public Account getByEmail(String emailAddress) {
        return getByField("email", emailAddress);
    }

    public boolean checkEmailExists(String emailAddress) {
        return checkEntityExists("email", emailAddress);
    }
}
