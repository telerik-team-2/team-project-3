package com.learnapp.learn.domains.accounts.dtos;

import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.accounts.wrappers.AccountCreateParameters;
import com.learnapp.learn.domains.base_classes.interfaces.CreateParametersDto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class AccountCreateDto implements CreateParametersDto<Account, AccountCreateParameters> {

    @NotNull
    private Integer accountTypeId;

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    @NotBlank
    private String email;

    @NotNull
    private String phoneNumber;

    @NotNull
    private String password;

    @Override
    public void transferFieldsToHolder(AccountCreateParameters accountCreateParameters) {
        accountCreateParameters.setAccountTypeId(accountTypeId);
        accountCreateParameters.setFirstName(firstName);
        accountCreateParameters.setLastName(lastName);
        accountCreateParameters.setEmail(email);
        accountCreateParameters.setPhoneNumber(phoneNumber);
        accountCreateParameters.setPassword(password);
    }

    public Integer getAccountTypeId() {
        return accountTypeId;
    }

    public void setAccountTypeId(Integer accountTypeId) {
        this.accountTypeId = accountTypeId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
