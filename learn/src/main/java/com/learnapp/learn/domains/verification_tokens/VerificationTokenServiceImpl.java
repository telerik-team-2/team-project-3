package com.learnapp.learn.domains.verification_tokens;

import java.time.LocalDateTime;

import com.learnapp.learn.domains.base_classes.BaseServiceImpl;
import com.learnapp.learn.domains.verification_tokens.interfaces.VerificationTokenRepository;
import com.learnapp.learn.domains.verification_tokens.interfaces.VerificationTokenService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VerificationTokenServiceImpl extends BaseServiceImpl<VerificationToken> implements VerificationTokenService {
    
    public static final int ACTIVE_PERIOD = 60 * 24;

    private final VerificationTokenRepository verificationTokenRepository;

    @Autowired
    public VerificationTokenServiceImpl(VerificationTokenRepository verificationTokenRepository) {
        super(verificationTokenRepository);
        this.verificationTokenRepository = verificationTokenRepository;
    }

    @Override
    protected void configureCreatedObject(VerificationToken objectToCreate) {
        objectToCreate.setExpirationTime(LocalDateTime.now().plusMinutes(ACTIVE_PERIOD));
    }

    public VerificationToken getByValue(String value) {
        return verificationTokenRepository.getByValue(value);
    }
}
