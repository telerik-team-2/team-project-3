package com.learnapp.learn.domains.accounts;

import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import com.learnapp.learn.domains.accounts.interfaces.AccountService;
import com.learnapp.learn.domains.administration_request_statuses.AdministrationRequestStatus;
import com.learnapp.learn.domains.administration_requests.AdministrationRequest;
import com.learnapp.learn.exceptions.UnauthorizedAccessException;
import com.learnapp.learn.mvc.BaseMvcController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/accounts")
public class AccountMvcController extends BaseMvcController{
    
    private final AccountService accountService;

    @Autowired
    private AccountMvcController(AccountService accountService) {
        super(accountService);
        this.accountService = accountService;
    }

    @GetMapping("/{id}")
    public String getById(HttpSession session,
                          Model model,
                          @PathVariable int id) {
        if (!checkHasLoggedUser(model)) {
            return "redirect:/";
        }

        Account loggedUser = getLoggedUser(model);

        Account account;
        try {
            account = accountService.getByIdAuthorized(loggedUser, id);
        } catch (UnauthorizedAccessException e) {
            return "redirect:/";
        }

        model.addAttribute("account", account);

        Set<AdministrationRequest> requestsActive = account.getAdministrationRequestSet();
        requestsActive = requestsActive.stream()
                                       .filter(request -> request.getStatus().getStatusId() != AdministrationRequestStatus.APPROVED_STATUS_ID && 
                                                        request.getStatus().getStatusId() != AdministrationRequestStatus.DENIED_STATUS_ID)
                                       .collect(Collectors.toSet());
        model.addAttribute("adminRequests", requestsActive);

        return "account-info";
    }
}
