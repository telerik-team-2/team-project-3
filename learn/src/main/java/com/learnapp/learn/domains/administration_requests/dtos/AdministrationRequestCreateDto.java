package com.learnapp.learn.domains.administration_requests.dtos;

import javax.validation.constraints.NotNull;

import com.learnapp.learn.domains.administration_requests.AdministrationRequest;
import com.learnapp.learn.domains.administration_requests.wrappers.AdministrationRequestCreateParameters;
import com.learnapp.learn.domains.base_classes.interfaces.CreateParametersDto;

public class AdministrationRequestCreateDto implements CreateParametersDto<AdministrationRequest, AdministrationRequestCreateParameters>{

    @NotNull
    private Integer administrationRequestTypeId;

    @Override
    public void transferFieldsToHolder(AdministrationRequestCreateParameters createParametersHolder) {
        createParametersHolder.setAdministrationRequestTypeId(administrationRequestTypeId);
    }

    public Integer getAdministrationRequestTypeId() {
        return administrationRequestTypeId;
    }

    public void setAdministrationRequestTypeId(Integer administrationRequestTypeId) {
        this.administrationRequestTypeId = administrationRequestTypeId;
    }
}
