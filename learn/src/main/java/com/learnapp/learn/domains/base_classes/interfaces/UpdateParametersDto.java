package com.learnapp.learn.domains.base_classes.interfaces;

public interface UpdateParametersDto<T, C extends UpdateParametersHolder<T>> {

    void transferFieldsToHolder(C updateParametersHolder);

}
