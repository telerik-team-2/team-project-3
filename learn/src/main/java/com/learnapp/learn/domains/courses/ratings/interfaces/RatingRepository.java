package com.learnapp.learn.domains.courses.ratings.interfaces;

import com.learnapp.learn.domains.base_classes.interfaces.BaseGetRepository;
import com.learnapp.learn.domains.courses.ratings.Rating;

public interface RatingRepository extends BaseGetRepository<Rating> {
}
