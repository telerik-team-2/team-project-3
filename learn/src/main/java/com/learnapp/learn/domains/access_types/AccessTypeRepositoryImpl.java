package com.learnapp.learn.domains.access_types;

import com.learnapp.learn.domains.access_types.interfaces.AccessTypeRepository;
import com.learnapp.learn.domains.base_classes.BaseGetRepositoryImpl;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AccessTypeRepositoryImpl extends BaseGetRepositoryImpl<AccessType> implements AccessTypeRepository {

    @Autowired
    public AccessTypeRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory, AccessType.class);
    }
}
