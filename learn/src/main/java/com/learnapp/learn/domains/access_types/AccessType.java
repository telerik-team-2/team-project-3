package com.learnapp.learn.domains.access_types;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "access_types")
public class AccessType {

    public static final int STUDENT_ACCESS_ID = 1;
    public static final int TEACHER_ACCESS_ID = 2;
    public static final int ADMIN_ACCESS_ID = 3;

    @Id
    @Column(name = "access_id")
    private Integer accessId;

    @Column(name = "name")
    private String name;

    public Integer getAccessId() {
        return accessId;
    }

    public void setAccessId(Integer accessId) {
        this.accessId = accessId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccessType that = (AccessType) o;
        return getAccessId().equals(that.getAccessId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAccessId());
    }
}
