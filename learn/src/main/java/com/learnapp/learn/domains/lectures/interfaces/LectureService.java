package com.learnapp.learn.domains.lectures.interfaces;

import com.learnapp.learn.domains.base_classes.interfaces.BaseService;
import com.learnapp.learn.domains.lectures.Lecture;

public interface LectureService extends BaseService<Lecture> {
}
