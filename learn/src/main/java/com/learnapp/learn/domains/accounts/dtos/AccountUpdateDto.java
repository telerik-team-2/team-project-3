package com.learnapp.learn.domains.accounts.dtos;

import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.accounts.wrappers.AccountUpdateParameters;
import com.learnapp.learn.domains.base_classes.interfaces.UpdateParametersDto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class AccountUpdateDto implements UpdateParametersDto<Account, AccountUpdateParameters> {

    @Size(min = 2, max = 20)
    @NotBlank
    private String firstName;

    @Size(min = 2, max = 20)
    @NotBlank
    private String lastName;

    @NotBlank
    private String phoneNumber;

    @Override
    public void transferFieldsToHolder(AccountUpdateParameters updateParametersHolder) {
        updateParametersHolder.setFirstName(firstName);
        updateParametersHolder.setLastName(lastName);
        updateParametersHolder.setPhoneNumber(phoneNumber);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
