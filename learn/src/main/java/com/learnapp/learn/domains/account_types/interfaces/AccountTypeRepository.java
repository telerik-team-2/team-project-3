package com.learnapp.learn.domains.account_types.interfaces;

import com.learnapp.learn.domains.account_types.AccountType;
import com.learnapp.learn.domains.base_classes.interfaces.BaseGetRepository;

public interface AccountTypeRepository extends BaseGetRepository<AccountType> {
}
