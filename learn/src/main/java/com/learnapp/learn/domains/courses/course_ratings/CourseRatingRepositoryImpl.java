package com.learnapp.learn.domains.courses.course_ratings;

import com.learnapp.learn.domains.base_classes.BaseRepositoryImpl;
import com.learnapp.learn.domains.courses.course_ratings.interfaces.CourseRatingRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CourseRatingRepositoryImpl extends BaseRepositoryImpl<CourseRating> implements CourseRatingRepository {

    @Autowired
    public CourseRatingRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory, CourseRating.class);
    }
}
