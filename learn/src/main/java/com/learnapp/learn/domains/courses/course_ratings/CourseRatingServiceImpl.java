package com.learnapp.learn.domains.courses.course_ratings;

import com.learnapp.learn.domains.AuthorizationHelpers;
import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.assignments.Assignment;
import com.learnapp.learn.domains.assignments.interfaces.AssignmentService;
import com.learnapp.learn.domains.base_classes.BaseServiceImpl;
import com.learnapp.learn.domains.courses.Course;
import com.learnapp.learn.domains.courses.course_ratings.interfaces.CourseRatingRepository;
import com.learnapp.learn.domains.courses.course_ratings.interfaces.CourseRatingService;
import com.learnapp.learn.domains.courses.course_ratings.wrappers.CourseRatingCreateParameters;
import com.learnapp.learn.domains.courses.interfaces.CourseService;
import com.learnapp.learn.exceptions.DuplicateEntityException;
import com.learnapp.learn.exceptions.EntityNotFoundException;
import com.learnapp.learn.exceptions.UnauthorizedAccessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class CourseRatingServiceImpl extends BaseServiceImpl<CourseRating> implements CourseRatingService {

    public static final String DUPLICATE_RATING_ASSIGNMENT = "The course %s already has a rating from account %s.";

    private final AssignmentService assignmentService;
    private final CourseService courseService;

    @Autowired
    public CourseRatingServiceImpl(CourseRatingRepository courseRatingRepository,
                                   AssignmentService assignmentService,
                                   CourseService courseService) {
        super(courseRatingRepository);
        this.assignmentService = assignmentService;
        this.courseService = courseService;
    }

    @Override
    public CourseRating createAuthorized(Account loggedUser, CourseRatingCreateParameters courseRatingCreateParameters) {
        Course course = courseService.getByIdAuthorized(loggedUser, courseRatingCreateParameters.getCourseId());
        if (!checkCourseCompleted(loggedUser, course)) {
            throw new UnauthorizedAccessException(AuthorizationHelpers.UNAUTHORIZED_ACCESS);
        }

        if (checkRatingExists(loggedUser, course)) {
            throw new DuplicateEntityException(String.format(DUPLICATE_RATING_ASSIGNMENT, course.getCourseId(), loggedUser.getAccountId()));
        }

        return super.createAuthorized(loggedUser, courseRatingCreateParameters);
    }

    @Override
    public boolean checkCourseCompleted(Account account, Course course) {
        List<Assignment> accountAssignments = assignmentService.getByCourse(account, course.getCourseId());
        if (accountAssignments.size() < course.getLectures().size()) {
            return false;
        }

        Integer coursePoints = courseGrade(accountAssignments);
        if (coursePoints == null || coursePoints < course.getPassingGrade()) {
            return false;
        }

        return true;
    }

    @Override
    public boolean checkRatingExists(Account account, Course course) {
        return getAccountCourseRatingUnsafe(account, course) != null;
    }

    @Override
    public CourseRating getAccountCourseRating(Account account, Course course) {
        CourseRating courseRating = getAccountCourseRatingUnsafe(account, course);
        if (courseRating == null) {
            throw new EntityNotFoundException("Course rating",
                    String.format("course id %s", course.getCourseId()),
                    String.format("from account %s", account.getAccountId()));
        }
        return courseRating;
    }
    private CourseRating getAccountCourseRatingUnsafe(Account account, Course course) {
        return course.getCourseRatings()
                .stream()
                .filter(courseRating -> Objects.equals(courseRating.getAccount().getAccountId(), account.getAccountId()))
                .findFirst()
                .orElse(null);
    }

    private Integer courseGrade(List<Assignment> assignments) {
        int coursePoints = 0;
        for (Assignment assignment : assignments) {
            Integer assignmentGrade = assignment.getGrade();
            if (assignmentGrade == null) {
                return null;
            }
            coursePoints += assignmentGrade;
        }
        return coursePoints;
    }
}
