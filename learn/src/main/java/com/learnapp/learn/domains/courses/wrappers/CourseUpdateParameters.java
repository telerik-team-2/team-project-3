package com.learnapp.learn.domains.courses.wrappers;

import com.learnapp.learn.domains.base_classes.interfaces.UpdateParametersHolder;
import com.learnapp.learn.domains.courses.course_ratings.CourseRating;
import com.learnapp.learn.domains.courses.Course;
import com.learnapp.learn.domains.courses.CourseDescription;
import com.learnapp.learn.domains.courses.statuses.interfaces.StatusService;
import com.learnapp.learn.domains.courses.topics.interfaces.TopicService;

import java.time.LocalDate;
import java.util.Set;

public class CourseUpdateParameters implements UpdateParametersHolder<Course> {

    private final TopicService topicService;
    private final StatusService statusService;

    private String title;
    private Integer topicId;
    private Integer passingGrade;
    private Integer statusId;
    private LocalDate startingDate;
    private Set<CourseRating> courseRatings;
    private String courseDescription;

    public CourseUpdateParameters(TopicService topicService, StatusService statusService) {
        this.topicService = topicService;
        this.statusService = statusService;
    }

    //TODO: update rating in DB

    @Override
    public void updateObjectWithParameters(Course objectToUpdate) {
        objectToUpdate.setTitle(title);
        objectToUpdate.setTopic(topicService.getById(topicId));
        objectToUpdate.setPassingGrade(passingGrade);
        objectToUpdate.setStatus(statusService.getById(statusId));
        objectToUpdate.setStartingDate(startingDate);

        if (this.courseDescription == null) {
            objectToUpdate.setCourseDescription(null);
            return;
        }

        CourseDescription courseDescription = objectToUpdate.getCourseDescription();
        if(courseDescription == null) {
            courseDescription = new CourseDescription();
            objectToUpdate.setCourseDescription(courseDescription);
            courseDescription.setCourse(objectToUpdate);
        }

        courseDescription.setDescriptionContent(this.courseDescription);
    }

    public TopicService getTopicService() {
        return topicService;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getTopicId() {
        return topicId;
    }

    public void setTopicId(Integer topicId) {
        this.topicId = topicId;
    }

    public Integer getPassingGrade() {
        return passingGrade;
    }

    public void setPassingGrade(Integer passingGrade) {
        this.passingGrade = passingGrade;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public LocalDate getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(LocalDate startingDate) {
        this.startingDate = startingDate;
    }

    public Set<CourseRating> getCourseRatings() {
        return courseRatings;
    }

    public void setCourseRatings(Set<CourseRating> courseRatings) {
        this.courseRatings = courseRatings;
    }

    public String getCourseDescription() {
        return courseDescription;
    }

    public void setCourseDescription(String courseDescription) {
        this.courseDescription = courseDescription;
    }
}
