package com.learnapp.learn.domains;

import com.learnapp.learn.domains.accounts.Account;

public class AuthorizationHelpers {

    public static final String UNAUTHORIZED_ACCESS = "Unauthorized access";

    public static boolean checkAccess(Account account, int accessId) {
        return account.getAccessTypeSet().stream()
                .anyMatch(accessType -> accessType.getAccessId() == accessId);
    }

    public static boolean authorizeAccountAccess(Account account, int accountId, int accessId) {
        if (account.getAccountId() == accountId) {
            return  true;
        }

        return AuthorizationHelpers.checkAccess(account, accessId);
    }
}
