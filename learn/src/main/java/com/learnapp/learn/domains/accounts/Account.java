package com.learnapp.learn.domains.accounts;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.learnapp.learn.domains.AuthorizationHelpers;
import com.learnapp.learn.domains.access_types.AccessType;
import com.learnapp.learn.domains.account_types.AccountType;
import com.learnapp.learn.domains.administration_requests.AdministrationRequest;
import com.learnapp.learn.domains.courses.Course;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "accounts")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "account_id")
    private Integer accountId;

    @ManyToOne
    @JoinColumn(name = "account_type_id")
    private AccountType accountType;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "email")
    private String email;

    @JsonIgnore
    @Column(name = "password")
    private String password;

    @Column(name = "verified")
    private boolean verified;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "accounts_access_types",
            joinColumns = @JoinColumn(name = "account_id"),
            inverseJoinColumns = @JoinColumn( name = "access_id"))
    private Set<AccessType> accessTypeSet;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "courses_accounts",
            joinColumns = @JoinColumn(name = "account_id"),
            inverseJoinColumns = @JoinColumn(name = "course_id"))
    private Set<Course> courseSet;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER,
               mappedBy = "submissionAccount")
    private Set<AdministrationRequest> administrationRequestSet;

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public Set<AccessType> getAccessTypeSet() {
        return accessTypeSet;
    }

    public void setAccessTypeSet(Set<AccessType> accessTypeSet) {
        this.accessTypeSet = accessTypeSet;
    }

    public Set<Course> getCourseSet() {
        return courseSet;
    }

    public void setCourseSet(Set<Course> courseSet) {
        this.courseSet = courseSet;
    }

    public Set<AdministrationRequest> getAdministrationRequestSet() {
        return administrationRequestSet;
    }

    public void setAdministrationRequestSet(Set<AdministrationRequest> administrationRequestSet) {
        this.administrationRequestSet = administrationRequestSet;
    }

    @JsonIgnore
    public boolean isAdmin() {
        return AuthorizationHelpers.checkAccess(this, AccessType.ADMIN_ACCESS_ID);
    }

    @JsonIgnore
    public boolean isTeacher() {
        return AuthorizationHelpers.checkAccess(this, AccessType.TEACHER_ACCESS_ID);
    }
}
