package com.learnapp.learn.domains.accounts;

import com.learnapp.learn.domains.AuthorizationHelpers;
import com.learnapp.learn.domains.access_types.AccessType;
import com.learnapp.learn.domains.access_types.interfaces.AccessTypeService;
import com.learnapp.learn.domains.account_types.AccountType;
import com.learnapp.learn.domains.account_types.interfaces.AccountTypeService;
import com.learnapp.learn.domains.accounts.interfaces.AccountRepository;
import com.learnapp.learn.domains.accounts.interfaces.AccountService;
import com.learnapp.learn.domains.accounts.wrappers.AccountCreateParameters;
import com.learnapp.learn.domains.administration_request_types.AdministrationRequestType;
import com.learnapp.learn.domains.administration_request_types.interfaces.AdministrationRequestTypeService;
import com.learnapp.learn.domains.administration_requests.interfaces.AdministrationRequestService;
import com.learnapp.learn.domains.administration_requests.wrappers.AdministrationRequestCreateParameters;
import com.learnapp.learn.domains.base_classes.BaseServiceImpl;
import com.learnapp.learn.domains.courses.Course;
import com.learnapp.learn.domains.courses.interfaces.CourseService;
import com.learnapp.learn.domains.file_management.FileManager;
import com.learnapp.learn.domains.verification_tokens.VerificationToken;
import com.learnapp.learn.exceptions.DuplicateEntityException;
import com.learnapp.learn.exceptions.UnauthorizedAccessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class AccountServiceImpl extends BaseServiceImpl<Account> implements AccountService {

    private static final String DUPLICATE_EMAIL_EXCEPTION = "Account with email address %s already exists.";
    private static final String EXPIRED_VERIFICATION = "The verification code has expired.";
    private static final String ALREADY_VERIFIED = "Your account is verified.";
    private static final int[] ALLOWED_REGISTRATION_ACCOUNT_TYPES = {AccessType.STUDENT_ACCESS_ID, AccessType.TEACHER_ACCESS_ID};
    private static final String INVALID_ACCOUNT_TYPE = "Invalid account type ID provided: %s";

    private final FileManager fileManager;

    private final AccountRepository accountRepository;
    private final AccessTypeService accessTypeService;
    private final AccountTypeService accountTypeService;
    private final AdministrationRequestService administrationRequestService;
    private final AdministrationRequestTypeService administrationRequestTypeService;
    private final CourseService courseService;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository,
                              FileManager fileManager,
                              AccessTypeService accessTypeService,
                              AccountTypeService accountTypeService,
                              AdministrationRequestService administrationRequestService,
                              AdministrationRequestTypeService administrationRequestTypeService,
                              CourseService courseService) {
        super(accountRepository);
        this.accountRepository = accountRepository;
        this.fileManager = fileManager;
        this.accessTypeService = accessTypeService;
        this.accountTypeService = accountTypeService;
        this.administrationRequestService = administrationRequestService;
        this.administrationRequestTypeService = administrationRequestTypeService;
        this.courseService = courseService;
    }

    @Override
    protected boolean checkGetAllAuthorization(Account loggedUser) {
        return AuthorizationHelpers.checkAccess(loggedUser, AccessType.ADMIN_ACCESS_ID);
    }

    @Override
    protected boolean checkGetByIdAuthorization(Account loggedUser, int accountId) {
        return AuthorizationHelpers.authorizeAccountAccess(loggedUser, accountId, AccessType.TEACHER_ACCESS_ID);
    }

    public Account getByEmail(String emailAddress) {
        return accountRepository.getByEmail(emailAddress);
    }

    @Override
    public File getProfilePictureAuthorized(Account loggedUser, int id) {
        if (!AuthorizationHelpers.authorizeAccountAccess(loggedUser, id, AccessType.TEACHER_ACCESS_ID)) {
            throw new UnauthorizedAccessException(AuthorizationHelpers.UNAUTHORIZED_ACCESS);
        }
        
        File profilePicture = fileManager.getProfilePicture(id);
        if (!Files.exists(profilePicture.toPath())) {
            profilePicture = fileManager.getDefaultProfilePicture();
        }

        return profilePicture;
    }

    @Override
    public Account create(AccountCreateParameters accountCreateParameters) {
        if (accountRepository.checkEmailExists(accountCreateParameters.getEmail())) {
            throw new DuplicateEntityException(String.format(DUPLICATE_EMAIL_EXCEPTION, accountCreateParameters.getEmail()));
        }

        int accountTypeId = accountCreateParameters.getAccountTypeId();
        if (!checkAccountTypeValidity(accountTypeId)) {
            throw new IllegalArgumentException(String.format(INVALID_ACCOUNT_TYPE, accountTypeId));
        }

        Account newAccount = super.create(accountCreateParameters);

        trySubmitTeacherRequest(newAccount);
        return newAccount;
    }

    private void trySubmitTeacherRequest(Account newAccount) {
        if (newAccount.getAccountType().getAccountTypeId() != AccountType.TEACHER_ID) {
            return;
        }

        AdministrationRequestCreateParameters administrationRequestCreateParameters = 
                new AdministrationRequestCreateParameters(this, administrationRequestTypeService);
        administrationRequestCreateParameters.setAdministrationRequestTypeId(AdministrationRequestType.TEACHER_APPROVAL_ID);
        administrationRequestCreateParameters.setSubmissionAccountId(newAccount.getAccountId());
        administrationRequestService.create(administrationRequestCreateParameters);
    }

    private boolean checkAccountTypeValidity(int accountTypeIdToCheck) {
        for (int accountTypeId : ALLOWED_REGISTRATION_ACCOUNT_TYPES) {
            if (accountTypeId == accountTypeIdToCheck) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void configureCreatedObject(Account objectToCreate) {
        objectToCreate.setVerified(false);
        Set<AccessType> accessTypeSet = new HashSet<>();
        accessTypeSet.add(accessTypeService.getById(AccessType.STUDENT_ACCESS_ID));
        objectToCreate.setAccessTypeSet(accessTypeSet);
    }

    @Override
    protected boolean checkUpdateAuthorization(Account loggedUser, Account objectToUpdate) {
        return AuthorizationHelpers.authorizeAccountAccess(loggedUser, objectToUpdate.getAccountId(), AccessType.ADMIN_ACCESS_ID);
    }

    @Override
    public void updatePassword(Account loggedUser, int id, String newPassword) {
        if (!AuthorizationHelpers.authorizeAccountAccess(loggedUser, id, AccessType.ADMIN_ACCESS_ID)) {
            throw new UnauthorizedAccessException(AuthorizationHelpers.UNAUTHORIZED_ACCESS);
        }
        
        Account accountToUpdate = getById(id);
        accountToUpdate.setPassword(newPassword);
        update(accountToUpdate);
    }

    @Override
    public Course enrollCourse(Account loggedUser, int id, int courseId) {
        Course course = courseService.getByIdAuthorized(loggedUser, courseId);
        Account accountToUpdate = getByIdAuthorized(loggedUser, id);
        Set<Course> courseSet = new HashSet<>(accountToUpdate.getCourseSet());
        courseSet.add(course);
        accountToUpdate.setCourseSet(courseSet);
        accountRepository.update(accountToUpdate);
        return course;
    }

    @Override
    public void addAccessTypes(Account loggedUser, int id, List<Integer> accessTypeIdList) {
        if (!AuthorizationHelpers.checkAccess(loggedUser, AccessType.ADMIN_ACCESS_ID)) {
            throw new UnauthorizedAccessException(AuthorizationHelpers.UNAUTHORIZED_ACCESS);
        }

        Account accountToUpdate = getById(id);
        Set<AccessType> accessTypeSet = accountToUpdate.getAccessTypeSet();
        for (int accessTypeId : accessTypeIdList) {
            accessTypeSet.add(accessTypeService.getById(accessTypeId));
        }
        update(accountToUpdate);
    }

    @Override
    public void updateAccountType(Account loggedUser, int id, int accountTypeId) {
        if (!AuthorizationHelpers.checkAccess(loggedUser, AccessType.ADMIN_ACCESS_ID)) {
            throw new UnauthorizedAccessException(AuthorizationHelpers.UNAUTHORIZED_ACCESS);
        }

        Account accountToUpdate = getById(id);
        accountToUpdate.setAccountType(accountTypeService.getById(accountTypeId));
        update(accountToUpdate);
    }

    @Override
    public void updateProfilePictureAuthorized(Account loggedUser, int id, File newPicture) {
        if (!AuthorizationHelpers.authorizeAccountAccess(loggedUser, id, AccessType.ADMIN_ACCESS_ID)) {
            throw new UnauthorizedAccessException(AuthorizationHelpers.UNAUTHORIZED_ACCESS);
        }
        
        fileManager.saveProfilePicture(id, newPicture);
    }

    @Override
    public void verify(VerificationToken verificationToken) {
        if (LocalDateTime.now().isAfter(verificationToken.getExpirationTime())) {
            throw new IllegalArgumentException(EXPIRED_VERIFICATION);
        }

        Account account = verificationToken.getAccount();

        if (account.isVerified()) {
            throw new IllegalArgumentException(ALREADY_VERIFIED);
        }

        account.setVerified(true);
        update(account);
    }

    @Override
    protected boolean checkDeleteAuthorization(Account loggedUser, Account objectToDelete) {
        return AuthorizationHelpers.authorizeAccountAccess(loggedUser, objectToDelete.getAccountId(), AccessType.ADMIN_ACCESS_ID);
    }

    @Override
    public void deleteAccessTypes(Account loggedUser, int id, List<Integer> accessTypeIdList) {
        if (!AuthorizationHelpers.checkAccess(loggedUser, AccessType.ADMIN_ACCESS_ID)) {
            throw new UnauthorizedAccessException(AuthorizationHelpers.UNAUTHORIZED_ACCESS);
        }

        Account accountToUpdate = getById(id);
        Set<AccessType> accessTypeSet = accountToUpdate.getAccessTypeSet();
        for (int accessTypeId : accessTypeIdList) {
            accessTypeSet.remove(accessTypeService.getById(accessTypeId));
        }
        update(accountToUpdate);
    }
}
