package com.learnapp.learn.domains.accounts.wrappers;

import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.base_classes.interfaces.UpdateParametersHolder;

public class AccountUpdateParameters implements UpdateParametersHolder<Account> {

    private String firstName;
    private String lastName;
    private String phoneNumber;

    @Override
    public void updateObjectWithParameters(Account objectToUpdate) {
        objectToUpdate.setFirstName(firstName);
        objectToUpdate.setLastName(lastName);
        objectToUpdate.setPhoneNumber(phoneNumber);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
