package com.learnapp.learn.domains.courses;

import com.learnapp.learn.domains.AuthenticationHelpers;
import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.accounts.interfaces.AccountService;
import com.learnapp.learn.domains.courses.dtos.CourseCreateDto;
import com.learnapp.learn.domains.courses.dtos.CourseUpdateDto;
import com.learnapp.learn.domains.courses.interfaces.CourseService;
import com.learnapp.learn.domains.courses.wrappers.CourseCreateParameters;
import com.learnapp.learn.domains.courses.wrappers.CourseGetAllParameters;
import com.learnapp.learn.domains.courses.wrappers.CourseUpdateParameters;
import com.learnapp.learn.domains.courses.statuses.interfaces.StatusService;
import com.learnapp.learn.domains.courses.topics.interfaces.TopicService;
import com.learnapp.learn.utilities.ControllerArgumentsParseHelpers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;

@RestController
@RequestMapping("/api/courses")
public class CourseRestController {

    private final AuthenticationHelpers authenticationHelpers;
    private final CourseService courseService;
    private final AccountService accountService;
    private final TopicService topicService;
    private final StatusService statusService;

    @Autowired
    public CourseRestController(AuthenticationHelpers authenticationHelpers,
                                AccountService accountService,
                                CourseService courseService,
                                TopicService topicService,
                                StatusService statusService) {
        this.authenticationHelpers = authenticationHelpers;
        this.courseService = courseService;
        this.accountService = accountService;
        this.topicService = topicService;
        this.statusService = statusService;
    }

    @GetMapping
    public List<Course> getAll(@RequestHeader(required = false) HttpHeaders headers,
                               @RequestParam(name = "title", required = false) String title,
                               @RequestParam(name = "topicId", required = false) Integer topicId,
                               @RequestParam(name = "teacherId", required = false) Integer teacherId,
                               @RequestParam(name = "statusId", required = false) Integer statusId,
                               @RequestParam(name = "rating", required = false) Integer rating,
                               @RequestParam(name = "enrolledAccountId", required = false) Integer enrolledAccountId,
                               @RequestParam(name = "sortParams", required = false) String sortParams) {
        CourseGetAllParameters courseGetAllParameters = new CourseGetAllParameters(accountService);
        courseGetAllParameters.setTitle(title);
        courseGetAllParameters.setTopicId(topicId);
        courseGetAllParameters.setOwnerAccountId(teacherId);
        courseGetAllParameters.setStatusId(statusId);
        courseGetAllParameters.setRating(rating);
        courseGetAllParameters.setEnrolledAccountId(enrolledAccountId);

        if (sortParams != null) {
            courseGetAllParameters.setSortParams(ControllerArgumentsParseHelpers.parseSortParams(sortParams));
        }

        Account loggedUser;
        if (headers.containsKey("Authentication")) {
            loggedUser = authenticationHelpers.tryGetUser(headers);
        } else {
            loggedUser = new Account();
            loggedUser.setAccessTypeSet(new HashSet<>());
        }

        return courseService.getAllAuthorized(loggedUser, courseGetAllParameters);
    }

    @GetMapping("/{id}")
    public Course getById(@RequestHeader HttpHeaders headers,
                          @PathVariable Integer id) {
        Account loggedUser = authenticationHelpers.tryGetUser(headers);
        return courseService.getByIdAuthorized(loggedUser, id);
    }

    @PostMapping
    public void create(@RequestHeader HttpHeaders headers,
                       @Valid @RequestBody CourseCreateDto courseCreateDto) {
        Account loggedUser = authenticationHelpers.tryGetUser(headers);
        CourseCreateParameters courseCreateParameters = new CourseCreateParameters(topicService, accountService);
        courseCreateDto.transferFieldsToHolder(courseCreateParameters);
        courseCreateParameters.setOwnerAccountId(loggedUser.getAccountId());
        courseService.createAuthorized(loggedUser, courseCreateParameters);
    }

    @PutMapping("/{id}")
    public void update(@RequestHeader HttpHeaders headers,
                       @PathVariable Integer id,
                       @Valid @RequestBody CourseUpdateDto courseUpdateDto) {
        Account loggedUser = authenticationHelpers.tryGetUser(headers);
        CourseUpdateParameters courseUpdateParameters = new CourseUpdateParameters(topicService, statusService);
        courseUpdateDto.transferFieldsToHolder(courseUpdateParameters);
        courseService.updateAuthorized(loggedUser, id, courseUpdateParameters);
    }

    @DeleteMapping("{id}")
    public void delete(@RequestHeader HttpHeaders headers,
                       @PathVariable Integer id) {
        Account loggedUser = authenticationHelpers.tryGetUser(headers);
        courseService.deleteAuthorized(loggedUser, id);
    }
}


