package com.learnapp.learn.domains.assignments;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

import com.learnapp.learn.domains.AuthorizationHelpers;
import com.learnapp.learn.domains.access_types.AccessType;
import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.assignments.interfaces.AssignmentRepository;
import com.learnapp.learn.domains.assignments.interfaces.AssignmentService;
import com.learnapp.learn.domains.assignments.wrappers.AssignmentGetAllParameters;
import com.learnapp.learn.domains.base_classes.BaseServiceImpl;
import com.learnapp.learn.domains.base_classes.interfaces.BaseGetAllParameters;
import com.learnapp.learn.domains.file_management.FileManager;
import com.learnapp.learn.exceptions.UnauthorizedAccessException;

import org.springframework.stereotype.Service;

@Service
public class AssignmentServiceImpl extends BaseServiceImpl<Assignment> implements AssignmentService {

    private static final String ASSIGNMENT_ALREADY_GRADED = "Assignment has already been graded.";

    private final AssignmentRepository assignmentRepository;
    private final FileManager fileManager;

    public AssignmentServiceImpl(AssignmentRepository assignmentRepository,
                                 FileManager fileManager) {
        super(assignmentRepository);
        this.assignmentRepository = assignmentRepository;
        this.fileManager = fileManager;
    }

    @Override
    public List<Assignment> getByCourse(Account loggedUser, int courseId) {
        AssignmentGetAllParameters assignmentGetAllParameters = new AssignmentGetAllParameters();
        assignmentGetAllParameters.setCourseId(courseId);
        assignmentGetAllParameters.setOwnerAccountId(loggedUser.getAccountId());
        return getAll(assignmentGetAllParameters);
    }

    @Override
    public List<Assignment> getAll(BaseGetAllParameters baseGetAllParameters) {
        List<Assignment> assignments = super.getAll(baseGetAllParameters);
        assignments = assignments.stream()
                                 .filter(assignment -> 
                                                fileManager.checkAssignmentExists(assignment.getOwnerAccount().getAccountId(), assignment.getLecture().getLectureId()))
                                .collect(Collectors.toList());
        return assignments;
    }

    @Override
    protected boolean checkGetAllAuthorization(Account loggedUser) {
        return AuthorizationHelpers.checkAccess(loggedUser, AccessType.TEACHER_ACCESS_ID);
    }

    @Override
    public Assignment getByLecture(Account loggedUser, int lectureId) {
        return assignmentRepository.getByLecture(loggedUser.getAccountId(), lectureId);
    }

    @Override
    public File getFileAuthorized(Account loggedUser, int id) {
        Assignment assignment = getById(id);
        int ownerAccountId = assignment.getOwnerAccount().getAccountId();
        if (!AuthorizationHelpers.authorizeAccountAccess(loggedUser, ownerAccountId, AccessType.TEACHER_ACCESS_ID)) {
            throw new UnauthorizedAccessException(AuthorizationHelpers.UNAUTHORIZED_ACCESS);
        }

        return getFile(assignment);
    }

    @Override
    public File getFile(Assignment assignment) {
        return fileManager.getAssignment(assignment.getOwnerAccount().getAccountId(), assignment.getLecture().getLectureId());
    }

    @Override
    public Assignment updateFileAuthorized(Account loggedUser, int id, File assignmentToSave) {
        Assignment assignment = getById(id);
        if (!AuthorizationHelpers.authorizeAccountAccess(loggedUser, assignment.getOwnerAccount().getAccountId(), AccessType.TEACHER_ACCESS_ID)) {
            throw new UnauthorizedAccessException(AuthorizationHelpers.UNAUTHORIZED_ACCESS);
        }

        if (assignment.getGrade() != null) {
            throw new IllegalArgumentException(ASSIGNMENT_ALREADY_GRADED);
        }

        fileManager.saveAssignment(loggedUser.getAccountId(), assignment.getLecture().getLectureId(), assignmentToSave);

        return assignment;
    }

    @Override
    public void updateGradeAuthorized(Account loggedUser, int id, int grade) {
        if (!AuthorizationHelpers.checkAccess(loggedUser, AccessType.TEACHER_ACCESS_ID)) {
            throw new UnauthorizedAccessException(AuthorizationHelpers.UNAUTHORIZED_ACCESS);
        }

        Assignment assignmentToUpdate = getById(id);
        assignmentToUpdate.setGrade(Math.min(grade, assignmentToUpdate.getLecture().getMaxGrade()));
        update(assignmentToUpdate);
    }
}
