package com.learnapp.learn.domains.access_types.interfaces;

import com.learnapp.learn.domains.access_types.AccessType;
import com.learnapp.learn.domains.base_classes.interfaces.BaseGetService;

public interface AccessTypeService extends BaseGetService<AccessType> {
}
