package com.learnapp.learn.domains.courses.statuses;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "course_statuses")
public class Status {

    public static final int DRAFT_COURSE_STATUS_ID = 1;
    public static final int PUBLISHED_COURSE_STATUS_ID = 2;

    @Id
    @Column(name = "status_id")
    private Integer statusId;

    @Column(name = "name")
    private String name;

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
