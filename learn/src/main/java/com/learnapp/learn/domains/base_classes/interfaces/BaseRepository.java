package com.learnapp.learn.domains.base_classes.interfaces;

public interface BaseRepository<T> extends BaseGetRepository<T> {

    void create(T objectToCreate);

    void update(T objectToUpdate);

    void delete(T objectToDelete);

}
