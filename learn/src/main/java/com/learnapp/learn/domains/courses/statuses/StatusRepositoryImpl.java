package com.learnapp.learn.domains.courses.statuses;

import com.learnapp.learn.domains.base_classes.BaseGetRepositoryImpl;
import com.learnapp.learn.domains.courses.statuses.interfaces.StatusRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class StatusRepositoryImpl extends BaseGetRepositoryImpl<Status> implements StatusRepository {

    @Autowired
    public StatusRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory, Status.class);
    }
}
