package com.learnapp.learn.domains.administration_requests;

import com.learnapp.learn.domains.administration_requests.interfaces.AdministrationRequestRepository;
import com.learnapp.learn.domains.base_classes.BaseRepositoryImpl;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AdministrationRequestRepositoryImpl extends BaseRepositoryImpl<AdministrationRequest> implements AdministrationRequestRepository {

    @Autowired
    public AdministrationRequestRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory, AdministrationRequest.class);
    }
}
