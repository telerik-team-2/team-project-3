package com.learnapp.learn.domains.courses.dtos;

import com.learnapp.learn.domains.base_classes.interfaces.UpdateParametersDto;
import com.learnapp.learn.domains.courses.Course;
import com.learnapp.learn.domains.courses.wrappers.CourseUpdateParameters;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

public class CourseUpdateDto implements UpdateParametersDto<Course, CourseUpdateParameters> {

    @Size(min = 5, max = 50, message = "Title must be between 5 and 50 characters long.")
    @NotBlank
    private String title;
    @NotNull
    private Integer topicId;
    @NotNull
    private Integer passingGrade;
    private Integer statusId;
    private LocalDate startingDate;

    @Size(max = 1000)
    private String courseDescription;


    @Override
    public void transferFieldsToHolder(CourseUpdateParameters updateParametersHolder) {
        updateParametersHolder.setTitle(title);
        updateParametersHolder.setTopicId(topicId);
        updateParametersHolder.setStatusId(statusId);
        updateParametersHolder.setPassingGrade(passingGrade);
        updateParametersHolder.setStartingDate(startingDate);
        updateParametersHolder.setCourseDescription(courseDescription);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getTopicId() {
        return topicId;
    }

    public void setTopicId(Integer topicId) {
        this.topicId = topicId;
    }

    public Integer getPassingGrade() {
        return passingGrade;
    }

    public void setPassingGrade(Integer passingGrade) {
        this.passingGrade = passingGrade;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public LocalDate getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(LocalDate startingDate) {
        this.startingDate = startingDate;
    }

    public String getCourseDescription() {
        return courseDescription;
    }

    public void setCourseDescription(String courseDescription) {
        this.courseDescription = courseDescription;
    }
}
