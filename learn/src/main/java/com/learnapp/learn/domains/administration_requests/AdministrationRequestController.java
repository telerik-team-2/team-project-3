package com.learnapp.learn.domains.administration_requests;

import com.learnapp.learn.domains.AuthenticationHelpers;
import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.accounts.interfaces.AccountService;
import com.learnapp.learn.domains.administration_request_statuses.AdministrationRequestStatus;
import com.learnapp.learn.domains.administration_request_statuses.interfaces.AdministrationRequestStatusService;
import com.learnapp.learn.domains.administration_request_statuses.wrappers.AdministrationRequestGetAllParameters;
import com.learnapp.learn.domains.administration_request_types.interfaces.AdministrationRequestTypeService;
import com.learnapp.learn.domains.administration_requests.dtos.AdministrationRequestCreateDto;
import com.learnapp.learn.domains.administration_requests.interfaces.AdministrationRequestService;
import com.learnapp.learn.domains.administration_requests.wrappers.AdministrationRequestCreateParameters;
import com.learnapp.learn.utilities.ControllerArgumentsParseHelpers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/administration_requests")
public class AdministrationRequestController {

    private final AuthenticationHelpers authenticationHelpers;
    private final AdministrationRequestService administrationRequestService;
    private final AccountService accountService;
    private final AdministrationRequestTypeService administrationRequestTypeService;
    private final AdministrationRequestStatusService administrationRequestStatusService;

    @Autowired
    public AdministrationRequestController(AuthenticationHelpers authenticationHelpers,
                                           AdministrationRequestService administrationRequestService,
                                           AccountService accountService,
                                           AdministrationRequestTypeService administrationRequestTypeService,
                                           AdministrationRequestStatusService administrationRequestStatusService) {
        this.authenticationHelpers = authenticationHelpers;
        this.administrationRequestService = administrationRequestService;
        this.accountService = accountService;
        this.administrationRequestTypeService = administrationRequestTypeService;
        this.administrationRequestStatusService = administrationRequestStatusService;
    }

    @GetMapping
    public List<AdministrationRequest> getAll(@RequestHeader HttpHeaders headers,
                                              @RequestParam(name = "submissionAccountId", required = false) Integer submissionAccountId,
                                              @RequestParam(name = "statusId", required = false) Integer statusId) {
        Account loggedUser = authenticationHelpers.tryGetUser(headers);
        AdministrationRequestGetAllParameters administrationRequestGetAllParameters = new AdministrationRequestGetAllParameters();
        administrationRequestGetAllParameters.setSubmissionAccountId(submissionAccountId);
        administrationRequestGetAllParameters.setStatusId(statusId);
        return administrationRequestService.getAllAuthorized(loggedUser, administrationRequestGetAllParameters);
    }

    @GetMapping("/statuses")
    public List<AdministrationRequestStatus> getAllStatuses(@RequestHeader HttpHeaders headers) {
        Account loggedUser = authenticationHelpers.tryGetUser(headers);
        return administrationRequestStatusService.getAllAuthorized(loggedUser);
    }

    @PostMapping
    public void create(@RequestHeader HttpHeaders headers,
                       @Valid @RequestBody AdministrationRequestCreateDto administrationRequestCreateDto) {
        Account loggedUser = authenticationHelpers.tryGetUser(headers);
        AdministrationRequestCreateParameters administrationRequestCreateParameters = 
            new AdministrationRequestCreateParameters(accountService, administrationRequestTypeService);
        administrationRequestCreateParameters.setSubmissionAccountId(loggedUser.getAccountId());
        administrationRequestCreateDto.transferFieldsToHolder(administrationRequestCreateParameters);
        administrationRequestService.createAuthorized(loggedUser, administrationRequestCreateParameters);
    }

    @PutMapping("/{id}/status")
    public void updateStatus(@RequestHeader HttpHeaders headers,
                             @PathVariable Integer id,
                             @RequestBody String newStatusIdString) {
        Account loggedUser = authenticationHelpers.tryGetUser(headers);
        int newStatusId = ControllerArgumentsParseHelpers.getInteger(newStatusIdString);
        administrationRequestService.updateStatusAuthorized(loggedUser, id, newStatusId);
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers,
                       @PathVariable Integer id) {
        Account loggedUser = authenticationHelpers.tryGetUser(headers);
        administrationRequestService.deleteAuthorized(loggedUser, id);
    }
}
