package com.learnapp.learn.domains.lectures.dtos;

import com.learnapp.learn.domains.base_classes.interfaces.CreateParametersDto;
import com.learnapp.learn.domains.lectures.Lecture;
import com.learnapp.learn.domains.lectures.wrappers.LectureCreateParameters;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class LectureCreateDto implements CreateParametersDto<Lecture, LectureCreateParameters> {

    @NotNull
    private Integer courseId;

    @Size(min = 5, max = 50, message = "Title must be between 5 and 50 characters long.")
    @NotBlank
    private String title;

    @NotBlank
    private String videoLink;

    @Positive
    private Integer maxGrade;

    @Override
    public void transferFieldsToHolder(LectureCreateParameters lectureCreateParameters) {
        lectureCreateParameters.setCourseId(courseId);
        lectureCreateParameters.setTitle(title);
        lectureCreateParameters.setVideoLink(videoLink);
        lectureCreateParameters.setMaxGrade(maxGrade);
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    public Integer getMaxGrade() {
        return maxGrade;
    }

    public void setMaxGrade(Integer maxGrade) {
        this.maxGrade = maxGrade;
    }
}
