package com.learnapp.learn.domains.courses.interfaces;

import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.base_classes.interfaces.BaseService;
import com.learnapp.learn.domains.courses.Course;
import com.learnapp.learn.domains.courses.wrappers.CourseGetAllParameters;

import java.util.List;

public interface CourseService extends BaseService<Course> {

    List<Course> getAllAuthorized(Account loggedUser, CourseGetAllParameters courseGetAllParameters);

}
