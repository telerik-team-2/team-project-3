package com.learnapp.learn.domains.accounts;

import java.util.Locale;

import org.springframework.context.ApplicationEvent;

public class OnRegistrationCompleteEvent extends ApplicationEvent {
    
    private String appUrl;
    private Locale locale;
    private Account account;

    public OnRegistrationCompleteEvent(Account account,
                                       Locale locale,
                                       String appUrl) {
        super(account);

        this.appUrl = appUrl;
        this.locale = locale;
        this.account = account;
    }

    public String getAppUrl() {
        return appUrl;
    }

    public void setAppUrl(String appUrl) {
        this.appUrl = appUrl;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
