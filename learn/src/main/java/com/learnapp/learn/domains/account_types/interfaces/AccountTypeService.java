package com.learnapp.learn.domains.account_types.interfaces;

import com.learnapp.learn.domains.account_types.AccountType;
import com.learnapp.learn.domains.base_classes.interfaces.BaseGetService;

public interface AccountTypeService extends BaseGetService<AccountType> {
}
