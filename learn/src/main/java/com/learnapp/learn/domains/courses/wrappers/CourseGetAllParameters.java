package com.learnapp.learn.domains.courses.wrappers;

import com.learnapp.learn.domains.accounts.interfaces.AccountService;
import com.learnapp.learn.domains.base_classes.interfaces.BaseGetAllParameters;

import java.util.*;

public class CourseGetAllParameters implements BaseGetAllParameters {

    private static final Set<String> validSortParameters = new HashSet<>(Arrays.asList( "title", "rating"));
    private static final Set<String> validSortOrder = new HashSet<>(Arrays.asList("asc", "desc"));

    private final AccountService accountService;

    private String title;
    private Integer topicId;
    private Integer ownerAccountId;
    private Integer statusId;
    private List<String[]> sortParams;
    private Integer rating;
    private Integer enrolledAccountId;

    public CourseGetAllParameters(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public void transferQueryParameters(LinkedList<String> queryParameters,
                                        Map<String, Object> parameterKeysValues,
                                        List<String> sortParams) {
        if (title != null) {
            queryParameters.add("title like :title");
            parameterKeysValues.put("title", String.format("%%%s%%", title));
        }

        if (ownerAccountId != null) {
            queryParameters.add("ownerAccount.accountId = :ownerAccountId");
            parameterKeysValues.put("ownerAccountId", ownerAccountId);
        }

        if (topicId != null) {
            queryParameters.add("topic.topicId = :topicId");
            parameterKeysValues.put("topicId", topicId);
        }

        if (statusId != null) {
            queryParameters.add("status.statusId = :statusId");
            parameterKeysValues.put("statusId", statusId);
        }

        if (rating != null) {
            queryParameters.add("rating >= :rating");
            parameterKeysValues.put("rating", rating);
        }

        if (enrolledAccountId != null) {
            queryParameters.add(":enrolledAccount member of accountSet");
            parameterKeysValues.put("enrolledAccount", accountService.getById(enrolledAccountId));
        }

        if (this.sortParams != null) {
            for (String[] pair : this.sortParams) {
                String parameter = pair[0];
                String order = pair[1];

                if(validSortParameters.contains(parameter) && validSortOrder.contains(order)) {
                    sortParams.add(parameter + " " + order);
                }
            }
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getTopicId() {
        return topicId;
    }

    public void setTopicId(Integer topicId) {
        this.topicId = topicId;
    }

    public Integer getOwnerAccountId() {
        return ownerAccountId;
    }

    public void setOwnerAccountId(Integer ownerAccountId) {
        this.ownerAccountId = ownerAccountId;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public List<String[]> getSortParams() {
        return sortParams;
    }

    public void setSortParams(List<String[]> sortParams) {
        this.sortParams = sortParams;
    }

    public Integer getEnrolledAccountId() {
        return enrolledAccountId;
    }

    public void setEnrolledAccountId(Integer enrolledAccountId) {
        this.enrolledAccountId = enrolledAccountId;
    }
}
