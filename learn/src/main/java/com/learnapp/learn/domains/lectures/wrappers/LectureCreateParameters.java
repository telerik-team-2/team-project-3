package com.learnapp.learn.domains.lectures.wrappers;

import com.learnapp.learn.domains.base_classes.interfaces.CreateParametersHolder;
import com.learnapp.learn.domains.courses.interfaces.CourseService;
import com.learnapp.learn.domains.lectures.Lecture;

public class LectureCreateParameters implements CreateParametersHolder<Lecture> {

    private Integer courseId;
    private String title;
    private String videoLink;
    private Integer maxGrade;

    private final CourseService courseService;

    public LectureCreateParameters(CourseService courseService) {
        this.courseService = courseService;
    }

    @Override
    public Lecture createObjectFromParameters() {
        Lecture lecture = new Lecture();
        lecture.setCourse(courseService.getById(courseId));
        lecture.setTitle(title);
        lecture.setVideoLink(videoLink);
        lecture.setMaxGrade(maxGrade);
        return lecture;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    public Integer getMaxGrade() {
        return maxGrade;
    }

    public void setMaxGrade(Integer maxGrade) {
        this.maxGrade = maxGrade;
    }
}
