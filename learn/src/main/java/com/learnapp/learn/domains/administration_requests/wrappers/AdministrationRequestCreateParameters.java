package com.learnapp.learn.domains.administration_requests.wrappers;

import java.time.LocalDateTime;

import com.learnapp.learn.domains.accounts.interfaces.AccountService;
import com.learnapp.learn.domains.administration_request_types.interfaces.AdministrationRequestTypeService;
import com.learnapp.learn.domains.administration_requests.AdministrationRequest;
import com.learnapp.learn.domains.base_classes.interfaces.CreateParametersHolder;

public class AdministrationRequestCreateParameters implements CreateParametersHolder<AdministrationRequest> {

    private final AccountService accountService;
    private final AdministrationRequestTypeService administrationRequestTypeService;

    private Integer administrationRequestTypeId;
    private Integer submissionAccountId;

    public AdministrationRequestCreateParameters(AccountService accountService,
                                                 AdministrationRequestTypeService administrationRequestTypeService) {
        this.accountService = accountService;
        this.administrationRequestTypeService = administrationRequestTypeService;
    }

    @Override
    public AdministrationRequest createObjectFromParameters() {
        AdministrationRequest administrationRequest = new AdministrationRequest();
        administrationRequest.setAdministrationRequestType(administrationRequestTypeService.getById(administrationRequestTypeId));
        administrationRequest.setSubmissionAccount(accountService.getById(submissionAccountId));
        administrationRequest.setCreationDate(LocalDateTime.now());
        return administrationRequest;
    }

    public Integer getAdministrationRequestTypeId() {
        return administrationRequestTypeId;
    }

    public void setAdministrationRequestTypeId(Integer administrationRequestTypeId) {
        this.administrationRequestTypeId = administrationRequestTypeId;
    }

    public Integer getSubmissionAccountId() {
        return submissionAccountId;
    }

    public void setSubmissionAccountId(Integer submissionAccountId) {
        this.submissionAccountId = submissionAccountId;
    }
}
