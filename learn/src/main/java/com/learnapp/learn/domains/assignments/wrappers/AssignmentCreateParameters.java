package com.learnapp.learn.domains.assignments.wrappers;

import com.learnapp.learn.domains.accounts.interfaces.AccountService;
import com.learnapp.learn.domains.assignments.Assignment;
import com.learnapp.learn.domains.base_classes.interfaces.CreateParametersHolder;
import com.learnapp.learn.domains.lectures.interfaces.LectureService;

public class AssignmentCreateParameters implements CreateParametersHolder<Assignment> {

    private final AccountService accountService;
    private final LectureService lectureService;

    private Integer lectureId;
    private Integer accountId;

    public AssignmentCreateParameters(AccountService accountService,
                                      LectureService lectureService,
                                      int lectureId,
                                      int accountId) {
        this.accountService = accountService;
        this.lectureService = lectureService;
        this.lectureId = lectureId;
        this.accountId = accountId;
    }

    @Override
    public Assignment createObjectFromParameters() {
        Assignment assignment = new Assignment();
        assignment.setLecture(lectureService.getById(lectureId));
        assignment.setOwnerAccount(accountService.getById(accountId));
        return assignment;
    }

    public Integer getLectureId() {
        return lectureId;
    }

    public void setLectureId(Integer lectureId) {
        this.lectureId = lectureId;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }
}
