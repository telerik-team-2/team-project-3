package com.learnapp.learn.domains.base_classes.interfaces;

import com.learnapp.learn.domains.accounts.Account;

import java.util.List;

public interface BaseGetService<T> {

    List<T> getAllAuthorized(Account loggedUser);

    List<T> getAll();

    List<T> getAllAuthorized(Account loggedUser, BaseGetAllParameters baseGetAllParameters);

    List<T> getAll(BaseGetAllParameters baseGetAllParameters);

    T getByIdAuthorized(Account loggedUser, int objectId);

    T getById(int objectId);

}
