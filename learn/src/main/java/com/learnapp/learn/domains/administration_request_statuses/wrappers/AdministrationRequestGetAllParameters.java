package com.learnapp.learn.domains.administration_request_statuses.wrappers;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.learnapp.learn.domains.base_classes.interfaces.BaseGetAllParameters;

public class AdministrationRequestGetAllParameters implements BaseGetAllParameters {

    private Integer submissionAccountId;
    private Integer statusId;

    @Override
    public void transferQueryParameters(LinkedList<String> queryParameters, Map<String, Object> parameterKeysValues,
            List<String> sortParams) {
        
        if (submissionAccountId != null) {
            queryParameters.add("submissionAccount.accountId = :submissionAccountId");
            parameterKeysValues.put("submissionAccountId", submissionAccountId);
        }

        if (statusId != null) {
            queryParameters.add("status.statusId = :statusId");
            parameterKeysValues.put("statusId", statusId);
        }
        
        sortParams.add("creationDate asc");
    }

    public Integer getSubmissionAccountId() {
        return submissionAccountId;
    }

    public void setSubmissionAccountId(Integer submissionAccountId) {
        this.submissionAccountId = submissionAccountId;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }
}
