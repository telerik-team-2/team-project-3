package com.learnapp.learn.domains.courses.topics;

import com.learnapp.learn.domains.base_classes.BaseGetRepositoryImpl;
import com.learnapp.learn.domains.courses.topics.interfaces.TopicRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TopicRepositoryImpl extends BaseGetRepositoryImpl<Topic> implements TopicRepository {

    @Autowired
    public TopicRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory, Topic.class);
    }
}
