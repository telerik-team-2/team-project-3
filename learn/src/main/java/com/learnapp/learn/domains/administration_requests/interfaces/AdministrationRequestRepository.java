package com.learnapp.learn.domains.administration_requests.interfaces;

import com.learnapp.learn.domains.administration_requests.AdministrationRequest;
import com.learnapp.learn.domains.base_classes.interfaces.BaseRepository;

public interface AdministrationRequestRepository extends BaseRepository<AdministrationRequest> {
}
