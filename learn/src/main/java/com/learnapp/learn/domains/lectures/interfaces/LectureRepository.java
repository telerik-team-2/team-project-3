package com.learnapp.learn.domains.lectures.interfaces;

import com.learnapp.learn.domains.base_classes.interfaces.BaseRepository;
import com.learnapp.learn.domains.lectures.Lecture;

public interface LectureRepository extends BaseRepository<Lecture> {
}
