package com.learnapp.learn.domains.base_classes.interfaces;

public interface CreateParametersHolder<T> {

    T createObjectFromParameters();

}
