package com.learnapp.learn.domains.access_types;

import com.learnapp.learn.domains.access_types.interfaces.AccessTypeRepository;
import com.learnapp.learn.domains.access_types.interfaces.AccessTypeService;
import com.learnapp.learn.domains.base_classes.BaseGetServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccessTypeServiceImpl extends BaseGetServiceImpl<AccessType> implements AccessTypeService {

    @Autowired
    public AccessTypeServiceImpl(AccessTypeRepository accessTypeRepository) {
        super(accessTypeRepository);
    }
}
