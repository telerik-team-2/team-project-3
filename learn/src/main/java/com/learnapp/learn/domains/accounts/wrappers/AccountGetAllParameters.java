package com.learnapp.learn.domains.accounts.wrappers;

import com.learnapp.learn.domains.base_classes.interfaces.BaseGetAllParameters;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class AccountGetAllParameters implements BaseGetAllParameters {

    private Integer accountTypeId;
    private String searchAllString;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private Boolean verified;

    @Override
    public void transferQueryParameters(LinkedList<String> queryParameters, Map<String, Object> parameterKeysValues, List<String> sortParams) {
        if (accountTypeId != null) {
            queryParameters.add("accountType.accountTypeId = :accountTypeId");
            parameterKeysValues.put("accountTypeId", accountTypeId);
        }

        if(searchAllString != null) {
            String[] properties = {"firstName like :searchAllString",
                                   "lastName like :searchAllString",
                                   "email like :searchAllString"
                                };
            queryParameters.add(String.format("(%s)", String.join(" or ", properties)));
            parameterKeysValues.put("searchAllString", String.format("%%%s%%", searchAllString));
        } else {
            if (firstName != null) {
                queryParameters.add("firstName like :firstName");
                parameterKeysValues.put("firstName", String.format("%%%s%%", firstName));
            }

            if (lastName != null) {
                queryParameters.add("lastName like :lastName");
                parameterKeysValues.put("lastName", String.format("%%%s%%", lastName));
            }

            if (email != null) {
                queryParameters.add("email like :email");
                parameterKeysValues.put("email", String.format("%%%s%%", email));
            }
        }

        if (phoneNumber != null) {
            queryParameters.add("phoneNumber like :phoneNumber");
            parameterKeysValues.put("phoneNumber", String.format("%%%s%%", phoneNumber));
        }

        if (verified != null) {
            queryParameters.add("verified = :verified");
            parameterKeysValues.put("verified", verified);
        }
    }

    public Integer getAccountTypeId() {
        return accountTypeId;
    }

    public void setAccountTypeId(Integer accountTypeId) {
        this.accountTypeId = accountTypeId;
    }
    
    public String getSearchAllString() {
        return searchAllString;
    }

    public void setSearchAllString(String searchAllString) {
        this.searchAllString = searchAllString;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }
}
