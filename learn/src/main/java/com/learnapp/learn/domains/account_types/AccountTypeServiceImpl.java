package com.learnapp.learn.domains.account_types;

import com.learnapp.learn.domains.account_types.interfaces.AccountTypeRepository;
import com.learnapp.learn.domains.account_types.interfaces.AccountTypeService;
import com.learnapp.learn.domains.base_classes.BaseGetServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountTypeServiceImpl extends BaseGetServiceImpl<AccountType> implements AccountTypeService {

    @Autowired
    public AccountTypeServiceImpl(AccountTypeRepository accountTypeRepository) {
        super(accountTypeRepository);
    }
}
