package com.learnapp.learn.domains.lectures.wrappers;

import com.learnapp.learn.domains.base_classes.interfaces.BaseGetAllParameters;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class LectureGetAllParameters implements BaseGetAllParameters {

    @Override
    public void transferQueryParameters(LinkedList<String> queryParameters, Map<String, Object> parameterKeysValues, List<String> sortParams) {
        sortParams.add("lectureId asc");
    }
}
