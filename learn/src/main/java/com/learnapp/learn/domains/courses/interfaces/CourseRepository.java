package com.learnapp.learn.domains.courses.interfaces;

import com.learnapp.learn.domains.base_classes.interfaces.BaseRepository;
import com.learnapp.learn.domains.courses.Course;

public interface CourseRepository extends BaseRepository<Course> {

}
