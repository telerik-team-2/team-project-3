package com.learnapp.learn.domains.base_classes.interfaces;

public interface CreateParametersDto<T, C extends CreateParametersHolder<T>> {

    void transferFieldsToHolder(C createParametersHolder);

}
