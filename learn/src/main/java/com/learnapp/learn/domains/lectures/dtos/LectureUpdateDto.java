package com.learnapp.learn.domains.lectures.dtos;

import com.learnapp.learn.domains.base_classes.interfaces.UpdateParametersDto;
import com.learnapp.learn.domains.lectures.Lecture;
import com.learnapp.learn.domains.lectures.wrappers.LectureUpdateParameters;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class LectureUpdateDto implements UpdateParametersDto<Lecture, LectureUpdateParameters> {

    @Size(min = 5, max = 50, message = "Title must be between 5 and 50 characters long.")
    @NotBlank
    private String title;

    @NotBlank
    private String videoLink;

    @Positive
    private Integer maxGrade;

    @Size(max = 1000)
    private String lectureDescription;

    @Override
    public void transferFieldsToHolder(LectureUpdateParameters updateParametersHolder) {
        updateParametersHolder.setMaxGrade(maxGrade);
        updateParametersHolder.setTitle(title);
        updateParametersHolder.setVideoLink(videoLink);
        updateParametersHolder.setLectureDescription(lectureDescription);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    public Integer getMaxGrade() {
        return maxGrade;
    }

    public void setMaxGrade(Integer maxGrade) {
        this.maxGrade = maxGrade;
    }

    public String getLectureDescription() {
        return lectureDescription;
    }

    public void setLectureDescription(String lectureDescription) {
        this.lectureDescription = lectureDescription;
    }
}
