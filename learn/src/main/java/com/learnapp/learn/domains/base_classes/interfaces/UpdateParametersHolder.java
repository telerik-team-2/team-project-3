package com.learnapp.learn.domains.base_classes.interfaces;

public interface UpdateParametersHolder<T> {

    void updateObjectWithParameters(T objectToUpdate);

}
