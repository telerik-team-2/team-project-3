package com.learnapp.learn.domains.administration_request_types;

import com.learnapp.learn.domains.administration_request_types.interfaces.AdministrationRequestTypeRepository;
import com.learnapp.learn.domains.administration_request_types.interfaces.AdministrationRequestTypeService;
import com.learnapp.learn.domains.base_classes.BaseGetServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdministrationRequestTypeServiceImpl extends BaseGetServiceImpl<AdministrationRequestType> implements AdministrationRequestTypeService {

    @Autowired
    public AdministrationRequestTypeServiceImpl(AdministrationRequestTypeRepository administrationRequestTypeRepository) {
        super(administrationRequestTypeRepository);
    }
}
