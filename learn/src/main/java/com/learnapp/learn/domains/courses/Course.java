package com.learnapp.learn.domains.courses;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.courses.course_ratings.CourseRating;
import com.learnapp.learn.domains.lectures.Lecture;
import com.learnapp.learn.domains.courses.statuses.Status;
import com.learnapp.learn.domains.courses.topics.Topic;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "courses")
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "course_id")
    private Integer courseId;

    @ManyToOne
    @JoinColumn(name = "owner_account_id")
    private Account ownerAccount;

    @Column(name = "title")
    private String title;

    @ManyToOne
    @JoinColumn(name = "topic_id")
    private Topic topic;

    @ManyToOne
    @JoinColumn(name = "status_id")
    private Status status;

    @Column(name = "passing_grade")
    private Integer passingGrade;

    @Column(name = "starting_date")
    private LocalDate startingDate;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "course")
    private Set<CourseRating> courseRatings;

    @Column(name = "rating")
    private float currentRating;

    @OneToOne(mappedBy = "course")
    private CourseDescription courseDescription;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "course")
    private Set<Lecture> lectures;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "courses_accounts",
            joinColumns = @JoinColumn(name = "course_id"),
            inverseJoinColumns = @JoinColumn(name = "account_id"))
    private Set<Account> accountSet;

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public Account getOwnerAccount() {
        return ownerAccount;
    }

    public void setOwnerAccount(Account ownerAccount) {
        this.ownerAccount = ownerAccount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Integer getPassingGrade() {
        return passingGrade;
    }

    public void setPassingGrade(Integer passingGrade) {
        this.passingGrade = passingGrade;
    }

    public LocalDate getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(LocalDate startingDate) {
        this.startingDate = startingDate;
    }

    public Set<CourseRating> getCourseRatings() {
        return courseRatings;
    }

    public void setCourseRatings(Set<CourseRating> courseRatings) {
        this.courseRatings = courseRatings;
    }

    public Float getCurrentRating() {
        setCurrentRating();
        return currentRating;
    }

    public void setCurrentRating() {
        float average = 0;
        if (courseRatings != null && courseRatings.size() > 0) {
            for (CourseRating rating : courseRatings) {
                average += rating.getRating().getRatingId();
            }

            average = average / courseRatings.size();
        }

        this.currentRating = average;
    }

    public void setCurrentRating(Float currentRating) {
        this.currentRating = currentRating;
    }

    public CourseDescription getCourseDescription() {
        return courseDescription;
    }

    public void setCourseDescription(CourseDescription courseDescription) {
        this.courseDescription = courseDescription;
    }

    public Set<Lecture> getLectures() {
        return lectures;
    }

    public void setLectures(Set<Lecture> lectures) {
        this.lectures = lectures;
    }

    public Set<Account> getAccountSet() {
        return accountSet;
    }

    public void setAccountSet(Set<Account> accountSet) {
        this.accountSet = accountSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course course = (Course) o;
        return getCourseId().equals(course.getCourseId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCourseId());
    }
}
