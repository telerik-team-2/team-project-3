package com.learnapp.learn.domains.courses.dtos;

import com.learnapp.learn.domains.base_classes.interfaces.CreateParametersDto;
import com.learnapp.learn.domains.courses.Course;
import com.learnapp.learn.domains.courses.wrappers.CourseCreateParameters;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

public class CourseCreateDto implements CreateParametersDto<Course, CourseCreateParameters> {

    @Size(min = 5, max = 50)
    @NotBlank
    private String title;
    @NotNull
    private Integer topicId;
    @NotNull
    private Integer passingGrade;
    private LocalDate startingDate;

    @Override
    public void transferFieldsToHolder(CourseCreateParameters courseCreateParameters) {
        courseCreateParameters.setTitle(title);
        courseCreateParameters.setTopicId(topicId);
        courseCreateParameters.setPassingGrade(passingGrade);
        courseCreateParameters.setStartingDate(startingDate);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getTopicId() {
        return topicId;
    }

    public void setTopicId(Integer topicId) {
        this.topicId = topicId;
    }

    public Integer getPassingGrade() {
        return passingGrade;
    }

    public void setPassingGrade(Integer passingGrade) {
        this.passingGrade = passingGrade;
    }

    public LocalDate getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(LocalDate startingDate) {
        this.startingDate = startingDate;
    }
}
