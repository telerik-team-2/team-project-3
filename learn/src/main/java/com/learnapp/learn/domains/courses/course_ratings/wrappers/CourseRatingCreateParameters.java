package com.learnapp.learn.domains.courses.course_ratings.wrappers;

import com.learnapp.learn.domains.accounts.interfaces.AccountService;
import com.learnapp.learn.domains.base_classes.interfaces.CreateParametersHolder;
import com.learnapp.learn.domains.courses.course_ratings.CourseRating;
import com.learnapp.learn.domains.courses.interfaces.CourseService;
import com.learnapp.learn.domains.courses.ratings.interfaces.RatingService;

public class CourseRatingCreateParameters implements CreateParametersHolder<CourseRating> {

    private final CourseService courseService;
    private final AccountService accountService;
    private final RatingService ratingService;

    private Integer courseId;
    private Integer accountId;
    private Integer ratingId;

    public CourseRatingCreateParameters (CourseService courseService,
                                         AccountService accountService,
                                         RatingService ratingService) {
        this.courseService = courseService;
        this.accountService = accountService;
        this.ratingService = ratingService;
    }

    @Override
    public CourseRating createObjectFromParameters() {
        CourseRating courseRating = new CourseRating();
        courseRating.setCourse(courseService.getById(courseId));
        courseRating.setAccount(accountService.getById(accountId));
        courseRating.setRating(ratingService.getById(ratingId));

        return courseRating;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Integer getRatingId() {
        return ratingId;
    }

    public void setRatingId(Integer ratingId) {
        this.ratingId = ratingId;
    }
}
