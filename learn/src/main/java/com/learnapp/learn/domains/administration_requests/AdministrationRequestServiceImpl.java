package com.learnapp.learn.domains.administration_requests;

import java.util.List;

import com.learnapp.learn.domains.AuthorizationHelpers;
import com.learnapp.learn.domains.access_types.AccessType;
import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.administration_request_statuses.AdministrationRequestStatus;
import com.learnapp.learn.domains.administration_request_statuses.interfaces.AdministrationRequestStatusService;
import com.learnapp.learn.domains.administration_request_statuses.wrappers.AdministrationRequestGetAllParameters;
import com.learnapp.learn.domains.administration_requests.interfaces.AdministrationRequestRepository;
import com.learnapp.learn.domains.administration_requests.interfaces.AdministrationRequestService;
import com.learnapp.learn.domains.base_classes.BaseServiceImpl;
import com.learnapp.learn.exceptions.UnauthorizedAccessException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdministrationRequestServiceImpl extends BaseServiceImpl<AdministrationRequest> implements AdministrationRequestService {

    private final AdministrationRequestStatusService administrationRequestStatusService;

    @Autowired
    public AdministrationRequestServiceImpl(AdministrationRequestRepository administrationRequestRepository,
                                            AdministrationRequestStatusService administrationRequestStatusService) {
        super(administrationRequestRepository);
        this.administrationRequestStatusService = administrationRequestStatusService;
    }

    @Override
    public List<AdministrationRequest> getAllAuthorized(Account loggedUser, AdministrationRequestGetAllParameters administrationRequestGetAllParameters) {
        if (!AuthorizationHelpers.checkAccess(loggedUser, AccessType.ADMIN_ACCESS_ID)) {
            administrationRequestGetAllParameters.setSubmissionAccountId(loggedUser.getAccountId());
        }

        return super.getAllAuthorized(loggedUser, administrationRequestGetAllParameters);
    }

    @Override
    protected void configureCreatedObject(AdministrationRequest objectToCreate) {
        objectToCreate.setStatus(administrationRequestStatusService.getById(AdministrationRequestStatus.WAITING_STATUS_ID));
    }

    @Override
    public void updateStatusAuthorized(Account loggedUser, int id, int newStatusId) {
        if (!AuthorizationHelpers.checkAccess(loggedUser, AccessType.ADMIN_ACCESS_ID)) {
            throw new UnauthorizedAccessException(AuthorizationHelpers.UNAUTHORIZED_ACCESS);
        }

        AdministrationRequest administrationRequestToUpdate = getById(id);
        AdministrationRequestStatus newStatus = administrationRequestStatusService.getById(newStatusId);
        administrationRequestToUpdate.setStatus(newStatus);
        update(administrationRequestToUpdate);
    }

    @Override
    protected boolean checkDeleteAuthorization(Account loggedUser, AdministrationRequest objectToUpdate) {
        return AuthorizationHelpers.authorizeAccountAccess(loggedUser, objectToUpdate.getSubmissionAccount().getAccountId(), AccessType.ADMIN_ACCESS_ID);
    }
}
