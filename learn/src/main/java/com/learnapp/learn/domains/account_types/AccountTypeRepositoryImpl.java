package com.learnapp.learn.domains.account_types;

import com.learnapp.learn.domains.account_types.interfaces.AccountTypeRepository;
import com.learnapp.learn.domains.base_classes.BaseGetRepositoryImpl;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AccountTypeRepositoryImpl extends BaseGetRepositoryImpl<AccountType> implements AccountTypeRepository {

    @Autowired
    public AccountTypeRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory, AccountType.class);
    }
}
