package com.learnapp.learn.domains.courses.topics.interfaces;

import com.learnapp.learn.domains.base_classes.interfaces.BaseGetRepository;
import com.learnapp.learn.domains.courses.topics.Topic;

public interface TopicRepository extends BaseGetRepository<Topic> {
}
