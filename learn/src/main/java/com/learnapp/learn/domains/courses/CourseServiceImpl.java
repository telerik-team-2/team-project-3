package com.learnapp.learn.domains.courses;

import com.learnapp.learn.domains.AuthorizationHelpers;
import com.learnapp.learn.domains.access_types.AccessType;
import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.base_classes.BaseServiceImpl;
import com.learnapp.learn.domains.base_classes.interfaces.CreateParametersHolder;
import com.learnapp.learn.domains.courses.interfaces.CourseRepository;
import com.learnapp.learn.domains.courses.interfaces.CourseService;
import com.learnapp.learn.domains.courses.wrappers.CourseGetAllParameters;
import com.learnapp.learn.domains.courses.statuses.Status;
import com.learnapp.learn.domains.courses.statuses.interfaces.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseServiceImpl extends BaseServiceImpl<Course> implements CourseService {


    private final StatusService statusService;

    @Autowired
    public CourseServiceImpl(CourseRepository courseRepository,
                             StatusService statusService) {
        super(courseRepository);
        this.statusService = statusService;
    }

    @Override
    public List<Course> getAllAuthorized(Account loggedUser, CourseGetAllParameters courseGetAllParameters) {
        if(!AuthorizationHelpers.checkAccess(loggedUser, AccessType.TEACHER_ACCESS_ID)) {
            courseGetAllParameters.setStatusId(Status.PUBLISHED_COURSE_STATUS_ID);
        }
        return super.getAllAuthorized(loggedUser, courseGetAllParameters);
    }

    @Override
    protected boolean checkGetByIdPostGetAuthorization(Account loggedUser, Course objectToGet) {
        if (objectToGet.getStatus().getStatusId() != Status.PUBLISHED_COURSE_STATUS_ID) {
            return AuthorizationHelpers.checkAccess(loggedUser, AccessType.TEACHER_ACCESS_ID);
        }
        return true;
    }

    @Override
    protected boolean checkCreateAuthorization(Account loggedUser, CreateParametersHolder<Course> createParametersHolder) {
        return AuthorizationHelpers.checkAccess(loggedUser, AccessType.TEACHER_ACCESS_ID);
    }

    @Override
    protected void configureCreatedObject(Course courseToCreate) {
        courseToCreate.setStatus(statusService.getById(Status.DRAFT_COURSE_STATUS_ID));
    }

    @Override
    protected boolean checkUpdateAuthorization(Account loggedUser, Course courseToUpdate) {
        return AuthorizationHelpers.checkAccess(loggedUser, AccessType.TEACHER_ACCESS_ID);
    }

    @Override
    protected boolean checkDeleteAuthorization(Account loggedUser, Course courseToDelete) {
        return AuthorizationHelpers.checkAccess(loggedUser, AccessType.TEACHER_ACCESS_ID);
    }
}
