package com.learnapp.learn.domains.lectures.wrappers;

import com.learnapp.learn.domains.base_classes.interfaces.UpdateParametersHolder;
import com.learnapp.learn.domains.lectures.Lecture;
import com.learnapp.learn.domains.lectures.LectureDescription;

public class LectureUpdateParameters implements UpdateParametersHolder<Lecture> {

    private String title;
    private String videoLink;
    private Integer maxGrade;
    private String lectureDescription;

    @Override
    public void updateObjectWithParameters(Lecture lectureToUpdate) {
        lectureToUpdate.setMaxGrade(maxGrade);
        lectureToUpdate.setTitle(title);
        lectureToUpdate.setVideoLink(videoLink);
        if (this.lectureDescription == null) {
            lectureToUpdate.setLectureDescription(null);
            return;
        }

        LectureDescription lectureDescription = lectureToUpdate.getLectureDescription();
        if (lectureDescription == null) {
            lectureDescription = new LectureDescription();
            lectureToUpdate.setLectureDescription(lectureDescription);
            lectureDescription.setLecture(lectureToUpdate);
        }

        lectureDescription.setDescriptionContent(this.lectureDescription);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    public Integer getMaxGrade() {
        return maxGrade;
    }

    public void setMaxGrade(Integer maxGrade) {
        this.maxGrade = maxGrade;
    }

    public String getLectureDescription() {
        return lectureDescription;
    }

    public void setLectureDescription(String lectureDescription) {
        this.lectureDescription = lectureDescription;
    }
}
