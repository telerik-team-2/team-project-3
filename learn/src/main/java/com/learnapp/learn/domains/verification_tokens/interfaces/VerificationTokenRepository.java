package com.learnapp.learn.domains.verification_tokens.interfaces;

import com.learnapp.learn.domains.base_classes.interfaces.BaseRepository;
import com.learnapp.learn.domains.verification_tokens.VerificationToken;

public interface VerificationTokenRepository extends BaseRepository<VerificationToken> {

    VerificationToken getByValue(String value);
    
}
