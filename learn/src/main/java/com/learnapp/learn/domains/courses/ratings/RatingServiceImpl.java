package com.learnapp.learn.domains.courses.ratings;

import com.learnapp.learn.domains.base_classes.BaseGetServiceImpl;
import com.learnapp.learn.domains.courses.ratings.interfaces.RatingRepository;
import com.learnapp.learn.domains.courses.ratings.interfaces.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RatingServiceImpl extends BaseGetServiceImpl<Rating> implements RatingService {

    @Autowired
    public RatingServiceImpl(RatingRepository ratingRepository) {
        super(ratingRepository);
    }
}
