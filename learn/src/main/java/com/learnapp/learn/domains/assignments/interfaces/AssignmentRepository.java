package com.learnapp.learn.domains.assignments.interfaces;

import com.learnapp.learn.domains.assignments.Assignment;
import com.learnapp.learn.domains.base_classes.interfaces.BaseRepository;

public interface AssignmentRepository extends BaseRepository<Assignment> {

    Assignment getByLecture(int accountId, int lectureId);

}
