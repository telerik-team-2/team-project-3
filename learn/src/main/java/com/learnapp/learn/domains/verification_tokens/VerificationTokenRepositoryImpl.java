package com.learnapp.learn.domains.verification_tokens;

import com.learnapp.learn.domains.base_classes.BaseRepositoryImpl;
import com.learnapp.learn.domains.verification_tokens.interfaces.VerificationTokenRepository;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class VerificationTokenRepositoryImpl extends BaseRepositoryImpl<VerificationToken> implements VerificationTokenRepository {
    
    @Autowired
    public VerificationTokenRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory, VerificationToken.class);
    }

    @Override
    public VerificationToken getByValue(String value) {
        return getByField("value", value);
    }
}
