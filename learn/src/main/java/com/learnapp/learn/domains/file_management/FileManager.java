package com.learnapp.learn.domains.file_management;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;

import com.learnapp.learn.exceptions.EntityNotFoundException;
import com.learnapp.learn.exceptions.FileStorageException;

import org.springframework.stereotype.Component;

@Component
public class FileManager {

    private static final String IO_EXCEPTION_MESSAGE = "There was an error working with account data.";

    private static final String DATA_DIR_FILEPATH = "./data";
    private static final String DEFAULT_DATA_DIR_FILEPATH = "/default";
    private static final String ASSIGNMENTS_DIR_FILEPATH = "/assignments";

    private static final String PROFILE_PICTURE_FILENAME = "/profile_picture.jpg";
    //format: assignmentsDir/lecture_id.txt
    private static final String ASSIGNMENT_FILEPATH = "%s/%s.txt";

    public File getProfilePicture(int accountId) {
        return getProfilePictureFile(getAccountDir(accountId));
    }

    public File getDefaultProfilePicture() {
        return new File(String.format("%s%s%s", DATA_DIR_FILEPATH, DEFAULT_DATA_DIR_FILEPATH, PROFILE_PICTURE_FILENAME));
    }

    public void saveProfilePicture(int accountId, File pictureToSave) {
        File accountDir = getAccountDir(accountId);
        
        File profilePicture = getProfilePictureFile(accountDir);

        try (OutputStream outputStream = new FileOutputStream(profilePicture);
             InputStream inputStream = new FileInputStream(pictureToSave)) {
            byte[] inputBytes = inputStream.readAllBytes();
            outputStream.write(inputBytes);
        } catch (IOException e) {
            throw new FileStorageException(String.format("%s: %s", IO_EXCEPTION_MESSAGE, e.getMessage()));
        }
    }

    public void saveAssignment(int accountId, int lectureId, File assignmentToSave) {
        File assignmentsDir = getAssignmentsDir(getAccountDir(accountId));

        File assignment = new File(getAssignmentFileString(assignmentsDir, lectureId));

        try (FileWriter fileWriter = new FileWriter(assignment);
             FileReader fileReader = new FileReader(assignmentToSave)) {
            fileReader.transferTo(fileWriter);
        } catch (IOException e) {
            throw new FileStorageException(String.format("%s: %s", IO_EXCEPTION_MESSAGE, e.getMessage()));
        }
    }

    public boolean checkAssignmentExists(int accountId, int lectureId) {
        File assignment = getAssignmentUnchecked(accountId, lectureId);

        return Files.exists(assignment.toPath());
    }

    public File getAssignment(int accountId, int lectureId) {
        File assignment = getAssignmentUnchecked(accountId, lectureId);

        if (!Files.exists(assignment.toPath())) {
            throw new EntityNotFoundException("Assignment", "lecture", String.valueOf(lectureId));
        }
        return assignment;
    }

    private File getAssignmentUnchecked(int accountId, int lectureId) {
        File assignmentsDir = getAssignmentsDir(getAccountDir(accountId));
        return new File(getAssignmentFileString(assignmentsDir, lectureId));
    }

    private File getAssignmentsDir(File accountDir) {
        return new File(getAssignmentsDirString(accountDir));
    }

    private File getAccountDir(int accountId) {
        File accountDir = new File(getAccountDirString(accountId));
        if (!Files.exists(accountDir.toPath())) {
            createAccountDir(accountId);
        }
        return accountDir;
    }

    private void createAccountDir(int accountId) {
        File accountDir = new File(getAccountDirString(accountId));
        accountDir.mkdir();
        File assignmentsDir = new File(getAssignmentsDirString(accountDir));
        assignmentsDir.mkdir();
    }

    private File getProfilePictureFile(File accountDir) {
        return new File(getProfilePictureString(accountDir.getPath()));
    }

    private String getAssignmentFileString(File assignmentsDir, int lectureId) {
        return String.format(ASSIGNMENT_FILEPATH, assignmentsDir.getPath(), lectureId);
    }

    private String getProfilePictureString(String accountDir) {
        return String.format("%s%s", accountDir, PROFILE_PICTURE_FILENAME);
    }

    private String getAssignmentsDirString(File accountDir) {
        return String.format("%s%s", accountDir.getPath(), ASSIGNMENTS_DIR_FILEPATH);
    }

    private String getAccountDirString(int accountId) {
        return String.format("%s/%s", DATA_DIR_FILEPATH, accountId);
    }
}
