package com.learnapp.learn.domains.verification_tokens.interfaces;

import com.learnapp.learn.domains.base_classes.interfaces.BaseService;
import com.learnapp.learn.domains.verification_tokens.VerificationToken;

public interface VerificationTokenService extends BaseService<VerificationToken> {

    VerificationToken getByValue(String value);

}
