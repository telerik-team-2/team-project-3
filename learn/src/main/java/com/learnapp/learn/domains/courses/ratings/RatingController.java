package com.learnapp.learn.domains.courses.ratings;

import com.learnapp.learn.domains.courses.ratings.interfaces.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/course_ratings")
public class RatingController {

    private final RatingService ratingService;

    @Autowired
    public RatingController(RatingService ratingService) {
        this.ratingService = ratingService;
    }

    @GetMapping
    public List<Rating> getAll() {
        return ratingService.getAll()
                .stream()
                .sorted(Comparator.comparing(Rating::getRatingId))
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public Rating getById(@PathVariable int id) {
        return ratingService.getById(id);
    }

}
