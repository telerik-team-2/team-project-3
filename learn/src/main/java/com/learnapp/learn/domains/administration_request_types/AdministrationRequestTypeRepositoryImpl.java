package com.learnapp.learn.domains.administration_request_types;

import com.learnapp.learn.domains.administration_request_types.interfaces.AdministrationRequestTypeRepository;
import com.learnapp.learn.domains.base_classes.BaseGetRepositoryImpl;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AdministrationRequestTypeRepositoryImpl extends BaseGetRepositoryImpl<AdministrationRequestType> implements AdministrationRequestTypeRepository {

    @Autowired
    public AdministrationRequestTypeRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory, AdministrationRequestType.class);
    }
}
