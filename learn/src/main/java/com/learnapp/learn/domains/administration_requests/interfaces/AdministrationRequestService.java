package com.learnapp.learn.domains.administration_requests.interfaces;

import java.util.List;

import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.administration_request_statuses.wrappers.AdministrationRequestGetAllParameters;
import com.learnapp.learn.domains.administration_requests.AdministrationRequest;
import com.learnapp.learn.domains.base_classes.interfaces.BaseService;

public interface AdministrationRequestService extends BaseService<AdministrationRequest> {

    List<AdministrationRequest> getAllAuthorized(Account loggedUser, AdministrationRequestGetAllParameters administrationRequestGetAllParameters);

    void updateStatusAuthorized(Account loggedUser, int id, int newStatusId);

}
