package com.learnapp.learn.domains.administration_request_statuses.interfaces;

import com.learnapp.learn.domains.administration_request_statuses.AdministrationRequestStatus;
import com.learnapp.learn.domains.base_classes.interfaces.BaseGetRepository;

public interface AdministrationRequestStatusRepository extends BaseGetRepository<AdministrationRequestStatus> {
}
