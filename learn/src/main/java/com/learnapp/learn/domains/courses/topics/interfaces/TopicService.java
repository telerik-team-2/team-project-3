package com.learnapp.learn.domains.courses.topics.interfaces;

import com.learnapp.learn.domains.base_classes.interfaces.BaseGetService;
import com.learnapp.learn.domains.courses.topics.Topic;

public interface TopicService extends BaseGetService<Topic> {
}
