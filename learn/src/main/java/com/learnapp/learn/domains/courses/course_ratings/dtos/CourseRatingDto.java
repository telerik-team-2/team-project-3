package com.learnapp.learn.domains.courses.course_ratings.dtos;

import com.learnapp.learn.domains.base_classes.interfaces.CreateParametersDto;
import com.learnapp.learn.domains.courses.course_ratings.CourseRating;
import com.learnapp.learn.domains.courses.course_ratings.wrappers.CourseRatingCreateParameters;

import javax.validation.constraints.NotNull;

public class CourseRatingDto implements CreateParametersDto<CourseRating, CourseRatingCreateParameters> {

    @NotNull
    private Integer courseId;

    @NotNull
    private Integer accountId;

    @NotNull
    private Integer ratingId;

    @Override
    public void transferFieldsToHolder(CourseRatingCreateParameters courseRatingCreateParameters) {
        courseRatingCreateParameters.setCourseId(getCourseId());
        courseRatingCreateParameters.setAccountId(getAccountId());
        courseRatingCreateParameters.setRatingId(getRatingId());
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Integer getRatingId() {
        return ratingId;
    }

    public void setRatingId(Integer ratingId) {
        this.ratingId = ratingId;
    }
}
