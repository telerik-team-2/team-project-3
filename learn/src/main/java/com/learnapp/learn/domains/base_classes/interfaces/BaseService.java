package com.learnapp.learn.domains.base_classes.interfaces;

import com.learnapp.learn.domains.accounts.Account;

public interface BaseService<T> extends BaseGetService<T> {

    T createAuthorized(Account loggedUser, CreateParametersHolder<T> createParametersHolder);

    T create(CreateParametersHolder<T> createParametersHolder);

    void updateAuthorized(Account loggedUser, int objectId, UpdateParametersHolder<T> updateParametersHolder);

    void update(T objectToUpdate, UpdateParametersHolder<T> updateParametersHolder);

    void update(T objectToUpdate);

    void deleteAuthorized(Account loggedUser, int objectId);

    void delete(T objectToDelete);

}
