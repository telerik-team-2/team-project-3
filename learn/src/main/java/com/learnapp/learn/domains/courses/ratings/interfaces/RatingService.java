package com.learnapp.learn.domains.courses.ratings.interfaces;

import com.learnapp.learn.domains.base_classes.interfaces.BaseGetService;
import com.learnapp.learn.domains.courses.ratings.Rating;

public interface RatingService extends BaseGetService<Rating> {
}
