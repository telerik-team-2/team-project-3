package com.learnapp.learn.domains.administration_request_types.interfaces;

import com.learnapp.learn.domains.administration_request_types.AdministrationRequestType;
import com.learnapp.learn.domains.base_classes.interfaces.BaseGetRepository;

public interface AdministrationRequestTypeRepository extends BaseGetRepository<AdministrationRequestType> {
}
