package com.learnapp.learn.domains.courses.course_ratings.interfaces;


import com.learnapp.learn.domains.base_classes.interfaces.BaseRepository;
import com.learnapp.learn.domains.courses.course_ratings.CourseRating;

public interface CourseRatingRepository  extends BaseRepository<CourseRating> {
}
