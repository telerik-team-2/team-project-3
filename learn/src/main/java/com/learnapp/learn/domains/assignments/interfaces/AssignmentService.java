package com.learnapp.learn.domains.assignments.interfaces;

import java.io.File;
import java.util.List;

import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.assignments.Assignment;
import com.learnapp.learn.domains.base_classes.interfaces.BaseService;

public interface AssignmentService extends BaseService<Assignment> {

    List<Assignment> getByCourse(Account loggedUser, int courseId);

    Assignment getByLecture(Account loggedUser, int lectureId);

    File getFileAuthorized(Account loggedUser, int id);

    File getFile(Assignment assignment);

    Assignment updateFileAuthorized(Account loggedUser, int id, File assignmentToSave);

    void updateGradeAuthorized(Account loggedUser, int id, int grade);

}
