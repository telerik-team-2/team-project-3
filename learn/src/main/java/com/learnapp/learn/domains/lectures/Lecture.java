package com.learnapp.learn.domains.lectures;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.learnapp.learn.domains.courses.Course;

import javax.persistence.*;

@Entity
@Table(name = "lectures")
public class Lecture {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "lecture_id")
    private Integer lectureId;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "course_id")
    private Course course;

    @Column(name = "title")
    private String title;

    @Column(name = "video_link")
    private String videoLink;

    @Column(name = "max_grade")
    private Integer maxGrade;

    @OneToOne
    @JoinTable(name = "lecture_descriptions",
            joinColumns = @JoinColumn(name = "lecture_id"),
            inverseJoinColumns = @JoinColumn(name = "description_id"))
    private LectureDescription lectureDescription;

    public Integer getLectureId() {
        return lectureId;
    }

    public void setLectureId(Integer lectureId) {
        this.lectureId = lectureId;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVideoLink() {
        return videoLink;
    }

    public void setVideoLink(String videoLink) {
        this.videoLink = videoLink;
    }

    public Integer getMaxGrade() {
        return maxGrade;
    }

    public void setMaxGrade(Integer maxGrade) {
        this.maxGrade = maxGrade;
    }

    public LectureDescription getLectureDescription() {
        return lectureDescription;
    }

    public void setLectureDescription(LectureDescription lectureDescription) {
        this.lectureDescription = lectureDescription;
    }
}
