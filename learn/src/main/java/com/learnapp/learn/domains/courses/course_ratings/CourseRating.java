package com.learnapp.learn.domains.courses.course_ratings;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.courses.Course;
import com.learnapp.learn.domains.courses.ratings.Rating;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "courses_ratings")
public class CourseRating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "course_rating_id")
    private Integer ratingId;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "course_id")
    private Course course;

    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account;

    @ManyToOne
    @JoinColumn(name = "rating_id")
    private Rating rating;

    public Integer getCourseRatingId() {
        return ratingId;
    }

    public void setCourseRatingId(Integer ratingId) {
        this.ratingId = ratingId;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CourseRating that = (CourseRating) o;
        return getCourse().equals(that.getCourse()) && getAccount().equals(that.getAccount());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCourse(), getAccount());
    }
}
