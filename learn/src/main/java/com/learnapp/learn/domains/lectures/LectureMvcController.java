package com.learnapp.learn.domains.lectures;

import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.accounts.interfaces.AccountService;
import com.learnapp.learn.domains.assignments.Assignment;
import com.learnapp.learn.domains.assignments.interfaces.AssignmentService;
import com.learnapp.learn.domains.lectures.interfaces.LectureService;
import com.learnapp.learn.exceptions.EntityNotFoundException;
import com.learnapp.learn.exceptions.UnauthorizedAccessException;
import com.learnapp.learn.mvc.BaseMvcController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/lectures")
public class LectureMvcController extends BaseMvcController {
    
    private final LectureService lectureService;
    private final AssignmentService assignmentService;
    
    @Autowired
    public LectureMvcController(AccountService accountService,
                                LectureService lectureService,
                                AssignmentService assignmentService) {
        super(accountService);
        this.lectureService = lectureService;
        this.assignmentService = assignmentService;
    }

    @GetMapping("/{id}")
    public String getById(@RequestHeader HttpHeaders headers,
                   @PathVariable int id,
                   Model model) {
        if (!checkHasLoggedUser(model)) {
            return "redirect:/";
        }

        Account loggedUser = getLoggedUser(model);

        Lecture lecture;

        try {
            lecture = lectureService.getByIdAuthorized(loggedUser, id);
        } catch (UnauthorizedAccessException|EntityNotFoundException e) {
            return "redirect:/";
        }

        model.addAttribute("lecture", lecture);
        boolean isEnrolled = loggedUser.getCourseSet().contains(lecture.getCourse());
        model.addAttribute("isEnrolled", isEnrolled);
        if (isEnrolled) {
            Assignment assignment = assignmentService.getByLecture(loggedUser, lecture.getLectureId());
            model.addAttribute("assignment", assignment);
            model.addAttribute("assignmentGraded", assignment.getGrade() != null);
        }

        return "lecture-info";
    }
}
