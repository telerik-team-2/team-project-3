package com.learnapp.learn.domains;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.accounts.interfaces.AccountService;
import com.learnapp.learn.exceptions.InvalidLoginException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

@Component
public class AuthenticationHelpers {

    private static final int PASSWORD_MIN_LENGTH = 8;
    private static final int PASSWORD_MAX_LENGTH = 16;
    private static final Pattern PASSWORD_VALIDATION_REGEX = 
            Pattern.compile(String.format("^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%%&.<>^+=])(?=\\S+$).{%s,}$", PASSWORD_MIN_LENGTH));
    public static final String INVALID_PASSWORD_MESSAGE = "Invalid password format.";

    private static final String AUTHENTICATION_HEADER = "Authentication";
    private static final String EMAIL_NOT_VALIDATED = "The account's email address %s needs to be validated.";
    private static final String LOGIN_INVALID_CREDENTIALS = "Invalid email/password";

    private final AccountService accountService;

    @Autowired
    public AuthenticationHelpers(AccountService accountService) {
        this.accountService = accountService;
    }

    public Account tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHENTICATION_HEADER)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Missing Authentication header");
        }

        String emailAddress = headers.getFirst(AUTHENTICATION_HEADER);
        return tryGetUser(emailAddress);
    }

    public Account tryGetUser(String email, String password) {
        Account account = tryGetUser(email);

        if (!account.getPassword().equals(password)) {
            throw new InvalidLoginException(LOGIN_INVALID_CREDENTIALS);
        }

        return account;
    }

    public Account tryGetUser(String email) {
        Account loggedUser;
        loggedUser = accountService.getByEmail(email);

        if (!loggedUser.isVerified()) {
            throw new InvalidLoginException(String.format(EMAIL_NOT_VALIDATED, loggedUser.getEmail()));
        }
        return loggedUser;
    }

    public String hashPassword(String passwordRaw) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA3-256");
            final byte[] hashBytes = digest.digest(String.format("%s%s%s", "lkv", passwordRaw, "6fn").getBytes(StandardCharsets.UTF_8));
            return Base64.getEncoder().encodeToString(hashBytes);
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("Missing password hash algorithm.");
        }
    }

    public boolean checkPasswordValidity(String passwordToCheck) {
        int passwordLength = passwordToCheck.length();

        if (passwordLength > PASSWORD_MAX_LENGTH) {
            return false;
        }

        Matcher matcher = PASSWORD_VALIDATION_REGEX.matcher(passwordToCheck);
        return matcher.find();
    }
}
