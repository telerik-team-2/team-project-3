package com.learnapp.learn.domains.assignments.wrappers;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.learnapp.learn.domains.base_classes.interfaces.BaseGetAllParameters;

public class AssignmentGetAllParameters implements BaseGetAllParameters {

    private Integer courseId;
    private Integer lectureId;
    private Integer ownerAccountId;

    private Integer gradeMin;
    private Integer gradeMax;
    private Boolean graded;

    @Override
    public void transferQueryParameters(LinkedList<String> queryParameters,
                                        Map<String, Object> parameterKeysValues,
                                        List<String> sortParams) {
        if (courseId != null) {
            queryParameters.add("lecture.course.courseId = :courseId");
            parameterKeysValues.put("courseId", courseId);
        }

        if (lectureId != null) {
            queryParameters.add("lecture.lectureId = :lectureId");
            parameterKeysValues.put("lectureId", lectureId);
        }

        if (ownerAccountId != null) {
            queryParameters.add("ownerAccount.accountId = :ownerAccountId");
            parameterKeysValues.put("ownerAccountId", ownerAccountId);
        }

        if (graded != null) {
            queryParameters.add("grade is " + (graded ? "not null" : "null"));
            if (!graded) {
                return;
            }
        }

        if (gradeMin != null) {
            queryParameters.add("grade >= :gradeMin");
            parameterKeysValues.put("gradeMin", gradeMin);
        }

        if (gradeMax != null) {
            queryParameters.add("grade <= :gradeMax");
            parameterKeysValues.put("gradeMax", gradeMax);
        }
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public Integer getLectureId() {
        return lectureId;
    }

    public void setLectureId(Integer lectureId) {
        this.lectureId = lectureId;
    }

    public Integer getOwnerAccountId() {
        return ownerAccountId;
    }

    public void setOwnerAccountId(Integer ownerAccountId) {
        this.ownerAccountId = ownerAccountId;
    }

    public Integer getGradeMin() {
        return gradeMin;
    }

    public void setGradeMin(Integer gradeMin) {
        this.gradeMin = gradeMin;
    }

    public Integer getGradeMax() {
        return gradeMax;
    }

    public void setGradeMax(Integer gradeMax) {
        this.gradeMax = gradeMax;
    }

    public Boolean getGraded() {
        return graded;
    }

    public void setGraded(Boolean graded) {
        this.graded = graded;
    }
}
