package com.learnapp.learn.domains.accounts;

import java.util.UUID;

import com.learnapp.learn.domains.verification_tokens.interfaces.VerificationTokenService;
import com.learnapp.learn.domains.verification_tokens.wrappers.VerificationTokenCreateParameters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {
    

    private final VerificationTokenService verificationTokenService;
    private final JavaMailSender javaMailSender;

    @Autowired
    public RegistrationListener(VerificationTokenService verificationTokenService,
                                JavaMailSender javaMailSender) {
        this.verificationTokenService = verificationTokenService;
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void onApplicationEvent(OnRegistrationCompleteEvent event) {
        this.confirmRegistration(event);
    }

    private void confirmRegistration(OnRegistrationCompleteEvent event) {
        Account account = event.getAccount();
        String token = UUID.randomUUID().toString();
        VerificationTokenCreateParameters verificationTokenCreateParameters = 
            new VerificationTokenCreateParameters(token, account);
        verificationTokenService.create(verificationTokenCreateParameters);

        String recipientAddress = account.getEmail();
        String subject = "Registration confirmation";
        String confirmationUrl 
            = event.getAppUrl() + "/registrationConfirm?token=" + token;
        String message =
            String.format("Registration successful%n" +
                          "Use the link below to verify your email:%n");

        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(recipientAddress);
        email.setSubject(subject);
        email.setText(String.format("%shttp://localhost:8080%s", message, confirmationUrl));
        javaMailSender.send(email);
    }
}
