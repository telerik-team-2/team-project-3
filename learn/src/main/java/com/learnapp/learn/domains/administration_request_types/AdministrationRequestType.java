package com.learnapp.learn.domains.administration_request_types;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "administration_request_types")
public class AdministrationRequestType {

    public static final int TEACHER_APPROVAL_ID = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "request_type_id")
    private Integer requestTypeId;

    @Column(name = "name")
    private String name;

    public Integer getRequestTypeId() {
        return requestTypeId;
    }

    public void setRequestTypeId(Integer requestTypeId) {
        this.requestTypeId = requestTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AdministrationRequestType that = (AdministrationRequestType) o;
        return getRequestTypeId().equals(that.getRequestTypeId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRequestTypeId());
    }
}
