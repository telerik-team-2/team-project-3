package com.learnapp.learn.domains.base_classes;

import com.learnapp.learn.domains.base_classes.interfaces.BaseGetRepository;
import com.learnapp.learn.domains.base_classes.interfaces.BaseGetAllParameters;
import com.learnapp.learn.exceptions.EntityNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.*;

public class BaseGetRepositoryImpl<T> implements BaseGetRepository<T> {

    private final SessionFactory sessionFactory;
    private final Class<T> objectClass;

    public BaseGetRepositoryImpl(SessionFactory sessionFactory,
                                 Class<T> objectClass) {
        this.sessionFactory = sessionFactory;
        this.objectClass = objectClass;
    }

    protected SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    @Override
    public List<T> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery(String.format("from %s", objectClass.getSimpleName()), objectClass).list();
        }
    }

    @Override
    public List<T> getAll(BaseGetAllParameters baseGetAllParameters) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder(String.format("from %s", objectClass.getSimpleName()));

            LinkedList<String> queryParameters = new LinkedList<>();
            Map<String, Object> parameterKeysValues = new HashMap<>();
            List<String> sortParams = new LinkedList<>();

            baseGetAllParameters.transferQueryParameters(queryParameters, parameterKeysValues, sortParams);

            if (!queryParameters.isEmpty()) {
                queryString.append(String.format(" where %s", String.join(" and ", queryParameters)));
            }

            if(!sortParams.isEmpty()) {
                queryString.append(String.format(" order by %s", String.join(", ", sortParams)));
            }

            Query<T> query = session.createQuery(queryString.toString(), objectClass);

            query.setProperties(parameterKeysValues);

            return query.list();
        }
    }

    @Override
    public T getById(int objectId) {
        try (Session session = sessionFactory.openSession()) {
            T objectToReturn = session.get(objectClass, objectId);

            if (objectToReturn == null) {
                throw new EntityNotFoundException(objectClass.getSimpleName(), "id", String.valueOf(objectId));
            }

            return objectToReturn;
        }
    }

    @Override
    public T getByField(String fieldName, Object fieldValue) {
        List<T> result = getObjectsByField(fieldName, fieldValue);
        if (result.isEmpty()) {
            throw new EntityNotFoundException(objectClass.getSimpleName(), fieldName, fieldValue.toString());
        }
        return result.get(0);
    }

    @Override
    public boolean checkEntityExists(String fieldName, Object fieldValue) {
        return !getObjectsByField(fieldName, fieldValue).isEmpty();
    }

    private List<T> getObjectsByField(String fieldName, Object fieldValue) {
        try (Session session = sessionFactory.openSession()) {
            Query<T> query = session.createQuery(String.format("from %s where %s = :%s", objectClass.getSimpleName(), fieldName, fieldName), objectClass);
            query.setParameter(fieldName, fieldValue);
            return query.list();
        }
    }
}
