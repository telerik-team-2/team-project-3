package com.learnapp.learn.domains.accounts.interfaces;

import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.base_classes.interfaces.BaseRepository;

public interface AccountRepository extends BaseRepository<Account> {

    Account getByEmail(String emailAddress);

    boolean checkEmailExists(String emailAddress);

}
