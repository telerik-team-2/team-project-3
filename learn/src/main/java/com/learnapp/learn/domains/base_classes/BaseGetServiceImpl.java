package com.learnapp.learn.domains.base_classes;

import com.learnapp.learn.domains.AuthorizationHelpers;
import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.base_classes.interfaces.BaseGetRepository;
import com.learnapp.learn.domains.base_classes.interfaces.BaseGetService;
import com.learnapp.learn.domains.base_classes.interfaces.BaseGetAllParameters;
import com.learnapp.learn.exceptions.UnauthorizedAccessException;

import java.util.List;

public class BaseGetServiceImpl<T> implements BaseGetService<T> {

    private final BaseGetRepository<T> baseGetRepository;

    public BaseGetServiceImpl(BaseGetRepository<T> baseGetRepository) {
        this.baseGetRepository = baseGetRepository;
    }

    @Override
    public List<T> getAllAuthorized(Account loggedUser) {
        if (!checkGetAllAuthorization(loggedUser)) {
            throw new UnauthorizedAccessException(AuthorizationHelpers.UNAUTHORIZED_ACCESS);
        }
        return getAll();
    }

    @Override
    public List<T> getAll() {
        return baseGetRepository.getAll();
    }

    @Override
    public List<T> getAllAuthorized(Account loggedUser, BaseGetAllParameters baseGetAllParameters) {
        if (!checkGetAllAuthorization(loggedUser)) {
            throw new UnauthorizedAccessException(AuthorizationHelpers.UNAUTHORIZED_ACCESS);
        }
        return getAll(baseGetAllParameters);
    }

    protected boolean checkGetAllAuthorization(Account loggedUser) {
        return true;
    }

    @Override
    public List<T> getAll(BaseGetAllParameters baseGetAllParameters) {
        return baseGetRepository.getAll(baseGetAllParameters);
    }

    @Override
    public T getByIdAuthorized(Account loggedUser, int objectId) {
        if (!checkGetByIdAuthorization(loggedUser, objectId)) {
            throw new UnauthorizedAccessException(AuthorizationHelpers.UNAUTHORIZED_ACCESS);
        }
        T objectToGet = getById(objectId);
        if (!checkGetByIdPostGetAuthorization(loggedUser, objectToGet)) {
            throw new UnauthorizedAccessException(AuthorizationHelpers.UNAUTHORIZED_ACCESS);
        }
        return objectToGet;
    }

    protected boolean checkGetByIdAuthorization(Account loggedUser, int objectId) {
        return true;
    }

    protected boolean checkGetByIdPostGetAuthorization(Account loggedUser, T objectToGet) {
        return true;
    }

    @Override
    public T getById(int objectId) {
        return baseGetRepository.getById(objectId);
    }
}
