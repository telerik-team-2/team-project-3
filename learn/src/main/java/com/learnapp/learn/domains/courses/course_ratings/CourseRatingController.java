package com.learnapp.learn.domains.courses.course_ratings;

import com.learnapp.learn.domains.AuthenticationHelpers;
import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.accounts.interfaces.AccountService;
import com.learnapp.learn.domains.courses.course_ratings.dtos.CourseRatingDto;
import com.learnapp.learn.domains.courses.course_ratings.interfaces.CourseRatingService;
import com.learnapp.learn.domains.courses.course_ratings.wrappers.CourseRatingCreateParameters;
import com.learnapp.learn.domains.courses.interfaces.CourseService;
import com.learnapp.learn.domains.courses.ratings.interfaces.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/courses_ratings")
public class CourseRatingController {

    private final AuthenticationHelpers authenticationHelpers;
    private final CourseRatingService courseRatingService;
    private final CourseService courseService;
    private final AccountService accountService;
    private final RatingService ratingService;

    @Autowired
    public CourseRatingController(CourseRatingService courseRatingService,
                                  AuthenticationHelpers authenticationHelpers,
                                  CourseService courseService,
                                  AccountService accountService,
                                  RatingService ratingService) {
        this.courseRatingService = courseRatingService;
        this.authenticationHelpers = authenticationHelpers;
        this.courseService = courseService;
        this.accountService = accountService;
        this.ratingService = ratingService;
    }

    @GetMapping
    public List<CourseRating> getAll() {
        return courseRatingService.getAll();
    }

    @GetMapping("/{id}")
    public CourseRating getById(@PathVariable int id) {
        return courseRatingService.getById(id);
    }

    @PostMapping
    public void create(@RequestHeader HttpHeaders headers,
                       @Valid @RequestBody CourseRatingDto courseRatingDto){
        Account loggedUser = authenticationHelpers.tryGetUser(headers);
        CourseRatingCreateParameters courseRatingCreateParameters = new CourseRatingCreateParameters(courseService, accountService, ratingService);
        courseRatingDto.transferFieldsToHolder(courseRatingCreateParameters);
        courseRatingService.createAuthorized(loggedUser, courseRatingCreateParameters);
    }
}
