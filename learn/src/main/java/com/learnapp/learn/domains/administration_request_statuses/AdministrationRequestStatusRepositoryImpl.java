package com.learnapp.learn.domains.administration_request_statuses;

import com.learnapp.learn.domains.administration_request_statuses.interfaces.AdministrationRequestStatusRepository;
import com.learnapp.learn.domains.base_classes.BaseGetRepositoryImpl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AdministrationRequestStatusRepositoryImpl extends BaseGetRepositoryImpl<AdministrationRequestStatus> implements AdministrationRequestStatusRepository {
    
    @Autowired
    public AdministrationRequestStatusRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory, AdministrationRequestStatus.class);
    }
}
