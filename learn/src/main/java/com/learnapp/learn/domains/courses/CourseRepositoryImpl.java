package com.learnapp.learn.domains.courses;

import com.learnapp.learn.domains.base_classes.BaseRepositoryImpl;
import com.learnapp.learn.domains.courses.interfaces.CourseRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CourseRepositoryImpl extends BaseRepositoryImpl<Course> implements CourseRepository {

    @Autowired
    public CourseRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory, Course.class);
    }

    @Override
    protected void configureUpdatePrerequisites(Course objectToUpdate, Session session) {
        CourseDescription courseDescription = objectToUpdate.getCourseDescription();
        if (courseDescription != null) {
            session.saveOrUpdate(courseDescription);
        }
    }
}
