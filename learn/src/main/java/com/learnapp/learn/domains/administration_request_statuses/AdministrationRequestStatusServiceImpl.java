package com.learnapp.learn.domains.administration_request_statuses;

import com.learnapp.learn.domains.AuthorizationHelpers;
import com.learnapp.learn.domains.access_types.AccessType;
import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.administration_request_statuses.interfaces.AdministrationRequestStatusRepository;
import com.learnapp.learn.domains.administration_request_statuses.interfaces.AdministrationRequestStatusService;
import com.learnapp.learn.domains.base_classes.BaseGetServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdministrationRequestStatusServiceImpl extends BaseGetServiceImpl<AdministrationRequestStatus> implements AdministrationRequestStatusService {
    
    @Autowired
    public AdministrationRequestStatusServiceImpl(AdministrationRequestStatusRepository administrationRequestStatusRepository) {
        super(administrationRequestStatusRepository);
    }

    @Override
    protected boolean checkGetAllAuthorization(Account loggedUser) {
        return AuthorizationHelpers.checkAccess(loggedUser, AccessType.ADMIN_ACCESS_ID);
    }
}
