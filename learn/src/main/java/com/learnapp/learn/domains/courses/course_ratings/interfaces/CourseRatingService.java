package com.learnapp.learn.domains.courses.course_ratings.interfaces;

import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.base_classes.interfaces.BaseService;
import com.learnapp.learn.domains.courses.Course;
import com.learnapp.learn.domains.courses.course_ratings.CourseRating;
import com.learnapp.learn.domains.courses.course_ratings.wrappers.CourseRatingCreateParameters;


public interface CourseRatingService extends BaseService<CourseRating> {

    boolean checkCourseCompleted(Account account, Course course);

    boolean checkRatingExists(Account account, Course course);

    CourseRating getAccountCourseRating(Account account, Course course);

    CourseRating createAuthorized(Account loggedUser, CourseRatingCreateParameters courseRatingCreateParameters);

}
