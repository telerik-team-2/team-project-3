package com.learnapp.learn.domains.courses.statuses.interfaces;

import com.learnapp.learn.domains.base_classes.interfaces.BaseGetRepository;
import com.learnapp.learn.domains.courses.statuses.Status;

public interface StatusRepository extends BaseGetRepository<Status> {
}
