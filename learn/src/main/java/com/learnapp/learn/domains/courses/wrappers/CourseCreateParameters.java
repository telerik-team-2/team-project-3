package com.learnapp.learn.domains.courses.wrappers;

import com.learnapp.learn.domains.accounts.interfaces.AccountService;
import com.learnapp.learn.domains.base_classes.interfaces.CreateParametersHolder;
import com.learnapp.learn.domains.courses.course_ratings.CourseRating;
import com.learnapp.learn.domains.courses.Course;
import com.learnapp.learn.domains.courses.topics.interfaces.TopicService;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class CourseCreateParameters implements CreateParametersHolder<Course> {

    private final TopicService topicService;
    private final AccountService accountService;

    private String title;
    private Integer topicId;
    private Integer passingGrade;
    private LocalDate startingDate;
    private Set<CourseRating> courseRatings;
    private int ownerAccountId;

    public CourseCreateParameters(TopicService topicService, AccountService accountService) {
        this.topicService = topicService;
        this.accountService = accountService;
    }

    @Override
    public Course createObjectFromParameters() {
        Course courseNew = new Course();
        courseNew.setTitle(title);
        courseNew.setTopic(topicService.getById(topicId));
        courseNew.setCourseRatings(new HashSet<>());
        courseNew.setStartingDate(startingDate);
        courseNew.setOwnerAccount(accountService.getById(ownerAccountId));
        courseNew.setPassingGrade(passingGrade);
        return courseNew;
    }

    public TopicService getTopicService() {
        return topicService;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getTopicId() {
        return topicId;
    }

    public void setTopicId(Integer topicId) {
        this.topicId = topicId;
    }

    public Integer getPassingGrade() {
        return passingGrade;
    }

    public void setPassingGrade(Integer passingGrade) {
        this.passingGrade = passingGrade;
    }

    public Set<CourseRating> getCourseRatings() {
        return courseRatings;
    }

    public void setCourseRatings(Set<CourseRating> courseRatings) {
        this.courseRatings = courseRatings;
    }

    public LocalDate getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(LocalDate startingDate) {
        this.startingDate = startingDate;
    }

    public int getOwnerAccountId() {
        return ownerAccountId;
    }

    public void setOwnerAccountId(int ownerAccountId) {
        this.ownerAccountId = ownerAccountId;
    }
}
