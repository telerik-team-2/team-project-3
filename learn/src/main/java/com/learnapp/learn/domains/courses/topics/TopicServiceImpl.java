package com.learnapp.learn.domains.courses.topics;

import com.learnapp.learn.domains.base_classes.BaseGetServiceImpl;
import com.learnapp.learn.domains.courses.topics.interfaces.TopicRepository;
import com.learnapp.learn.domains.courses.topics.interfaces.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TopicServiceImpl extends BaseGetServiceImpl<Topic> implements TopicService {

    @Autowired
    public TopicServiceImpl(TopicRepository topicRepository) {
        super(topicRepository);
    }
}
