package com.learnapp.learn.domains.base_classes.interfaces;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public interface BaseGetAllParameters {

    /**
     * Method that transfers filter parameters from holder to 
     * collections used to construct the getAll query.
     * @param queryParameters list of filter parameters, e.g. "parameter = :parameterValue"
     * @param parameterKeysValues list of filter values, e.g. ("parameterValue", parameterValue)
     * @param sortParams list of sorting modifiers, e.g. "date desc" or "id asc"
     */
    void transferQueryParameters(LinkedList<String> queryParameters,
                                 Map<String, Object> parameterKeysValues,
                                 List<String> sortParams);
}
