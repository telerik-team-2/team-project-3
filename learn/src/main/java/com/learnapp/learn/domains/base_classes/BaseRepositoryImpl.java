package com.learnapp.learn.domains.base_classes;

import com.learnapp.learn.domains.base_classes.interfaces.BaseRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

abstract public class BaseRepositoryImpl<T> extends BaseGetRepositoryImpl<T> implements BaseRepository<T> {

    public BaseRepositoryImpl(SessionFactory sessionFactory,
                              Class<T> objectClass) {
        super(sessionFactory, objectClass);
    }

    @Override
    public void create(T objectToCreate) {
        try (Session session = getSessionFactory().openSession()) {
            session.beginTransaction();
            session.save(objectToCreate);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(T objectToUpdate) {
        try (Session session = getSessionFactory().openSession()) {
            session.beginTransaction();
            configureUpdatePrerequisites(objectToUpdate, session);
            session.update(objectToUpdate);
            session.getTransaction().commit();
        }
    }

    protected void configureUpdatePrerequisites(T objectToUpdate, Session session){}

    @Override
    public void delete(T objectToDelete) {
        try (Session session = getSessionFactory().openSession()) {
            session.beginTransaction();
            session.delete(objectToDelete);
            session.getTransaction().commit();
        }
    }
}
