package com.learnapp.learn.domains.courses.statuses.interfaces;

import com.learnapp.learn.domains.base_classes.interfaces.BaseGetService;
import com.learnapp.learn.domains.courses.statuses.Status;

public interface StatusService extends BaseGetService<Status> {
}
