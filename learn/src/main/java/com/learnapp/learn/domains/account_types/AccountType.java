package com.learnapp.learn.domains.account_types;

import javax.persistence.*;

@Entity
@Table(name = "account_types")
public class AccountType {

    public static final int STUDENT_ID = 1;
    public static final int TEACHER_ID = 2;
    public static final int ADMIN_ID = 3;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "account_type_id")
    private Integer accountTypeId;

    @Column(name = "name")
    private String name;

    public AccountType() {
    }

    public Integer getAccountTypeId() {
        return accountTypeId;
    }

    public void setAccountTypeId(Integer accountTypeId) {
        this.accountTypeId = accountTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
