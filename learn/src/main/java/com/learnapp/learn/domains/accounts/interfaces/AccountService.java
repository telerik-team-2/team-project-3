package com.learnapp.learn.domains.accounts.interfaces;

import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.accounts.wrappers.AccountCreateParameters;
import com.learnapp.learn.domains.base_classes.interfaces.BaseService;
import com.learnapp.learn.domains.courses.Course;
import com.learnapp.learn.domains.verification_tokens.VerificationToken;

import java.io.File;
import java.util.List;

public interface AccountService extends BaseService<Account> {

    Account getByEmail(String emailAddress);

    File getProfilePictureAuthorized(Account loggedUser, int id);

    Account create(AccountCreateParameters accountCreateParameters);

    Course enrollCourse(Account loggedUser, int id, int courseId);

    void addAccessTypes(Account loggedUser, int id, List<Integer> accessTypeIdList);

    void updatePassword(Account loggedUser, int id, String newPassword);

    void updateAccountType(Account loggedUser, int id, int accountTypeId);

    void updateProfilePictureAuthorized(Account loggedUser, int id, File newPicture);

    void verify(VerificationToken verificationToken);

    void deleteAccessTypes(Account loggedUser, int id, List<Integer> accessTypeIdList);

}
