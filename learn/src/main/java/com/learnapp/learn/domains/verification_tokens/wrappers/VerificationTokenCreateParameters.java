package com.learnapp.learn.domains.verification_tokens.wrappers;

import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.base_classes.interfaces.CreateParametersHolder;
import com.learnapp.learn.domains.verification_tokens.VerificationToken;

public class VerificationTokenCreateParameters implements CreateParametersHolder<VerificationToken> {

    private String value;
    private Account account;

    public VerificationTokenCreateParameters(String value,
                                             Account account) {
        this.value = value;
        this.account = account;
    }

    @Override
    public VerificationToken createObjectFromParameters() {
        VerificationToken verificationToken = new VerificationToken();
        verificationToken.setValue(value);
        verificationToken.setAccount(account);
        return verificationToken;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
