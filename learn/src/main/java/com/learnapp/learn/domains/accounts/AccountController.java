package com.learnapp.learn.domains.accounts;

import com.learnapp.learn.domains.AuthenticationHelpers;
import com.learnapp.learn.domains.account_types.AccountType;
import com.learnapp.learn.domains.account_types.interfaces.AccountTypeService;
import com.learnapp.learn.domains.accounts.dtos.AccountCreateDto;
import com.learnapp.learn.domains.accounts.dtos.AccountUpdateDto;
import com.learnapp.learn.domains.accounts.interfaces.AccountService;
import com.learnapp.learn.domains.accounts.wrappers.AccountCreateParameters;
import com.learnapp.learn.domains.accounts.wrappers.AccountGetAllParameters;
import com.learnapp.learn.domains.accounts.wrappers.AccountUpdateParameters;
import com.learnapp.learn.domains.assignments.interfaces.AssignmentService;
import com.learnapp.learn.domains.assignments.wrappers.AssignmentCreateParameters;
import com.learnapp.learn.domains.courses.Course;
import com.learnapp.learn.domains.lectures.Lecture;
import com.learnapp.learn.domains.lectures.interfaces.LectureService;
import com.learnapp.learn.utilities.ControllerArgumentsParseHelpers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/accounts")
public class AccountController {

    private final AuthenticationHelpers authenticationHelpers;
    private final ApplicationEventPublisher applicationEventPublisher;

    private final AccountService accountService;
    private final AccountTypeService accountTypeService;
    private final AssignmentService assignmentService;
    private final LectureService lectureService;

    @Autowired
    public AccountController(AuthenticationHelpers authenticationHelpers,
                             ApplicationEventPublisher applicationEventPublisher,
                             AccountService accountService,
                             AccountTypeService accountTypeService,
                             AssignmentService assignmentService,
                             LectureService lectureService) {
        this.authenticationHelpers = authenticationHelpers;
        this.applicationEventPublisher = applicationEventPublisher;
        this.accountService = accountService;
        this.accountTypeService = accountTypeService;
        this.assignmentService = assignmentService;
        this.lectureService = lectureService;
    }

    //TODO complete sorting
    @GetMapping
    public List<Account> getAll(@RequestHeader HttpHeaders headers,
                                @RequestParam(name = "accountTypeId", required = false) Integer accountTypeId,
                                @RequestParam(name = "searchAllString", required = false) String searchAllString,
                                @RequestParam(name = "firstName", required = false) String firstName,
                                @RequestParam(name = "lastName", required = false) String lastName,
                                @RequestParam(name = "email", required = false) String email,
                                @RequestParam(name = "phoneNumber", required = false) String phoneNumber,
                                @RequestParam(name = "verified", required = false) Boolean verified) {
        Account loggedUser = authenticationHelpers.tryGetUser(headers);

        AccountGetAllParameters accountGetAllParameters = new AccountGetAllParameters();
        accountGetAllParameters.setAccountTypeId(accountTypeId);
        accountGetAllParameters.setSearchAllString(searchAllString);
        accountGetAllParameters.setFirstName(firstName);
        accountGetAllParameters.setLastName(lastName);
        accountGetAllParameters.setEmail(email);
        accountGetAllParameters.setPhoneNumber(phoneNumber);
        accountGetAllParameters.setVerified(verified);

        return accountService.getAllAuthorized(loggedUser, accountGetAllParameters);
    }

    @GetMapping("/teachers")
    public List<Account> getTeachers() {
        AccountGetAllParameters accountGetAllParameters = new AccountGetAllParameters();
        accountGetAllParameters.setAccountTypeId(AccountType.TEACHER_ID);

        return accountService.getAll(accountGetAllParameters);
    }

    @GetMapping("/{id}")
    public Account getById(@RequestHeader HttpHeaders headers,
                           @PathVariable Integer id) {
        Account loggedUser = authenticationHelpers.tryGetUser(headers);
        return accountService.getByIdAuthorized(loggedUser, id);
    }

    @GetMapping(path = "/{id}/profile_picture", 
                produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] getProfilePicture(@RequestHeader HttpHeaders headers,
                                    @PathVariable Integer id) throws IOException {
        Account loggedUser = authenticationHelpers.tryGetUser(headers);
        return Files.readAllBytes(accountService.getProfilePictureAuthorized(loggedUser, id).toPath());
    }

    @PostMapping
    public void create(@Valid @RequestBody AccountCreateDto accountCreateDto,
                       HttpServletRequest request) {
        if (!authenticationHelpers.checkPasswordValidity(accountCreateDto.getPassword())) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, AuthenticationHelpers.INVALID_PASSWORD_MESSAGE);
        }
        accountCreateDto.setPassword(authenticationHelpers.hashPassword(accountCreateDto.getPassword()));
        AccountCreateParameters accountCreateParameters = new AccountCreateParameters(accountTypeService);
        accountCreateDto.transferFieldsToHolder(accountCreateParameters);
        Account account = accountService.create(accountCreateParameters);

        Locale locale = new Locale("en");
        String appUrl = request.getContextPath();
        applicationEventPublisher.publishEvent(new OnRegistrationCompleteEvent(account, locale, appUrl));
    }

    @PostMapping("/{id}/courses")
    public void enrollCourse(@RequestHeader HttpHeaders headers,
                             @PathVariable Integer id,
                             @RequestBody String courseIdString) {
        Account loggedUser = authenticationHelpers.tryGetUser(headers);
        int courseId = Integer.parseInt(courseIdString);
        Course course = accountService.enrollCourse(loggedUser, id, courseId);

        Set<Lecture> lectures = course.getLectures();
        AssignmentCreateParameters assignmentCreateParameters = new AssignmentCreateParameters(accountService, lectureService, 0, loggedUser.getAccountId());
        for (Lecture lecture : lectures) {
            assignmentCreateParameters.setLectureId(lecture.getLectureId());
            assignmentService.createAuthorized(loggedUser, assignmentCreateParameters);
        }
    }

    @PutMapping("/{id}")
    public void update(@RequestHeader HttpHeaders headers,
                       @PathVariable Integer id,
                       @Valid @RequestBody AccountUpdateDto accountUpdateDto) {
        Account loggedUser = authenticationHelpers.tryGetUser(headers);
        AccountUpdateParameters accountUpdateParameters = new AccountUpdateParameters();
        accountUpdateDto.transferFieldsToHolder(accountUpdateParameters);
        accountService.updateAuthorized(loggedUser, id, accountUpdateParameters);
    }

    @PutMapping("/{accountId}/password")
    public void updatePassword(@RequestHeader HttpHeaders headers,
                               @RequestBody String newPassword,
                               @PathVariable int accountId) {
        Account loggedUser = authenticationHelpers.tryGetUser(headers);
        if (!authenticationHelpers.checkPasswordValidity(newPassword)) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, AuthenticationHelpers.INVALID_PASSWORD_MESSAGE);
        }
        accountService.updatePassword(loggedUser, accountId, authenticationHelpers.hashPassword(newPassword));
    }

    @PutMapping("/{id}/access_types")
    public void updateAccessTypes(@RequestHeader HttpHeaders headers,
                                  @PathVariable Integer id,
                                  @RequestBody String accessTypesString) {
        Account loggedUser = authenticationHelpers.tryGetUser(headers);
        List<Integer> accessTypeIdList = Arrays.stream(accessTypesString.split(","))
                .map(ControllerArgumentsParseHelpers::getInteger)
                .collect(Collectors.toList());
        accountService.addAccessTypes(loggedUser, id, accessTypeIdList);
    }

    @PutMapping("/{id}/account_type")
    public void updateAccountType(@RequestHeader HttpHeaders headers,
                                  @PathVariable Integer id,
                                  @RequestBody String accountTypeIdString) {
        Account loggedUser = authenticationHelpers.tryGetUser(headers);
        int accountTypeId = ControllerArgumentsParseHelpers.getInteger(accountTypeIdString);
        accountService.updateAccountType(loggedUser, id, accountTypeId);
    }

    @PutMapping(path = "/{id}/profile_picture", 
                consumes = "multipart/form-data")
    public void updateProfilePicture(@RequestHeader HttpHeaders headers,
                                     @PathVariable Integer id,
                                     @RequestPart("image") MultipartFile file) throws IOException {
        File fileToSave = File.createTempFile("profile", "file");
        try {
            file.transferTo(fileToSave);
            Account loggedUser = authenticationHelpers.tryGetUser(headers);
            accountService.updateProfilePictureAuthorized(loggedUser, id, fileToSave);
        } finally {
            fileToSave.delete();
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers,
                       @PathVariable Integer id) {
        Account loggedUser = authenticationHelpers.tryGetUser(headers);
        accountService.deleteAuthorized(loggedUser, id);
    }

    @DeleteMapping("/{id}/access_types")
    public void deleteAccessType(@RequestHeader HttpHeaders headers,
                                 @PathVariable Integer id,
                                 @RequestBody String accessTypesString) {
        Account loggedUser = authenticationHelpers.tryGetUser(headers);
        List<Integer> accessTypeIdList = Arrays.stream(accessTypesString.split(","))
                .map(ControllerArgumentsParseHelpers::getInteger)
                .collect(Collectors.toList());
        accountService.deleteAccessTypes(loggedUser, id, accessTypeIdList);
    }
}
