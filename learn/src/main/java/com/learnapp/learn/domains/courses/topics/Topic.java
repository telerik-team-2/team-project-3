package com.learnapp.learn.domains.courses.topics;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "course_topics")
public class Topic {

    @Id
    @Column(name = "topic_id")
    private Integer topicId;

    @Column(name = "name")
    private String name;

    public Integer getTopicId() {
        return topicId;
    }

    public void setTopicId(Integer topicId) {
        this.topicId = topicId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
