package com.learnapp.learn.domains.assignments;

import java.util.List;

import com.learnapp.learn.domains.assignments.interfaces.AssignmentRepository;
import com.learnapp.learn.domains.assignments.wrappers.AssignmentGetAllParameters;
import com.learnapp.learn.domains.base_classes.BaseRepositoryImpl;
import com.learnapp.learn.exceptions.EntityNotFoundException;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AssignmentRepositoryImpl extends BaseRepositoryImpl<Assignment> implements AssignmentRepository {
    
    @Autowired
    public AssignmentRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory, Assignment.class);
    }

    @Override
    public Assignment getByLecture(int accountId, int lectureId) {
        AssignmentGetAllParameters assignmentGetAllParameters = new AssignmentGetAllParameters();
        assignmentGetAllParameters.setLectureId(lectureId);
        assignmentGetAllParameters.setOwnerAccountId(accountId);

        List<Assignment> assignments = getAll(assignmentGetAllParameters);

        if (assignments.isEmpty()) {
            throw new EntityNotFoundException("Assignment", "lecture id", String.valueOf(lectureId));
        }

        return assignments.get(0);
    }
}
