package com.learnapp.learn.domains.lectures;

import com.learnapp.learn.domains.AuthorizationHelpers;
import com.learnapp.learn.domains.access_types.AccessType;
import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.base_classes.BaseServiceImpl;
import com.learnapp.learn.domains.lectures.interfaces.LectureRepository;
import com.learnapp.learn.domains.lectures.interfaces.LectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class LectureServiceImpl extends BaseServiceImpl<Lecture> implements LectureService {

    @Autowired
    public LectureServiceImpl(LectureRepository lectureRepository) {
        super(lectureRepository);
    }

    @Override
    protected boolean checkGetAllAuthorization(Account loggedUser) {
        return AuthorizationHelpers.checkAccess(loggedUser, AccessType.TEACHER_ACCESS_ID);
    }

    @Override
    protected boolean checkGetByIdPostGetAuthorization(Account loggedUser, Lecture objectToGet) {
        if (loggedUser.getCourseSet().stream().anyMatch(course -> Objects.equals(course.getCourseId(), objectToGet.getCourse().getCourseId()))) {
            return true;
        }
        return AuthorizationHelpers.checkAccess(loggedUser, AccessType.TEACHER_ACCESS_ID);
    }
}
