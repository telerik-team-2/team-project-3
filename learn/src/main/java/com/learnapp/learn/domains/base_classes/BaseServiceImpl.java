package com.learnapp.learn.domains.base_classes;

import com.learnapp.learn.domains.AuthorizationHelpers;
import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.base_classes.interfaces.*;
import com.learnapp.learn.exceptions.UnauthorizedAccessException;

public class BaseServiceImpl<T> extends BaseGetServiceImpl<T> implements BaseService<T> {

    private final BaseRepository<T> baseRepository;

    public BaseServiceImpl(BaseRepository<T> baseRepository) {
        super(baseRepository);
        this.baseRepository = baseRepository;
    }

    @Override
    public T createAuthorized(Account loggedUser, CreateParametersHolder<T> createParametersHolder) {
        if (!checkCreateAuthorization(loggedUser, createParametersHolder)) {
            throw new UnauthorizedAccessException(AuthorizationHelpers.UNAUTHORIZED_ACCESS);
        }
        return create(createParametersHolder);
    }

    protected boolean checkCreateAuthorization(Account loggedUser, CreateParametersHolder<T> createParametersHolder) {
        return true;
    }

    @Override
    public T create(CreateParametersHolder<T> createParametersHolder) {
        T objectToCreate = createParametersHolder.createObjectFromParameters();
        configureCreatedObject(objectToCreate);
        baseRepository.create(objectToCreate);
        return objectToCreate;
    }

    protected void configureCreatedObject(T objectToCreate) {}

    @Override
    public void updateAuthorized(Account loggedUser, int objectId, UpdateParametersHolder<T> updateParametersHolder) {
        T objectToUpdate = getById(objectId);
        if (!checkUpdateAuthorization(loggedUser, objectToUpdate)) {
            throw new UnauthorizedAccessException(AuthorizationHelpers.UNAUTHORIZED_ACCESS);
        }
        update(objectToUpdate, updateParametersHolder);
    }

    protected boolean checkUpdateAuthorization(Account loggedUser, T objectToUpdate) {
        return true;
    }

    @Override
    public void update(T objectToUpdate, UpdateParametersHolder<T> updateParametersHolder) {
        updateParametersHolder.updateObjectWithParameters(objectToUpdate);
        update(objectToUpdate);
    }

    @Override
    public void update(T objectToUpdate) {
        baseRepository.update(objectToUpdate);
    }

    @Override
    public void deleteAuthorized(Account loggedUser, int objectId) {
        T objectToDelete = getById(objectId);
        if (!checkDeleteAuthorization(loggedUser, objectToDelete)) {
            throw new UnauthorizedAccessException(AuthorizationHelpers.UNAUTHORIZED_ACCESS);
        }
        delete(objectToDelete);
    }

    protected boolean checkDeleteAuthorization(Account loggedUser, T objectToUpdate) {
        return true;
    }

    @Override
    public void delete(T objectToDelete) {
        baseRepository.delete(objectToDelete);
    }
}
