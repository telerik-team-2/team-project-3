package com.learnapp.learn.domains.courses;

import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.accounts.interfaces.AccountService;
import com.learnapp.learn.domains.assignments.Assignment;
import com.learnapp.learn.domains.assignments.interfaces.AssignmentService;
import com.learnapp.learn.domains.courses.course_ratings.interfaces.CourseRatingService;
import com.learnapp.learn.domains.courses.interfaces.CourseService;
import com.learnapp.learn.domains.courses.statuses.Status;
import com.learnapp.learn.domains.lectures.Lecture;
import com.learnapp.learn.exceptions.UnauthorizedAccessException;
import com.learnapp.learn.mvc.BaseMvcController;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/courses")
public class CourseMvcController extends BaseMvcController {

    private final CourseService courseService;
    private final AssignmentService assignmentService;
    private final CourseRatingService courseRatingService;

    public CourseMvcController(AccountService accountService,
                               CourseService courseService,
                               AssignmentService assignmentService,
                               CourseRatingService courseRatingService) {
        super(accountService);
        this.courseService = courseService;
        this.assignmentService = assignmentService;
        this.courseRatingService = courseRatingService;
    }

    @GetMapping("/{id}")
    public String getById(HttpSession session,
                          Model model,
                          @PathVariable int id) {
        if (!checkHasLoggedUser(model)) {
            return "redirect:/";
        }
        
        Account loggedUser = getLoggedUser(model);

        Course course;
        try {
            course = courseService.getByIdAuthorized(loggedUser, id);
            model.addAttribute("course", course);
            model.addAttribute("lectures", course.getLectures().stream()
                    .sorted(Comparator.comparingInt(Lecture::getLectureId))
                    .collect(Collectors.toList()));
        } catch (EntityNotFoundException|UnauthorizedAccessException e) {
            return "redirect:/";
        }

        boolean ratingExists = courseRatingService.checkRatingExists(loggedUser, course);
        if (ratingExists) {
            model.addAttribute("rating", courseRatingService.getAccountCourseRating(loggedUser, course).getRating().getRatingId());
        }
        
        model.addAttribute("hasRated", ratingExists);
        model.addAttribute("canRate", courseRatingService.checkCourseCompleted(loggedUser, course) && !ratingExists);
        model.addAttribute("canEnroll", checkCanEnrollCourse(loggedUser, course));
        setupCourseGrade(model, loggedUser, course);

        return "course-info";
    }

    private void setupCourseGrade(Model model, Account account, Course course) {
        List<Assignment> assignments = assignmentService.getByCourse(account, course.getCourseId());
        int courseGrade = 0;
        for (Assignment assignment : assignments) {
            Integer grade = assignment.getGrade();
            if (grade != null) {
                courseGrade += grade;
            }
        }
        model.addAttribute("courseGrade", courseGrade);
    } 

    private boolean checkCanEnrollCourse(Account loggedUser, Course course) {
        if (course.getStatus().getStatusId() == Status.DRAFT_COURSE_STATUS_ID) {
            return false;
        }

        if (loggedUser.getCourseSet().contains(course)) {
            return false;
        }

        if (course.getStartingDate().isAfter(LocalDate.now())) {
            return false;
        }

        return true;
    }
}
