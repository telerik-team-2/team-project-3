package com.learnapp.learn.domains.courses.statuses;

import com.learnapp.learn.domains.base_classes.BaseGetServiceImpl;
import com.learnapp.learn.domains.courses.statuses.interfaces.StatusRepository;
import com.learnapp.learn.domains.courses.statuses.interfaces.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StatusServiceImpl extends BaseGetServiceImpl<Status> implements StatusService {

    @Autowired
    public StatusServiceImpl(StatusRepository statusRepository) {
        super(statusRepository);
    }
}
