package com.learnapp.learn.domains.courses.topics;

import com.learnapp.learn.domains.courses.topics.interfaces.TopicService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/topics")
public class TopicController {

    private final TopicService topicService;

    public TopicController(TopicService topicService) {
        this.topicService = topicService;
    }

    @GetMapping
    public List<Topic> getAll() {
        return topicService.getAll();
    }

    @GetMapping("/{id}")
    public Topic getById(@PathVariable Integer id) {
        return topicService.getById(id);
    }
}
