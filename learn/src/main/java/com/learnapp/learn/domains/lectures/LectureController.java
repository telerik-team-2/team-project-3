package com.learnapp.learn.domains.lectures;

import com.learnapp.learn.domains.AuthenticationHelpers;
import com.learnapp.learn.domains.accounts.Account;
import com.learnapp.learn.domains.courses.interfaces.CourseService;
import com.learnapp.learn.domains.lectures.dtos.LectureCreateDto;
import com.learnapp.learn.domains.lectures.dtos.LectureUpdateDto;
import com.learnapp.learn.domains.lectures.interfaces.LectureService;
import com.learnapp.learn.domains.lectures.wrappers.LectureCreateParameters;
import com.learnapp.learn.domains.lectures.wrappers.LectureGetAllParameters;
import com.learnapp.learn.domains.lectures.wrappers.LectureUpdateParameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/lectures")
public class LectureController {


    private final AuthenticationHelpers authenticationHelpers;
    private final LectureService lectureService;
    private final CourseService courseService;

    //TODO add Rest to class name
    @Autowired
    public LectureController(AuthenticationHelpers authenticationHelpers,
                             LectureService lectureService,
                             CourseService courseService) {
        this.authenticationHelpers = authenticationHelpers;
        this.lectureService = lectureService;
        this.courseService = courseService;
    }

    //TODO complete filtering/sorting
    @GetMapping
    public List<Lecture> getAll(@RequestHeader HttpHeaders headers) {
        Account loggedUser = authenticationHelpers.tryGetUser(headers);
        return lectureService.getAllAuthorized(loggedUser, new LectureGetAllParameters());
    }

    @GetMapping("/{id}")
    public Lecture getById(@RequestHeader HttpHeaders headers,
                           @PathVariable Integer id) {
        Account loggedUser = authenticationHelpers.tryGetUser(headers);
        return lectureService.getByIdAuthorized(loggedUser, id);
    }

    @PostMapping
    public void create(@Valid @RequestBody LectureCreateDto lectureCreateDto) {
        LectureCreateParameters lectureCreateParameters = new LectureCreateParameters(courseService);
        lectureCreateDto.transferFieldsToHolder(lectureCreateParameters);
        lectureService.create(lectureCreateParameters);

    }

    @PutMapping("/{id}")
    public void update(@RequestHeader HttpHeaders headers,
                       @PathVariable Integer id,
                       @Valid @RequestBody LectureUpdateDto lectureUpdateDto) {
        Account loggedUser = authenticationHelpers.tryGetUser(headers);
        LectureUpdateParameters lectureUpdateParameters = new LectureUpdateParameters();
        lectureUpdateDto.transferFieldsToHolder(lectureUpdateParameters);
        lectureService.updateAuthorized(loggedUser, id, lectureUpdateParameters);
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers,
                       @PathVariable Integer id) {
        Account loggedUser = authenticationHelpers.tryGetUser(headers);
        lectureService.deleteAuthorized(loggedUser, id);
    }
}
