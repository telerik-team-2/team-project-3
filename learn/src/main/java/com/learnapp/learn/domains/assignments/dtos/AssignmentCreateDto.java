package com.learnapp.learn.domains.assignments.dtos;

import javax.validation.constraints.NotNull;

import com.learnapp.learn.domains.assignments.Assignment;
import com.learnapp.learn.domains.assignments.wrappers.AssignmentCreateParameters;
import com.learnapp.learn.domains.base_classes.interfaces.CreateParametersDto;

public class AssignmentCreateDto implements CreateParametersDto<Assignment, AssignmentCreateParameters> {

    @NotNull
    private Integer lectureId;

    @Override
    public void transferFieldsToHolder(AssignmentCreateParameters createParametersHolder) {
        createParametersHolder.setLectureId(lectureId);
    }

    public Integer getLectureId() {
        return lectureId;
    }

    public void setLectureId(Integer lectureId) {
        this.lectureId = lectureId;
    }
}