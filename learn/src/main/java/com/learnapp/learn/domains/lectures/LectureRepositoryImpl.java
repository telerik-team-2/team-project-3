package com.learnapp.learn.domains.lectures;

import com.learnapp.learn.domains.base_classes.BaseRepositoryImpl;
import com.learnapp.learn.domains.lectures.interfaces.LectureRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class LectureRepositoryImpl extends BaseRepositoryImpl<Lecture> implements LectureRepository {

    @Autowired
    public LectureRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory, Lecture.class);
    }

    @Override
    protected void configureUpdatePrerequisites(Lecture objectToUpdate, Session session) {
        LectureDescription lectureDescription = objectToUpdate.getLectureDescription();
        if (lectureDescription != null) {
            session.saveOrUpdate(objectToUpdate.getLectureDescription());
        }
    }
}
