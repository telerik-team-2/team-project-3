package com.learnapp.learn.domains.courses.dtos;

import com.learnapp.learn.domains.courses.statuses.Status;
import com.learnapp.learn.domains.courses.topics.Topic;

public class GetCourseDto {

    private Integer courseId;
    private String title;
    private Topic topic;
    private Status status;

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
