package com.learnapp.learn.domains.courses.ratings;

import com.learnapp.learn.domains.base_classes.BaseGetRepositoryImpl;
import com.learnapp.learn.domains.courses.ratings.interfaces.RatingRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RatingRepositoryImpl extends BaseGetRepositoryImpl<Rating> implements RatingRepository {

    @Autowired
    public RatingRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory, Rating.class);
    }
}
