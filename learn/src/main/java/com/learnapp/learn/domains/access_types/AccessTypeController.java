package com.learnapp.learn.domains.access_types;

import com.learnapp.learn.domains.access_types.interfaces.AccessTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/access_types")
public class AccessTypeController {

    private final AccessTypeService accessTypeService;

    @Autowired
    public AccessTypeController(AccessTypeService accessTypeService) {
        this.accessTypeService = accessTypeService;
    }

    @GetMapping
    public List<AccessType> getAll() {
        return accessTypeService.getAll();
    }

    @GetMapping("/{id}")
    public AccessType getById(@PathVariable int id) {
        return accessTypeService.getById(id);
    }
}
