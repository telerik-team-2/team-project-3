import * as helpers from "../helpers.js";
import * as accessTypesApi from "../api-requests/api-access-types.js";

const accessTypeSelectElements = document.getElementsByClassName("access-type-select");

fillAccessTypeSelect();

async function fillAccessTypeSelect() {
    let accessTypes = await accessTypesApi.accessTypes;

    let options = [];
    let defaultOption = helpers.createOptionElement("Select access type", "null");
    helpers.addElementToArray(options, defaultOption);
    for (let i = 0, length = accessTypes.length; i < length; i++) {
        let accessType = accessTypes[i];
        let option = helpers.createOptionElement(accessType["name"], accessType["accessId"]);
        helpers.addElementToArray(options, option);
    }

    for (let i = 0, length = accessTypeSelectElements.length; i < length; i++) {
        let selectElement = accessTypeSelectElements[i];
        let optionsCopy = helpers.deepCopyElementArray(options);
        for (let i2 = 0, optionsLength = optionsCopy.length; i2 < optionsLength; i2++) {
            selectElement.appendChild(optionsCopy[i2]);
        }
    }
}
