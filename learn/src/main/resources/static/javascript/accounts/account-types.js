import * as accountTypeApi from "../api-requests/api-account-types.js";
import * as helpers from "../helpers.js";

const accountTypeSelectElements = document.getElementsByClassName("account-type-select");

fillAccountTypeSelect();

async function fillAccountTypeSelect() {
    let accountTypes = await accountTypeApi.accountTypes;

    let options = [];
    let defaultOption = helpers.createOptionElement("Select account type", "null");
    helpers.addElementToArray(options, defaultOption);
    for (let i = 0, length = accountTypes.length; i < length; i++) {
        let accountType = accountTypes[i];
        let option = helpers.createOptionElement(accountType["name"], accountType["accountTypeId"]);
        helpers.addElementToArray(options, option);
    }

    for (let i = 0, length = accountTypeSelectElements.length; i < length; i++) {
        let selectElement = accountTypeSelectElements[i];
        let optionsCopy = helpers.deepCopyElementArray(options);
        for (let i2 = 0, optionsLength = optionsCopy.length; i2 < optionsLength; i2++) {
            selectElement.appendChild(optionsCopy[i2]);
        }
    }
}
