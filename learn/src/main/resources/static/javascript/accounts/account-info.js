export function createAccountEditPage(accountId, firstName, lastName, email) {
    var templateClone = document.getElementById("accountEditTemplate").content.cloneNode(true);
    let newNode = document.createElement("div");
    newNode.appendChild(templateClone);
    var result = newNode.innerHTML.replace(/{accountId}/g, accountId);
    result = result.replace(/{firstName}/g, firstName);
    result = result.replace(/{lastName}/g, lastName);
    result = result.replace(/{email}/g, email);
    newNode.innerHTML = result.trim();
    return newNode.firstChild;
}