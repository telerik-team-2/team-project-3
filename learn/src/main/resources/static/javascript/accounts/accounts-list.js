import * as accountsApi from "../api-requests/api-accounts.js";
import * as tableManager from "../templates/table-manager.js";

let accountsTable = new tableManager.TableManager(
    {
        "accountTypeId": document.getElementById("accounts-list-account-type"),
        "searchAllString": document.getElementById("accounts-list-search-all"),
        "firstName": document.getElementById("accounts-list-first-name"),
        "lastName": document.getElementById("accounts-list-last-name"),
        "email": document.getElementById("accounts-list-email"),
        "phoneNumber": document.getElementById("accounts-list-phone-number"),
        "verified": document.getElementById("accounts-list-verified"),
    },
    document.getElementById("accounts-list-table-body"),
    function (requestParameters) {
        return accountsApi.getAll(requestParameters);
    },
    function (resultEntry) {
        let resultInfo = {};
        resultInfo[resultEntry["accountId"]] = this.accountIdRegex;
        resultInfo[`${resultEntry["firstName"]} ${resultEntry["lastName"]}`] = "{name}";
        resultInfo[resultEntry["accountType"]["name"]] = "{accountType}";
        resultInfo[resultEntry["email"]] = "{email}";
        resultInfo[resultEntry["verified"]] = "{verified}";
        return resultInfo;
    },
    document.getElementById("accounts-list-entry-template")
);

accountsTable.accountIdRegex = new RegExp("{accountId}", "g");

function filterTable() {
    accountsTable.getEntries();
}

let setSearchButton = function (buttonElement) {
    buttonElement.addEventListener("click", filterTable);
};

setSearchButton(document.getElementById("accounts-list-submit"));

accountsTable.getEntries();
