import * as accountsApi from "../api-requests/api-accounts.js";

const submitButton = document.getElementById("register-submit");
const registerInputElements = {
    "accountTypeId": document.getElementById("register-account-type"),
    "firstName": document.getElementById("register-first-name"),
    "lastName": document.getElementById("register-last-name"),
    "email": document.getElementById("register-email"),
    "phoneNumber": document.getElementById("register-phone-number"),
    "password": document.getElementById("register-password"),
}

const registrationAccountTypes = {
    "Teacher": 2,
    "Student": 1,
}

const registerAccountTypeDatalist = document.getElementById("register-account-type-list");

submitButton.addEventListener("click", submitRegistration);

constructDatalist();

async function submitRegistration(event) {
    event.preventDefault()
    let registrationInfo = {};

    for (let field in registerInputElements) {
        if (!registerInputElements[field].value || registerInputElements[field].value.length === 0) {
            alert("Please fill all registration fields.")
            return;
        }
        registrationInfo[field] = registerInputElements[field].value
    }

    let response = await accountsApi.create(registrationInfo);

    if (response.ok) {
        alert("Registration complete. \
              An email has been sent to the email address associated with your account to confirm it.");
        // window.location.replace(`${window.location.origin}/login`);
    } else {
        alert(`There was an error with the registration: ${response.message}`);
    }
}

function constructDatalist() {
    for (let element in registrationAccountTypes) {
        let option = document.createElement("option");
        option.text = element;
        option.value = registrationAccountTypes[element]
        registerAccountTypeDatalist.appendChild(option);
    }
}
