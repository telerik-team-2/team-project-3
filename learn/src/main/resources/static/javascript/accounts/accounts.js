import * as ACCOUNTSAPI from "../api-requests/api-accounts.js";

async function getProfilePicture(accountId) {
    let profilePicture = await (await ACCOUNTSAPI.getProfilePicture(accountId)).blob();
    return URL.createObjectURL(profilePicture);
}

export async function setupProfilePicture(accountId, profilePictureElement) {
    profilePictureElement.src = await getProfilePicture(accountId);
}