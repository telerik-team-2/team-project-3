import * as accountsApi from "../api-requests/api-accounts.js";
import * as accessTypesApi from "../api-requests/api-access-types.js";
import * as helpers from "../helpers.js";

const accessTypesList = document.getElementById("access-types-list");
const accessTypesSelect = document.getElementById("edit-access-type");
const accessTypeAddButton = document.getElementById("add-access-type");
const accessTypeRemoveButton = document.getElementById("remove-access-type");
let accountAccessTypes = window.data["account"]["accessTypeSet"];

accessTypeAddButton.addEventListener("click", addAccessType);
accessTypeRemoveButton.addEventListener("click", deleteAccessType)

fillAccessTypesList();

async function addAccessType() {
    let result = await accountsApi.updateAccessTypes(window.data["account"]["accountId"], accessTypesSelect.value, false);
    if (!result.ok) {
        alert("Unsuccessful access modification.");
        return;
    }

    for (let i = 0, length = accountAccessTypes.length; i < length; i++) {
        let accessType = accountAccessTypes[i];
        if (accessType["accessId"] == accessTypesSelect.value) {
            return;
        }
    }

    let accessTypesMap = await accessTypesApi.accessTypesMap;
    let accessId = accessTypesSelect.value;
    let accessName = accessTypesMap[`${accessTypesSelect.value}`];

    helpers.addElementToArray(accountAccessTypes, {"accessId" : accessId, "name" : accessName});
    fillAccessTypesList();
}

async function deleteAccessType() {
    let result = await accountsApi.updateAccessTypes(window.data["account"]["accountId"], accessTypesSelect.value, true);
    if (!result.ok) {
        alert("Unsuccessful access modification.");
        return;
    }

    for (let i = 0, length = accountAccessTypes.length; i < length; i++) {
        let accessType = accountAccessTypes[i];
        if (accessType["accessId"] == accessTypesSelect.value) {
            accountAccessTypes.splice(i, 1);
            break;
        }
    }

    fillAccessTypesList();
}

function fillAccessTypesList() {
    accessTypesList.innerHTML = "";
    for (let accessType in accountAccessTypes) {
        accessTypesList.appendChild(createAccessTypeEntry(accountAccessTypes[accessType]["name"]));
    }
}

function createAccessTypeEntry(name) {
    let newListItem = document.createElement("li");
    newListItem.classList.add("list-group-item");
    newListItem.innerText = name;
    accessTypesList.appendChild(newListItem);
    return newListItem;
}
