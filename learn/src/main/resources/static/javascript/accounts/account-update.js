import * as accountsApi from "../api-requests/api-accounts.js";

const updateInputElements = {
    "firstName": document.getElementById("edit-first-name"),
    "lastName": document.getElementById("edit-last-name"),
    "phoneNumber": document.getElementById("edit-phone-number"),
}

const infoElements = {
    "firstName": document.getElementById("info-first-name"),
    "lastName": document.getElementById("info-last-name"),
    "phoneNumber": document.getElementById("info-phone-number"),
}

const updateButton = document.getElementById("edit-submit");

updateButton.addEventListener("click", updateAccount);

async function updateAccount(event) {
    let requestJson = {};

    for (let entry in updateInputElements) {
        requestJson[entry] = updateInputElements[entry].value;
    }

    let result = await accountsApi.update(window["data"]["account"]["accountId"], requestJson);

    if (result.ok) {
        updateInfoElements();
        alert("Update successful");
    } else {
        alert("Error performing update");
    }
}

function updateInfoElements() {
    for (let entry in infoElements) {
        infoElements[entry].innerText = updateInputElements[entry].value;
    }
}
