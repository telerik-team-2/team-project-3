import * as helpers from "../helpers.js";
import * as adminRequestsStatusesApi from "../api-requests/api-administration-request-statuses.js";

const adminRequestsStatusSelectElements = document.getElementsByClassName("admin-request-status-select");

fillStatusSelectElements();

async function fillStatusSelectElements() {
    let statuses = await adminRequestsStatusesApi.adminRequestsStatuses;

    let options = [];
    let defaultOption = helpers.createOptionElement("Select status", "null");
    helpers.addElementToArray(options, defaultOption);
    for (let i = 0, length = statuses.length; i < length; i++) {
        let status = statuses[i];
        let option = helpers.createOptionElement(status["name"], status["statusId"]);
        helpers.addElementToArray(options, option);
    }

    for (let i = 0, length = adminRequestsStatusSelectElements.length; i < length; i++) {
        let selectElement = adminRequestsStatusSelectElements[i];
        let optionsCopy = helpers.deepCopyElementArray(options);
        for (let i2 = 0, optionsLength = optionsCopy.length; i2 < optionsLength; i2++) {
            selectElement.appendChild(optionsCopy[i2]);
        }
    }
}
