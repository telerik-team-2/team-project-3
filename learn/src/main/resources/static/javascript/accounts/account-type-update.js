import * as accountsApi from "../api-requests/api-accounts.js";
import * as accountTypesApi from "../api-requests/api-account-types.js";

const accountTypeSelect = document.getElementById("edit-account-type");
const updateButton = document.getElementById("account-type-change-button");
const infoElement = document.getElementById("info-account-type")
updateButton.addEventListener("click", updateAccountType)

async function updateAccountType(event) {
    let result = await accountsApi.updateAccountType(window.data["account"]["accountId"], accountTypeSelect.value);
    if (!result.ok) {
        alert("There was an issue updating account type.");
        return;
    }

    let accountTypesMap = await accountTypesApi.accountTypesMap;

    infoElement.innerText = accountTypesMap[`${accountTypeSelect.value}`];
}
