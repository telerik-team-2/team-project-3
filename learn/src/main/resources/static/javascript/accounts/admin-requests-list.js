import * as adminRequestsApi from "../api-requests/api-administration-requests.js";
import * as tableManager from "../templates/table-manager.js";

const tableEntryElementId = "admin-request-entry-";

let adminRequestsTable = new tableManager.TableManager(
    {
        "submissionAccountId": document.getElementById("admin-requests-list-submission-account-id"),
        "statusId": document.getElementById("admin-requests-list-status"),
    },
    document.getElementById("admin-requests-list-table-body"),
    function(requestParameters) {
        return adminRequestsApi.getAll(requestParameters);
    },
    function(resultEntry) {
        let resultInfo = {};
        resultInfo[resultEntry["submissionAccount"]["accountId"]] = this.accountIdRegex;
        resultInfo[resultEntry["requestId"]] = this.requestIdRegex;
        resultInfo[resultEntry["administrationRequestType"]["name"]] = "{requestType}";
        resultInfo[resultEntry["creationDate"]] = "{date}";
        resultInfo[resultEntry["status"]["name"]] = "{status}";
        return resultInfo;
    },
    document.getElementById("admin-requests-list-entry-template")
);

adminRequestsTable.accountIdRegex = new RegExp("{accountId}", "g");
adminRequestsTable.requestIdRegex = new RegExp("{requestId}", "g");

async function updateAdministrationRequest(event) {
    let requestId = this.requestId;
    let statusId = this.statusId;
    let result = await adminRequestsApi.updateStatus(requestId, statusId);
    if (!result.ok) {
        alert("Unable to change request status.");
        return;
    }

    document.getElementById(`admin-request-${requestId}-status`).innerHTML = this.statusName;
    alert("Status updates successfully.");
}

adminRequestsTable.configureTableEntry = function(resultEntry, tableEntry) {
    let requestId = resultEntry["requestId"];
    let approveButton = document.getElementById(`admin-request-${requestId}-approve`);
    approveButton.requestId = requestId;
    approveButton.statusId = 2;
    approveButton.statusName = "Approved";
    approveButton.addEventListener("click", updateAdministrationRequest, false);
    let rejectButton = document.getElementById(`admin-request-${requestId}-reject`);
    rejectButton.requestId = requestId;
    rejectButton.statusId = 1;
    rejectButton.statusName = "Denied";
    rejectButton.addEventListener("click", updateAdministrationRequest, false);
}

async function filterTable() {
    await adminRequestsTable.getEntries();
}

let setSearchButton = function(buttonElement) {
    buttonElement.addEventListener("click", filterTable);
};

setSearchButton(document.getElementById("admin-requests-list-submit"));

adminRequestsTable.getEntries();
