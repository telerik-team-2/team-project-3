export function addElementToArray(array, element) {
    array[array.length] = element;
}

export function removeElementFromArray(array, element) {
    let index = array.indexOf(element);
    if (index <= -1) {
        return;
    }
    array.splice(index, 1);
}

export function deepCopyElementArray(elementArray) {
    let newArray = [];
    for (let i = 0, length = elementArray.length; i < length; i++) {
        addElementToArray(newArray, elementArray[i].cloneNode(true));
    }
    return newArray;
}

export function checkValuePresent(element) {
    if (element.value && element.value.length > 0 && element.value.localeCompare("null") !== 0) {
        return true;
    }

    return false;
}

export function createOptionElement(text, value) {
    let optionElement = document.createElement("option");
    optionElement.text = text;
    optionElement.value = value;
    return optionElement;
}

export function downloadFile(file, fileName, fileType) {
    let url = URL.createObjectURL(file);
    var a = document.createElement('a');
    a.href = url;
    a.download = `${fileName}.${fileType}`;
    document.body.appendChild(a);
    a.click();
    a.remove();
}
