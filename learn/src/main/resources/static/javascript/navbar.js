import { addElementToArray } from "./helpers.js";
import * as INDEX from "./index.js";

export function setupNavButton(buttonElement, bodyElement) {
    INDEX.pageContent.appendChild(bodyElement);
    addElementToArray(INDEX.navButtons, buttonElement);
    buttonElement.bodyElement = bodyElement;
    buttonElement.addEventListener("click", displaySectionBody);
}

export function displaySectionBody(event) {
    this.bodyElement.classList.remove("invisible_test");
    for (let i = 0, length = INDEX.navButtons.length; i < length; i++) {
        let element = INDEX.navButtons[i].bodyElement;
        if (element === this.bodyElement) {
            continue;
        }
        if (!element.classList.contains("invisible_test")) {
            element.classList.add("invisible_test");
        }
    }
}
