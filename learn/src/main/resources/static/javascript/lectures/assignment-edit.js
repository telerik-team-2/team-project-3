import * as assignmentsApi from "../api-requests/api-assignments.js";
import * as helpers from "../helpers.js";

const assignmentsModal = {
    "assignmentId": null,
    "header": document.getElementById("assignments-table-modal-header"),
    "body": {
        "assignmentDownload": document.getElementById("assignment-download"),
        "gradeValue": document.getElementById("assignment-grade-value"),
        "gradeSubmit": document.getElementById("assignment-grade-submit"),
    }
}

assignmentsModal["body"]["assignmentDownload"].addEventListener("click", downloadAssignment);
assignmentsModal["body"]["gradeSubmit"].addEventListener("click", submitAssignmentGrade);

async function submitAssignmentGrade(event) {
    let newGrade = assignmentsModal["body"]["gradeValue"].value;
    if (!newGrade || newGrade.length === 0) {
        alert("Please insert a grade value.");
        return;
    }
    let result = await assignmentsApi.updateGrade(assignmentsModal["assignmentId"], newGrade);

    if (result.ok) {
        alert("Grade updated successfully");
    } else {
        alert("There was an issue updating the grade.")
    }
}

async function downloadAssignment(event) {
    let result = await assignmentsApi.getFile(assignmentsModal["assignmentId"]);

    if (!result.ok) {
        alert("There was an issue downloading the assignment.");
        return;
    }

    let assignmentFile = await result.blob();
    helpers.downloadFile(assignmentFile, assignmentsModal.assignmentId, "txt");
}

export async function configureAssignmentButtons() {
    let assignmentButtons = await document.getElementsByClassName("assignments-table-entry");
    for (let i = 0, length = assignmentButtons.length; i < length; i++) {
        let button = assignmentButtons.item(i);
        let assignmentId = parseInt(button.id.split("-")[3]);
        button.assignmentId = assignmentId;
        button.addEventListener("click", setupAssignmentModal);
    }
}

function setupAssignmentModal(event) {
    assignmentsModal["assignmentId"] = this.assignmentId;
}
