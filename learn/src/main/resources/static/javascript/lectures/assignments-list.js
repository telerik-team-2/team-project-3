import * as assignmentsApi from "../api-requests/api-assignments.js";
import * as assignmentEdit from "./assignment-edit.js";
import * as tableManager from "../templates/table-manager.js";

let assignmentsTable = new tableManager.TableManager(
    {
        "gradeMin": document.getElementById("assignments-list-grade-min"),
        "gradeMax": document.getElementById("assignments-list-grade-max"),
        "graded": document.getElementById("assignments-list-graded")
    },
    document.getElementById("assignments-table-body"),
    function (requestParameters) {
        if (requestParameters.length > 0) {
            requestParameters = `${requestParameters}&lectureId=${window.data["lecture"]["lectureId"]}`;
        } else {
            requestParameters = `lectureId=${window.data["lecture"]["lectureId"]}`;
        }
        return assignmentsApi.getAll(requestParameters);
    },
    function (resultEntry) {
        let resultInfo = {};
        resultInfo[` ${resultEntry["assignmentId"]} `] = this.assignmentIdRegex;
        resultInfo[resultEntry["ownerAccount"]["accountId"]] = "{accountId}";
        resultInfo[`${resultEntry["ownerAccount"]["firstName"]} ${resultEntry["ownerAccount"]["lastName"]}`] = "{accountName}";
        resultInfo[resultEntry["grade"]] = "{grade}";
        return resultInfo;
    },
    document.getElementById("assignments-table-entry-template")
);

assignmentsTable.assignmentIdRegex = new RegExp("{assignmentId}", "g");

async function filterTable() {
    await assignmentsTable.getEntries();
    assignmentEdit.configureAssignmentButtons();
}

let setSearchButton = function (buttonElement) {
    buttonElement.addEventListener("click", filterTable);
};

setSearchButton(document.getElementById("assignments-list-submit"));

filterTable();
