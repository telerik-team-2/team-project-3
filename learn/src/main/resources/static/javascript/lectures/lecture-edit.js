import * as lecturesApi from "../api-requests/api-lectures.js";
import * as helpers from "../helpers.js";

const updateInputElements = {
    "title": document.getElementById("edit-lecture-title"),
    "lectureDescription": document.getElementById("edit-lecture-description"),
    "maxGrade": document.getElementById("edit-lecture-max-grade"),
    "videoLink": document.getElementById("edit-lecture-video-link")

}

const updateButton = document.getElementById("edit-lecture-submit");

updateButton.addEventListener("click", updateLecture);

async function updateLecture(event) {
    let requestJson = {};

    for(let entry in updateInputElements) {
        requestJson[entry] = updateInputElements[entry].value;
    }

    let result = await lecturesApi.update(window["data"]["lecture"]["lectureId"], requestJson);

    if(result.ok) {
        alert("Update successful");
    } else {
        alert("Error performing update");
    }
}
