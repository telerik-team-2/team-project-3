import * as assignmentsApi from "../api-requests/api-assignments.js";

const fileInput = document.getElementById("assignment-upload");
const inputLabel = document.getElementById("assignment-upload-label");
const submitButton = document.getElementById("assignment-upload-button");

fileInput.addEventListener("change", setLabelName);

submitButton.addEventListener("click", uploadAssignment);

async function uploadAssignment(event) {
    let assignment = fileInput.files[0];
    if (!assignment) {
        return;
    }

    let formData = new FormData();
    formData.append("assignmentFile", assignment);
    let response = await assignmentsApi.updateFile(window.data["assignment"]["assignmentId"], formData);
    if (response.ok) {
        alert("Assignment uploaded successfully")
    } else {
        alert("Unable to file");
    }
}

function setLabelName(event) {
    let indexOfName = this.value.lastIndexOf("\\");
    this.labels[0].innerText = this.value.substring(indexOfName + 1);
}
