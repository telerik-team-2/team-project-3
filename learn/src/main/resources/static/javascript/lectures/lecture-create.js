import * as lecturesApi from "../api-requests/api-lectures.js";
import * as helpers from "../helpers.js";

const createLectureInputElements = {
    "title": document.getElementById("create-lecture-title"),
    "maxGrade": document.getElementById("create-lecture-max-grade"),
    "videoLink": document.getElementById("create-lecture-video-link"),
}
let courseId = window.data["course"]["courseId"];
const createLectureButton = document.getElementById("create-lecture-submit");

createLectureButton.addEventListener("click", createLecture);

async function createLecture(event) {
    let lectureCreateInfo = {};

    for(let field in createLectureInputElements) {
        if(!createLectureInputElements[field].value
        || createLectureInputElements[field].value.length === 0) {
            alert("Please fill all fields.")
            return;
        }
        lectureCreateInfo[field] = createLectureInputElements[field].value;
    }
    lectureCreateInfo["courseId"] = courseId;

    let response = await lecturesApi.create(lectureCreateInfo);

    if(response.ok) {
        alert("Lecture created.");
    } else {
        alert(`There was an error creating the course: ${response.message}`);
    }
}