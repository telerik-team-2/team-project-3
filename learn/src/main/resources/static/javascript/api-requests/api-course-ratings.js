const apiEndpoint = `${window.location.origin}/api/course_ratings`;

export const courseRatings = getAll();

async function getAll() {
    return (await fetch(apiEndpoint, {
        method: "GET"
    })).json();
}