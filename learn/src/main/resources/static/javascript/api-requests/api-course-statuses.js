const apiEndpoint = `${window.location.origin}/api/course_statuses`;

export const courseStatuses = getAll();

async function getAll() {
    return(await fetch(apiEndpoint, {
        method: "GET"
    })).json();
}