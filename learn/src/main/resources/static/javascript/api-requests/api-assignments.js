const apiEndpoint = `${window.location.origin}/api/assignments`;

export function getAll(getAllParametersString) {
    return fetch(`${apiEndpoint}?${getAllParametersString}`, {
        method: 'GET',
        headers: {
            'Authentication': window.data["loggedUserEmail"],
        }
    });
}

export function getFile(assignmentId) {
    return fetch(`${apiEndpoint}/${assignmentId}/file`, {
        method: "GET",
        headers: {
            'Authentication': window.data["loggedUserEmail"],
        },
    })
}

export function updateFile(assignmentId, formData) {
    return fetch(`${apiEndpoint}/${assignmentId}/file`, {
        method: "PUT",
        body: formData,
        headers: {
            'Authentication': window.data["loggedUserEmail"],
        },
    });
}

export function updateGrade(assignmentId, grade) {
    return fetch(`${apiEndpoint}/${assignmentId}/grade`, {
        method: "PUT",
        headers: {
            'Authentication': window.data["loggedUserEmail"],
        },
        body: grade,
    })
}
