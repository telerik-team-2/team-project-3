const apiEndpoint = `${window.location.origin}/api/account_types`;

export const accountTypes = getAll()
export const accountTypesMap = constructAccountTypesMap();

async function getAll() {
    return (await fetch(apiEndpoint, {
        method: "GET"
    })).json();
}

async function  constructAccountTypesMap() {
    let accountTypesList = await accountTypes;
    let accountTypesMap = {};
    for (let i = 0, length = accountTypesList.length; i < length; i++) {
        accountTypesMap[`${accountTypesList[i]["accountTypeId"]}`] = accountTypesList[i]["name"];
    }
    return accountTypesMap;
}
