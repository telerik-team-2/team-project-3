const apiEndpoint = `${window.location.origin}/api/accounts`;

export function getProfilePicture(accountId) {
    return fetch(`${apiEndpoint}/${accountId}/profile_picture`, {
        method: "GET",
        headers: {
            'Authentication': window.data["loggedUserEmail"],
        },
    });
}

export function getAll(getAllParametersString) {
    return fetch(`${apiEndpoint}?${getAllParametersString}`, {
        method: 'GET',
        headers: {
            'Authentication': window.data["loggedUserEmail"],
        }
    });
}

export function create(createInfoJson) {
    return fetch(`${apiEndpoint}`, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(createInfoJson),
    });
}

export function enrollCourse(accountId, courseId) {
    return fetch(`${apiEndpoint}/${accountId}/courses`, {
        method: "POST",
        headers: {
            'Authentication': window.data["loggedUserEmail"],
        },
        body: courseId,
    })
}

export function update(accountId, updateInfoJson) {
    return fetch(`${apiEndpoint}/${accountId}`, {
        method: "PUT",
        headers: {
            'Content-Type': 'application/json',
            'Authentication': window.data["loggedUserEmail"],
        },
        body: JSON.stringify(updateInfoJson),
    });
}

export function updateAccessTypes(accountId, accessTypesString, shouldDelete) {
    return fetch(`${apiEndpoint}/${accountId}/access_types`, {
        method: shouldDelete ? "DELETE" : "PUT",
        body: accessTypesString,
        headers: {
            'Authentication': window.data["loggedUserEmail"],
        },
    });
}

export function updateAccountType(accountId, accountTypeIdString) {
    return fetch(`${apiEndpoint}/${accountId}/account_type`, {
        method: "PUT",
        body: accountTypeIdString,
        headers: {
            'Authentication': window.data["loggedUserEmail"],
        },
    });
}

export function updateProfilePicture(accountId, formData) {
    return fetch(`${apiEndpoint}/${accountId}/profile_picture`, {
        method: "PUT",
        body: formData,
        headers: {
            'Authentication': window.data["loggedUserEmail"],
        },
    });
}

export function updatePassword(accountId, password) {
    return fetch(`${apiEndpoint}/${accountId}/password`, {
        method: 'PUT',
        body: `${password}`,
        headers: {
            'Authentication': window.data["loggedUserEmail"],
        }
    });
}
