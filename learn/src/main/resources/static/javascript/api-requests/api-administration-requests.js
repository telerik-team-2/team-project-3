const apiEndpoint = `${window.location.origin}/api/administration_requests`;

export function getAll(getAllParametersString) {
    return fetch(`${apiEndpoint}?${getAllParametersString}`, {
        method: 'GET',
        headers: {
            'Authentication': window.data["loggedUserEmail"],
        }
    });
}

export function create(requestType) {
    return fetch(`${apiEndpoint}`, {
        method: "POST",
        headers: {
            'Authentication': window.data["loggedUserEmail"],
        },
        body: JSON.stringify({"administrationRequestTypeId": requestType}),
    })
}

export function updateStatus(requestId, newStatusId) {
    return fetch(`${apiEndpoint}/${requestId}/status`, {
        method: "PUT",
        headers: {
            'Authentication': window.data["loggedUserEmail"],
        },
        body: newStatusId,
    })
}
