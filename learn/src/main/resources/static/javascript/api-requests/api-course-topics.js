const apiEndpoint = `${window.location.origin}/api/topics`;

export const topics = getAll();

async function getAll() {
    return (await fetch(apiEndpoint, {
        method: "GET"
    })).json();
}