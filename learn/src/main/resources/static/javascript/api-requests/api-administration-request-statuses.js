const apiEndpoint = `${window.location.origin}/api/administration_requests/statuses`;

export const adminRequestsStatuses = getAll();

export async function getAll() {
    return (await fetch(`${apiEndpoint}`, {
        method: 'GET',
        headers: {
            'Authentication': window.data["loggedUserEmail"],
        }
    })).json();
}
