const apiEndpoint = `${window.location.origin}/api/courses_ratings`;


export function create(createInfoJson) {
    return fetch(`${apiEndpoint}`, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
            'Authentication': window.data["loggedUserEmail"],
        },
        body: JSON.stringify(createInfoJson),
    });
}
