const apiEndpoint = `${window.location.origin}/api/accounts/teachers`;

export const courseTeachers = getAll();

async function getAll() {
    return (await fetch(apiEndpoint, {
        method: "GET",
    })).json();
}