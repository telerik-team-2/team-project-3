const apiEndpoint = `${window.location.origin}/api/access_types`;

export const accessTypes = getAll();
export const accessTypesMap = constructAccessTypesMap();

async function constructAccessTypesMap() {
    let accessTypesMap = {};
    let accessTypesList = await accessTypes;
    for (let i = 0, length = accessTypesList.length; i < length; i++) {
        accessTypesMap[`${accessTypesList[i]["accessId"]}`] = accessTypesList[i]["name"];
    }
    return accessTypesMap;
}

async function getAll() {
    return (await fetch(apiEndpoint, {
        method: "GET"
    })).json();
}

async function getById(accessTypeId) {
    return (await fetch(`apiEndpoint/${accessTypeId}`, {
        method: "GET"
    })).json();
}
