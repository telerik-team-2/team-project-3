const apiEndpoint = `${window.location.origin}/api/lectures`;

export function getById(id) {
    let headers = {};
    if (window.data && window.data["loggedUserEmail"]) {
        headers["Authentication"] = window.data["loggedUserEmail"];
    }
    return fetch(`${apiEndpoint}/${id}`, {
        method: 'GET',
        headers: headers,
    });
}

export function create(createInfoJson) {
    return fetch(`${apiEndpoint}`, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
            'Authentication': window.data["loggedUserEmail"],
        },
        body: JSON.stringify(createInfoJson),
    })
}

export function update(lectureId, updateInfoJson) {
    return fetch(`${apiEndpoint}/${lectureId}`, {
        method: "PUT",
        headers: {
            'Content-Type': 'application/json',
            'Authentication': window.data["loggedUserEmail"],
        },
        body: JSON.stringify(updateInfoJson),
    })
}