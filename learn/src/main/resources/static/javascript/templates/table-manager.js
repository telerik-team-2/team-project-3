import * as helpers from "../helpers.js";

export function TableManager(filterSettings, entryTable, submitApiRequest, extractResultInfo, entryTemplate) {
    this.filterSettings = filterSettings;
    this.entryTable = entryTable;
    this.submitApiRequest = submitApiRequest;
    this.extractResultInfo = extractResultInfo;
    this.entryTemplate = entryTemplate;

    this.constructRequestParameters = function() {
        let requestOptions = [];
    
        for (let setting in this.filterSettings) {
            let element = this.filterSettings[setting];
            if (helpers.checkValuePresent(element)) {
                helpers.addElementToArray(requestOptions, `${setting}=${element.value}`);
            }
        }
        
        return requestOptions.join("&");
    };
    this.makeQuery = async function() {
        let requestParameters = this.constructRequestParameters();
        let result = await (await this.submitApiRequest(requestParameters)).json();
        return result;
    };
    this.createTableEntry = function(templateParameters) {
        let templateClone = this.entryTemplate.cloneNode(true).innerHTML;
        for (let parameter in templateParameters) {
            templateClone = templateClone.replace(templateParameters[parameter], parameter);
        }
        templateClone = templateClone.trim();
        let newTableEntry = document.createElement('tr');
        newTableEntry.innerHTML = templateClone;
        return newTableEntry;
    };

    this.configureTableEntry = null;

    this.fillTable = async function(resultJson) {
        for (let i = 0, length = resultJson.length; i < length; i++) {
            let resultEntry = resultJson[i];
            let templateParameters = this.extractResultInfo(resultEntry);
    
            let newEntry = this.createTableEntry(templateParameters);
            await this.entryTable.appendChild(newEntry);
            if (this.configureTableEntry) {
                this.configureTableEntry(resultEntry, newEntry);
            }
        }
    };

    this.getEntries = async function() {
        this.entryTable.innerHTML = "";
        let queryEntriesList = await this.makeQuery();
        this.fillTable(queryEntriesList);
    };
};