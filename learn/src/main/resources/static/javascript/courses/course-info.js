export function createCourseEditPage(courseId, title, topic, description, startingDate, status, passingGrade) {
    var templateClone = document.getElementById("course-info-template").content.cloneNode(true);
    let newNode = document.createElement("div");
    newNode.appendChild(templateClone);
    var result = newNode.innerHTML.replace(/{courseId}/g, courseId);
    result.replace(/{title}/g, title);
    result.replace(/{topic}/g, topic);
    result.replace(/{description/g, description);
    result.replace(/{startingDate}/g, startingDate);
    result.replace(/{status}/g, status)
    result.replace(/{passingGrade}/g, passingGrade)

    newNode.innerHTML = result.trim();
    return newNode.firstChild;
}