import * as courseTeachersApi from "../api-requests/api-course-teachers.js";

import * as helpers from "../helpers.js";

const courseTeachersSelectElements = document.getElementsByClassName("course-teacher-select");

fillCourseTeacherSelect();

async function fillCourseTeacherSelect() {
    let courseTeachers = await courseTeachersApi.courseTeachers;

    let options = [];
    let defaultOption = document.createElement("option");
    defaultOption.value = null;
    defaultOption.text = "Select teacher";
    helpers.addElementToArray(options, defaultOption);
    for (let i = 0, length = courseTeachers.length; i < length; i++) {
        let courseTeacher = courseTeachers[i];
        let option = document.createElement("option");
        option.text = courseTeacher["firstName"] + " " + courseTeacher["lastName"];
        option.value = courseTeacher["accountId"];
        helpers.addElementToArray(options, option);
    }

    for (let i = 0, length = courseTeachersSelectElements.length; i < length; i++) {
        let selectElement = courseTeachersSelectElements[i];
        let optionsCopy = helpers.deepCopyElementArray(options);
        for (let j = 0, optionsLength = optionsCopy.length; j < optionsLength; j++) {
            selectElement.appendChild(optionsCopy[j]);
        }
    }
}
