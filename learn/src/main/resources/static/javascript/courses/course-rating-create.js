import * as courseRatingsApi from "../api-requests/api-course-ratings.js";
import * as courseAccountRatingsApi from "../api-requests/api-course-account-ratings.js";
import * as helpers from "../helpers.js";

const createRatingValues = {
    "courseId": window.data["course"]["courseId"],
    "accountId": window.data["loggedUser"]["accountId"],
    "ratingId": document.getElementById("courses-list-ratings"),
}
const createRatingButton = document.getElementById("course-rating-submit");

createRatingButton.addEventListener("click", createRating);

async function createRating(event) {
    let ratingCreateInfo = {};
    ratingCreateInfo["courseId"] = createRatingValues["courseId"];
    ratingCreateInfo["accountId"] = createRatingValues["accountId"];

    let ratingElement = createRatingValues["ratingId"];

    if (!helpers.checkValuePresent(ratingElement)) {
        alert("Select a rating");
    }
    ratingCreateInfo["ratingId"] = ratingElement.value;

    let response = await courseAccountRatingsApi.create(ratingCreateInfo);

    if(response.ok) {
        alert("Rating created.");
        window.location.replace(window.location.href);
    } else {
        alert("There was an issue submitting the rating.");
    }
}
