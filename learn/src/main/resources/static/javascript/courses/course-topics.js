import * as courseTopicApi from "../api-requests/api-course-topics.js";
import * as helpers from "../helpers.js";

const courseTopicsSelectElements = document.getElementsByClassName("course-topic-select");

fillCourseTopicsSelect();

async function fillCourseTopicsSelect() {
    let courseTopics = await courseTopicApi.topics;

    let options = [];
    let defaultOption = document.createElement("option");
    defaultOption.value = null;
    defaultOption.text = "Select topic";
    helpers.addElementToArray(options, defaultOption);
    for (let i = 0, length = courseTopics.length; i < length; i++) {
        let courseTopic = courseTopics[i];
        let option = document.createElement("option");
        option.text = courseTopic["name"];
        option.value = courseTopic["topicId"];
        helpers.addElementToArray(options, option);
    }

    for (let i = 0, length = courseTopicsSelectElements.length; i < length; i++) {
        let selectElement = courseTopicsSelectElements[i];
        let optionsCopy = helpers.deepCopyElementArray(options);
        for (let j = 0, optionsLength = optionsCopy.length; j < optionsLength; j++) {
            selectElement.appendChild(optionsCopy[j]);
        }
    }
}