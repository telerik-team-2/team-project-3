import * as courseRatingsApi from "../api-requests/api-course-ratings.js";
import * as helpers from "../helpers.js";

const courseRatingSelectElements = document.getElementsByClassName("course-rating-select");

fillCourseRatingSelect();

async function fillCourseRatingSelect() {
    let courseRatings = await courseRatingsApi.courseRatings;

    let options = [];
    let defaultOption = document.createElement("option");
    defaultOption.value = null;
    defaultOption.text = "Select rating";
    helpers.addElementToArray(options, defaultOption);
    for(let i = 0, length = courseRatings.length; i < length; i++) {
        let courseRating = courseRatings[i];
        let option = document.createElement("option");
        option.text = courseRating["ratingId"];
        option.value = courseRating["ratingId"];
        helpers.addElementToArray(options, option);
    }

    for(let i = 0, length = courseRatingSelectElements.length; i < length; i++) {
        let selectElement = courseRatingSelectElements[i];
        let optionsCopy = helpers.deepCopyElementArray(options);
        for(let j = 0, optionsLength = optionsCopy.length; j < optionsLength; j++) {
            selectElement.appendChild(optionsCopy[j]);
        }
    }

}