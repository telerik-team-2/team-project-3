import * as coursesApi from "../api-requests/api-courses.js";
import * as helpers from "../helpers.js";

const createCourseInputElements = {
    "title": document.getElementById("create-title"),
    "topicId": document.getElementById("create-topic"),
    "startingDate": document.getElementById("create-start-date"),
    "passingGrade": document.getElementById("create-passing-grade")
}

const createCourseButton = document.getElementById("create-course-submit");

createCourseButton.addEventListener("click", createCourse);

async function createCourse(event) {
    //event.preventDefault();
    let courseCreateInfo = {};

    for(let field in createCourseInputElements) {
        if (!createCourseInputElements[field].value
            || createCourseInputElements[field].value.length === 0) {
            alert("Please fill all fields.")
            return;
        }
        courseCreateInfo[field] = createCourseInputElements[field].value;
    }

    let response = await coursesApi.create(courseCreateInfo);

    if(response.ok) {
        alert("Course created.");
    } else {
        alert(`There was an error creating the course: ${response.message}`);
    }
}