import * as courseStatusesApi from "../api-requests/api-course-statuses.js";
import * as helpers from "../helpers.js";

const courseStatusesSelectElements = document.getElementsByClassName("course-status-select");

fillCourseStatusesSelect();

async function fillCourseStatusesSelect() {
    let courseStatuses = await courseStatusesApi.courseStatuses;

    let options = [];
    let defaultOption = document.createElement("option");
    defaultOption.value = null;
    defaultOption.text = "Select status";
    helpers.addElementToArray(options, defaultOption);
    for (let i = 0, length = courseStatuses.length; i < length; i++) {
        let courseStatus = courseStatuses[i];
        let option = document.createElement("option");
        option.text = courseStatus["name"];
        option.value = courseStatus["statusId"];
        helpers.addElementToArray(options, option);
    }

    for (let i = 0, length = courseStatusesSelectElements.length; i < length; i++) {
        let selectElement = courseStatusesSelectElements[i];
        let optionsCopy = helpers.deepCopyElementArray(options);
        for (let j = 0, optionsLength = optionsCopy.length; j < optionsLength; j++) {
            selectElement.appendChild(optionsCopy[j]);
        }
    }
}