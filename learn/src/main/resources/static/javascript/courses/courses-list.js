import * as coursesApi from '../api-requests/api-courses.js';
import * as courseTopicApi from "/javascript/courses/course-topics.js";
import * as courseTeacherApi from "/javascript/courses/course-teachers.js";
import * as courseRatingApi from "/javascript/courses/course-ratings.js";
import * as helpers from "../helpers.js";

const coursesCardTemplate = document.getElementById("courses-list-entry-template");
const coursesDivBody = document.getElementById("courses-list-div-body");

const coursesListSearchButton = document.getElementById("courses-list-submit");
const filterSearchTitle = document.getElementById("courses-list-search-all");
const filterCourseTopic = document.getElementById("courses-list-topics");
const filterCourseTeacher = document.getElementById("courses-list-teachers");
const filterCourseRating = document.getElementById("courses-list-ratings");
const filterEnrolledAccount = document.getElementById("courses-list-enrolled-account");

coursesListSearchButton.addEventListener("click", getEntries);

export async function getEntries() {
    coursesDivBody.innerHTML = "";
    let coursesList = await (await coursesApi.getAll(constructOptionalParameters())).json();
    fillTable(coursesList);
}

function constructOptionalParameters() {
    let requestOptions = [];

    if(helpers.checkValuePresent(filterSearchTitle)) {
        helpers.addElementToArray(requestOptions, `title=${filterSearchTitle.value}`);
    }

    if(helpers.checkValuePresent(filterCourseTopic)) {
        helpers.addElementToArray(requestOptions, `topicId=${filterCourseTopic.value}`);
    }

    if(helpers.checkValuePresent(filterCourseTeacher)) {
        helpers.addElementToArray(requestOptions, `teacherId=${filterCourseTeacher.value}`);
    }

    if(helpers.checkValuePresent(filterCourseRating)) {
        helpers.addElementToArray(requestOptions, `rating=${filterCourseRating.value}`);
    }

    if(filterEnrolledAccount.checked) {
        helpers.addElementToArray(requestOptions, `enrolledAccountId=${window.data["loggedUser"]["accountId"]}`)
    }

    return requestOptions.join("&");
}

function fillTable(coursesJson) {
    for(let i = 0, length = coursesJson.length; i < length; i++) {
        let course = coursesJson[i];
        let courseId = course["courseId"];
        let title = `${course["title"]}`;
        let teacher = `${course["ownerAccount"]["firstName"]}`
            + " "
            + `${course["ownerAccount"]["lastName"]}`;
        let topic = course["topic"]["name"];
        let description = "no description";
        if(course["courseDescription"]) {
            description = course["courseDescription"]["descriptionContent"];
        }
        let rating = course["currentRating"];
        let newEntry = createCourseEntry(courseId, title, teacher, topic, description, rating);
        coursesDivBody.appendChild(newEntry);
    }
}

function createCourseEntry(courseId, title, teacher, topic, description, rating) {
    let templateClone = coursesCardTemplate.cloneNode(true).innerHTML;
    let newEntry = document.createElement('div');
    newEntry.innerHTML = templateClone.replace(/{courseId}/g, courseId)
        .replace("{title}", title)
        .replace("{teacherName}", teacher)
        .replace("{topic}", topic)
        .replace("{description}", description)
        .replace("{rating}", rating)
        .trim();
    return newEntry;
}
