import * as coursesApi from "../api-requests/api-courses.js";

const updateInputElements = {
    "title": document.getElementById("edit-title"),
    "topicId": document.getElementById("edit-topic"),
    "courseDescription": document.getElementById("edit-description"),
    "startingDate": document.getElementById("edit-start-date"),
    "statusId": document.getElementById("edit-status"),
    "passingGrade": document.getElementById("edit-passing-grade")
}

const updateButton = document.getElementById("edit-course-submit");

updateButton.addEventListener("click", updateCourse);

async function updateCourse(event) {
    let requestJson = {};

    for(let entry in updateInputElements) {
        requestJson[entry] = updateInputElements[entry].value;
    }

    let result = await coursesApi.update(window["data"]["course"]["courseId"], requestJson);

    if(result.ok) {
        alert("Update successful");
        window.location.replace(window.location.href);
    } else {
        alert("Error performing update");
    }
}
