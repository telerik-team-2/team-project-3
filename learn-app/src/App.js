import logo from './logo.svg';
import './App.css';
import './assets/stylesheet.css';
import React, { useEffect, useState } from 'react';
import * as ReactDOM from 'react-dom';

const url = "http://localhost:8080/api/courses/1";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
      <div id="test"></div>
    </div>
  );
}

function getstuff() {
  return fetch(url, {
    method: "get",
    mode: "cors",
    headers: {
      Authentication: "john.doe@email.com",
    },
  })
    .then(result => result.json())
    .then(resultJson => {
      let one = 1;
    })
}

const element = <CourseListComponent></CourseListComponent>;
ReactDOM.render(element, document.getElementById("root"));

function CourseListComponent() {
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);

  useEffect(() => {
    const abortCtrl = new AbortController();
    const opts = { signal: abortCtrl.signal };
    fetch(url, {
      opts,
      method: "get",
      mode: "cors",
      headers: {
        Authentication: "john.doe@email.com",
      },
    })
      .then(result => result.json())
      .then(
        result => {
          setIsLoaded(true);
          setItems(result);
        },
        error => {
          setIsLoaded(true);
          setError(error);
        }
      );
    return () => abortCtrl.abort();
  }, [isLoaded])

  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading...</div>
  } else {
    return (
      <ul>
        {items.map(item => (
          <li key={item.id}>
            {item.name} text
          </li>
        ))}
      </ul>
    );
  }
}

export default App;
