-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.2-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for learn_app
CREATE DATABASE IF NOT EXISTS `learn_app` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `learn_app`;

-- Dumping structure for table learn_app.access_types
CREATE TABLE IF NOT EXISTS `access_types` (
  `access_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`access_id`),
  UNIQUE KEY `access_types_access_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table learn_app.accounts
CREATE TABLE IF NOT EXISTS `accounts` (
  `account_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_type_id` int(11) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(300) NOT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT 0,
  `phone_number` varchar(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`account_id`),
  UNIQUE KEY `accounts_email_uindex` (`email`),
  KEY `accounts_account_types_fk` (`account_type_id`),
  CONSTRAINT `accounts_account_types_fk` FOREIGN KEY (`account_type_id`) REFERENCES `account_types` (`account_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table learn_app.accounts_access_types
CREATE TABLE IF NOT EXISTS `accounts_access_types` (
  `account_id` int(11) NOT NULL,
  `access_id` int(11) NOT NULL,
  KEY `accounts_access_types_accounts_fk` (`account_id`),
  KEY `accounts_access_types_access_types_fk` (`access_id`),
  CONSTRAINT `accounts_access_types_access_types_fk` FOREIGN KEY (`access_id`) REFERENCES `access_types` (`access_id`),
  CONSTRAINT `accounts_access_types_accounts_fk` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table learn_app.account_emails
CREATE TABLE IF NOT EXISTS `account_emails` (
  `email_id` int(11) NOT NULL AUTO_INCREMENT,
  `verified` tinyint(1) NOT NULL DEFAULT 0,
  `email_address` varchar(100) NOT NULL,
  PRIMARY KEY (`email_id`),
  UNIQUE KEY `account_emails_email_address_uindex` (`email_address`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table learn_app.account_types
CREATE TABLE IF NOT EXISTS `account_types` (
  `account_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  PRIMARY KEY (`account_type_id`),
  UNIQUE KEY `account_types_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table learn_app.administration_requests
CREATE TABLE IF NOT EXISTS `administration_requests` (
  `request_id` int(11) NOT NULL AUTO_INCREMENT,
  `request_type_id` int(11) NOT NULL,
  `submission_account_id` int(11) NOT NULL,
  `creation_date` datetime NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT 3,
  PRIMARY KEY (`request_id`),
  KEY `administration_requests_submission_accounts_fk` (`submission_account_id`),
  KEY `administration_requests_administration_request_types_fk` (`request_type_id`),
  KEY `administration_requests_administration_request_statuses_fk` (`status_id`),
  CONSTRAINT `administration_requests_administration_request_statuses_fk` FOREIGN KEY (`status_id`) REFERENCES `administration_request_statuses` (`status_id`),
  CONSTRAINT `administration_requests_administration_request_types_fk` FOREIGN KEY (`request_type_id`) REFERENCES `administration_request_types` (`request_type_id`),
  CONSTRAINT `administration_requests_submission_accounts_fk` FOREIGN KEY (`submission_account_id`) REFERENCES `accounts` (`account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table learn_app.administration_request_statuses
CREATE TABLE IF NOT EXISTS `administration_request_statuses` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`status_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table learn_app.administration_request_types
CREATE TABLE IF NOT EXISTS `administration_request_types` (
  `request_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`request_type_id`),
  UNIQUE KEY `administration_request_types_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table learn_app.assignments
CREATE TABLE IF NOT EXISTS `assignments` (
  `assignment_id` int(11) NOT NULL AUTO_INCREMENT,
  `lecture_id` int(11) NOT NULL DEFAULT 0,
  `owner_account_id` int(11) NOT NULL DEFAULT 0,
  `grade` int(11) DEFAULT NULL,
  PRIMARY KEY (`assignment_id`),
  KEY `lecture_id` (`lecture_id`),
  KEY `Column 3` (`owner_account_id`),
  CONSTRAINT `assignments_accounts_fk` FOREIGN KEY (`owner_account_id`) REFERENCES `accounts` (`account_id`),
  CONSTRAINT `assignments_lectures_fk` FOREIGN KEY (`lecture_id`) REFERENCES `lectures` (`lecture_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table learn_app.courses
CREATE TABLE IF NOT EXISTS `courses` (
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_account_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `passing_grade` int(11) NOT NULL,
  `starting_date` date NOT NULL,
  `rating` float NOT NULL DEFAULT 0,
  PRIMARY KEY (`course_id`),
  UNIQUE KEY `courses_title_uindex` (`title`),
  KEY `courses_course_topics_fk` (`topic_id`),
  KEY `courses_course_statuses_fk` (`status_id`),
  KEY `courses_accounts_fk` (`owner_account_id`),
  CONSTRAINT `courses_accounts_fk` FOREIGN KEY (`owner_account_id`) REFERENCES `accounts` (`account_id`),
  CONSTRAINT `courses_course_statuses_fk` FOREIGN KEY (`status_id`) REFERENCES `course_statuses` (`status_id`),
  CONSTRAINT `courses_course_topics_fk` FOREIGN KEY (`topic_id`) REFERENCES `course_topics` (`topic_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table learn_app.courses_accounts
CREATE TABLE IF NOT EXISTS `courses_accounts` (
  `course_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  KEY `courses_accounts_accounts_fk` (`account_id`),
  KEY `courses_accounts_courses_fk` (`course_id`),
  CONSTRAINT `courses_accounts_accounts_fk` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`account_id`),
  CONSTRAINT `courses_accounts_courses_fk` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table learn_app.courses_ratings
CREATE TABLE IF NOT EXISTS `courses_ratings` (
  `course_rating_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `rating_id` int(11) NOT NULL,
  PRIMARY KEY (`course_rating_id`),
  KEY `courses_ratings_accounts_fk` (`account_id`),
  KEY `courses_ratings_courses_fk` (`course_id`),
  KEY `courses_ratings_ratings_fk` (`rating_id`),
  CONSTRAINT `courses_ratings_accounts_fk` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`account_id`),
  CONSTRAINT `courses_ratings_courses_fk` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`),
  CONSTRAINT `courses_ratings_ratings_fk` FOREIGN KEY (`rating_id`) REFERENCES `course_ratings` (`rating_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table learn_app.course_descriptions
CREATE TABLE IF NOT EXISTS `course_descriptions` (
  `description_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `description_content` varchar(1000) NOT NULL,
  PRIMARY KEY (`description_id`),
  KEY `course_descriptions_courses_fk` (`course_id`),
  CONSTRAINT `course_descriptions_courses_fk` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table learn_app.course_ratings
CREATE TABLE IF NOT EXISTS `course_ratings` (
  `rating_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`rating_id`),
  UNIQUE KEY `course_ratings_rating_name_uindex` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table learn_app.course_statuses
CREATE TABLE IF NOT EXISTS `course_statuses` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`status_id`),
  UNIQUE KEY `course_statuses_status_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table learn_app.course_topics
CREATE TABLE IF NOT EXISTS `course_topics` (
  `topic_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`topic_id`),
  UNIQUE KEY `course_topics_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table learn_app.lectures
CREATE TABLE IF NOT EXISTS `lectures` (
  `lecture_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `video_link` varchar(255) NOT NULL,
  `max_grade` int(11) NOT NULL,
  PRIMARY KEY (`lecture_id`),
  KEY `lectures_courses_fk` (`course_id`),
  CONSTRAINT `lectures_courses_fk` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table learn_app.lectures_next_lectures
CREATE TABLE IF NOT EXISTS `lectures_next_lectures` (
  `lecture_id` int(11) NOT NULL,
  `next_lecture_id` int(11) NOT NULL,
  KEY `lectures_next_lectures_lectures_fk` (`lecture_id`),
  KEY `lectures_next_lectures_next_lectures_fk` (`next_lecture_id`),
  CONSTRAINT `lectures_next_lectures_lectures_fk` FOREIGN KEY (`lecture_id`) REFERENCES `lectures` (`lecture_id`),
  CONSTRAINT `lectures_next_lectures_next_lectures_fk` FOREIGN KEY (`next_lecture_id`) REFERENCES `lectures` (`lecture_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table learn_app.lecture_descriptions
CREATE TABLE IF NOT EXISTS `lecture_descriptions` (
  `description_id` int(11) NOT NULL AUTO_INCREMENT,
  `lecture_id` int(11) NOT NULL,
  `description_content` varchar(1000) NOT NULL,
  PRIMARY KEY (`description_id`),
  KEY `lecture_descriptions_lectures_fk` (`lecture_id`),
  CONSTRAINT `lecture_descriptions_lectures_fk` FOREIGN KEY (`lecture_id`) REFERENCES `lectures` (`lecture_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table learn_app.verification_tokens
CREATE TABLE IF NOT EXISTS `verification_tokens` (
  `token_id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(500) NOT NULL,
  `account_id` int(11) NOT NULL,
  `expiration_time` date NOT NULL,
  PRIMARY KEY (`token_id`),
  UNIQUE KEY `value` (`value`),
  KEY `verification_tokens_accounts_fk` (`account_id`),
  CONSTRAINT `verification_tokens_accounts_fk` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
