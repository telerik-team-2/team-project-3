-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.2-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping data for table learn_app.access_types: ~3 rows (approximately)
/*!40000 ALTER TABLE `access_types` DISABLE KEYS */;
INSERT INTO `access_types` (`access_id`, `name`) VALUES
	(3, 'admin'),
	(1, 'student'),
	(2, 'teacher');
/*!40000 ALTER TABLE `access_types` ENABLE KEYS */;

-- Dumping data for table learn_app.accounts: ~19 rows (approximately)
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` (`account_id`, `account_type_id`, `first_name`, `last_name`, `email`, `password`, `verified`, `phone_number`) VALUES
	(1, 3, 'John', 'Doe', 'john.doe@email.com', 'nUw5l+EDhuf2c7NkjliXbv/vG0zgCpPo0dfi+teZoqE=', 1, '0008256990'),
	(2, 2, 'Alex', 'Bloom', 'alex.bloom@email.com', 'N36TIBWoKieHsTymLgXsinAbXAZmgpqrLR2q4ItxZqs=', 0, '0'),
	(3, 1, 'David', 'Green', 'david.green@email.com', 'temp', 0, '0'),
	(6, 1, 'James', 'Moore', 'james.moore@email.com', 'temp', 0, '0'),
	(33, 1, 'Harold', 'Davidson', 'harold.davidson@email.com', 'temp', 0, '0'),
	(34, 1, 'Milan', 'Oneil', 'milan.oneil@email.com', 'temp', 0, '0'),
	(36, 2, 'Cody', 'Gale', 'cody.gale@email.com', 'temp', 0, '0'),
	(37, 1, 'Corrina', 'Leblanc', 'corrina.leblanc@email.com', 'temp', 0, '006899329'),
	(38, 1, 'Paddy', 'Hutton', 'paddy.hutton@learnapp01.com', 'temp', 0, '0068236'),
	(39, 1, 'Drake', 'Marsh', 'drake.marsh.lkmc93yXxzm#@learnapp01.com', 'ScBuuUKdlkoF0mgShbMExlzBMHce5uG9zqFS9l8/i3k=', 1, '00686278----'),
	(40, 1, 'Ezra', 'Calogero', 'ezra.calogero@learnapp01.com', 'ScBuuUKdlkoF0mgShbMExlzBMHce5uG9zqFS9l8/i3k=', 1, '008574578'),
	(41, 1, 'Jarred', 'Lu', 'jarred.lu@learnapp01.com', 'ScBuuUKdlkoF0mgShbMExlzBMHce5uG9zqFS9l8/i3k=', 1, '09738976276'),
	(42, 2, 'Shayaan', 'Bowman', 'shayaan.bowman@learnapp01.com', 'ScBuuUKdlkoF0mgShbMExlzBMHce5uG9zqFS9l8/i3k=', 1, '038759219'),
	(43, 1, 'Ronnie   ', 'Wheeler   ', 'Ronnie.Wheeler@learnapp01.com', 'ScBuuUKdlkoF0mgShbMExlzBMHce5uG9zqFS9l8/i3k=', 0, '0976586543'),
	(44, 1, 'Ronnie', 'Wheeler', 'Ronnie.Wheeler1@learnapp01.com', 'ScBuuUKdlkoF0mgShbMExlzBMHce5uG9zqFS9l8/i3k=', 1, '0976586543'),
	(45, 1, 'Pauline', 'Davis', 'test.gbht2000@gmail.com', 'ScBuuUKdlkoF0mgShbMExlzBMHce5uG9zqFS9l8/i3k=', 1, '09763768987'),
	(46, 2, 'Valerie', 'Brooks', 'valerie.brooks@learnapp01.com', 'ScBuuUKdlkoF0mgShbMExlzBMHce5uG9zqFS9l8/i3k=', 1, '03752563'),
	(47, 2, 'Paul', 'Baker', 'test.gbht200@gmail.com', 'ScBuuUKdlkoF0mgShbMExlzBMHce5uG9zqFS9l8/i3k=', 1, '08653843'),
	(48, 2, 'Ethel', 'Ortiz', 'test.gbht20@gmail.com', 'ScBuuUKdlkoF0mgShbMExlzBMHce5uG9zqFS9l8/i3k=', 1, '0865384385');
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;

-- Dumping data for table learn_app.accounts_access_types: ~26 rows (approximately)
/*!40000 ALTER TABLE `accounts_access_types` DISABLE KEYS */;
INSERT INTO `accounts_access_types` (`account_id`, `access_id`) VALUES
	(2, 1),
	(1, 2),
	(3, 1),
	(6, 1),
	(33, 1),
	(2, 2),
	(34, 1),
	(36, 1),
	(37, 1),
	(38, 1),
	(39, 1),
	(40, 1),
	(36, 2),
	(41, 1),
	(42, 1),
	(1, 3),
	(1, 1),
	(42, 2),
	(43, 1),
	(44, 1),
	(45, 1),
	(46, 1),
	(46, 2),
	(47, 1),
	(48, 1),
	(48, 2);
/*!40000 ALTER TABLE `accounts_access_types` ENABLE KEYS */;

-- Dumping data for table learn_app.account_emails: ~0 rows (approximately)
/*!40000 ALTER TABLE `account_emails` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_emails` ENABLE KEYS */;

-- Dumping data for table learn_app.account_types: ~3 rows (approximately)
/*!40000 ALTER TABLE `account_types` DISABLE KEYS */;
INSERT INTO `account_types` (`account_type_id`, `name`) VALUES
	(3, 'admin'),
	(1, 'student'),
	(2, 'teacher');
/*!40000 ALTER TABLE `account_types` ENABLE KEYS */;

-- Dumping data for table learn_app.administration_requests: ~6 rows (approximately)
/*!40000 ALTER TABLE `administration_requests` DISABLE KEYS */;
INSERT INTO `administration_requests` (`request_id`, `request_type_id`, `submission_account_id`, `creation_date`, `status_id`) VALUES
	(4, 1, 2, '2021-09-17 12:08:55', 2),
	(5, 1, 1, '2021-10-01 12:42:05', 1),
	(6, 1, 42, '2021-10-01 17:37:44', 2),
	(7, 1, 46, '2021-10-07 15:02:45', 2),
	(8, 1, 47, '2021-10-07 19:52:18', 3),
	(9, 1, 48, '2021-10-07 19:53:32', 2);
/*!40000 ALTER TABLE `administration_requests` ENABLE KEYS */;

-- Dumping data for table learn_app.administration_request_statuses: ~2 rows (approximately)
/*!40000 ALTER TABLE `administration_request_statuses` DISABLE KEYS */;
INSERT INTO `administration_request_statuses` (`status_id`, `name`) VALUES
	(2, 'Approved'),
	(1, 'Denied'),
	(3, 'Waiting');
/*!40000 ALTER TABLE `administration_request_statuses` ENABLE KEYS */;

-- Dumping data for table learn_app.administration_request_types: ~2 rows (approximately)
/*!40000 ALTER TABLE `administration_request_types` DISABLE KEYS */;
INSERT INTO `administration_request_types` (`request_type_id`, `name`) VALUES
	(2, 'ADMIN_APPROVAL'),
	(1, 'TEACHER_APPROVAL');
/*!40000 ALTER TABLE `administration_request_types` ENABLE KEYS */;

-- Dumping data for table learn_app.assignments: ~10 rows (approximately)
/*!40000 ALTER TABLE `assignments` DISABLE KEYS */;
INSERT INTO `assignments` (`assignment_id`, `lecture_id`, `owner_account_id`, `grade`) VALUES
	(7, 1, 1, 10),
	(8, 2, 1, 10),
	(9, 4, 1, NULL),
	(10, 2, 39, NULL),
	(11, 1, 39, 7),
	(12, 2, 45, 10),
	(13, 1, 45, 11),
	(14, 4, 45, NULL),
	(15, 1, 48, 20),
	(16, 2, 48, 20);
/*!40000 ALTER TABLE `assignments` ENABLE KEYS */;

-- Dumping data for table learn_app.courses: ~6 rows (approximately)
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` (`course_id`, `owner_account_id`, `title`, `topic_id`, `status_id`, `passing_grade`, `starting_date`, `rating`) VALUES
	(1, 2, 'Biology course', 1, 2, 20, '2021-09-11', 0),
	(2, 36, 'World History', 2, 2, 25, '2021-09-16', 0),
	(3, 36, 'Advanced Literature', 2, 2, 20, '2021-09-16', 0),
	(4, 2, 'Computer Science', 4, 1, 20, '2021-09-12', 0),
	(5, 42, 'Java Fundamentals', 4, 2, 10, '2021-11-07', 0),
	(6, 42, 'Python Fundamentals', 4, 1, 20, '2021-10-21', 0);
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;

-- Dumping data for table learn_app.courses_accounts: ~7 rows (approximately)
/*!40000 ALTER TABLE `courses_accounts` DISABLE KEYS */;
INSERT INTO `courses_accounts` (`course_id`, `account_id`) VALUES
	(1, 1),
	(4, 1),
	(1, 39),
	(1, 45),
	(3, 45),
	(4, 45),
	(1, 48);
/*!40000 ALTER TABLE `courses_accounts` ENABLE KEYS */;

-- Dumping data for table learn_app.courses_ratings: ~3 rows (approximately)
/*!40000 ALTER TABLE `courses_ratings` DISABLE KEYS */;
INSERT INTO `courses_ratings` (`course_rating_id`, `course_id`, `account_id`, `rating_id`) VALUES
	(1, 1, 1, 2),
	(2, 1, 45, 3),
	(3, 1, 48, 4);
/*!40000 ALTER TABLE `courses_ratings` ENABLE KEYS */;

-- Dumping data for table learn_app.course_descriptions: ~6 rows (approximately)
/*!40000 ALTER TABLE `course_descriptions` DISABLE KEYS */;
INSERT INTO `course_descriptions` (`description_id`, `course_id`, `description_content`) VALUES
	(1, 1, 'Study of fundamental concepts of biology that relate to problems confronting individuals and society. Topics include the scientific method; human dependence on other organisms; problems related to food production, human reproduction, inheritance and disease; biological implications of modern technology; and biological problems likely to confront society in the future.'),
	(2, 3, 'Learn how to understand and evaluate works of fiction, poetry, and drama from various periods and cultures. You’ll read literary works and write essays to explain and support your analysis of them.'),
	(3, 5, 'Java is a high-level, class-based, object-oriented programming language that is designed to have as few implementation dependencies as possible. It is a general-purpose programming language intended to let programmers write once, run anywhere (WORA), meaning that compiled Java code can run on all platforms that support Java without the need for recompilation. Java applications are typically compiled to bytecode that can run on any Java virtual machine (JVM) regardless of the underlying computer architecture. The syntax of Java is similar to C and C++, but has fewer low-level facilities than either of them. The Java runtime provides dynamic capabilities (such as reflection and runtime code modification) that are typically not available in traditional compiled languages. As of 2019, Java was one of the most popular programming languages in use according to GitHub, particularly for client-server web applications, with a reported 9 million developers.'),
	(4, 2, 'Learn history with online classes and courses covering a variety of subjects from ancient to modern. History education topics include world wars, famous historical events, the Middle East, Middle Ages, the Roman Empire, American History and more. Start learning history today!'),
	(5, 6, 'Python is an interpreted high-level general-purpose programming language. Its design philosophy emphasizes code readability with its use of significant indentation. Its language constructs as well as its object-oriented approach aim to help programmers write clear, logical code for small and large-scale projects.'),
	(6, 4, 'Computer science is the study of algorithmic processes, computational machines and computation itself. As a discipline, computer science spans a range of topics from theoretical studies of algorithms, computation and information to the practical issues of implementing computational systems in hardware and software.');
/*!40000 ALTER TABLE `course_descriptions` ENABLE KEYS */;

-- Dumping data for table learn_app.course_ratings: ~5 rows (approximately)
/*!40000 ALTER TABLE `course_ratings` DISABLE KEYS */;
INSERT INTO `course_ratings` (`rating_id`, `name`) VALUES
	(3, 'average'),
	(2, 'bad'),
	(4, 'good'),
	(1, 'very bad'),
	(5, 'very good');
/*!40000 ALTER TABLE `course_ratings` ENABLE KEYS */;

-- Dumping data for table learn_app.course_statuses: ~2 rows (approximately)
/*!40000 ALTER TABLE `course_statuses` DISABLE KEYS */;
INSERT INTO `course_statuses` (`status_id`, `name`) VALUES
	(1, 'draft'),
	(2, 'published');
/*!40000 ALTER TABLE `course_statuses` ENABLE KEYS */;

-- Dumping data for table learn_app.course_topics: ~4 rows (approximately)
/*!40000 ALTER TABLE `course_topics` DISABLE KEYS */;
INSERT INTO `course_topics` (`topic_id`, `name`) VALUES
	(4, 'computer science'),
	(2, 'history'),
	(3, 'literature'),
	(1, 'science');
/*!40000 ALTER TABLE `course_topics` ENABLE KEYS */;

-- Dumping data for table learn_app.lectures: ~8 rows (approximately)
/*!40000 ALTER TABLE `lectures` DISABLE KEYS */;
INSERT INTO `lectures` (`lecture_id`, `course_id`, `title`, `video_link`, `max_grade`) VALUES
	(1, 1, 'Biology 1', 'mG4NLNZ37y4', 20),
	(2, 1, 'Biology 2', 'temp', 20),
	(4, 4, 'Lecture 1', 'ReVeUvwTGdU', 40),
	(5, 5, 'Java core', 'mG4NLNZ37y4', 10),
	(6, 6, 'Functions 1', 'jjPQ4KuGS_U', 25),
	(7, 2, 'The Middle East', 'Yocja_N5s1I', 20),
	(8, 2, 'The Roman Empire', 'oPf27gAup9U', 20),
	(9, 2, 'American History', 'kquAIJg8vRE', 20);
/*!40000 ALTER TABLE `lectures` ENABLE KEYS */;

-- Dumping data for table learn_app.lectures_next_lectures: ~0 rows (approximately)
/*!40000 ALTER TABLE `lectures_next_lectures` DISABLE KEYS */;
/*!40000 ALTER TABLE `lectures_next_lectures` ENABLE KEYS */;

-- Dumping data for table learn_app.lecture_descriptions: ~8 rows (approximately)
/*!40000 ALTER TABLE `lecture_descriptions` DISABLE KEYS */;
INSERT INTO `lecture_descriptions` (`description_id`, `lecture_id`, `description_content`) VALUES
	(1, 2, 'Second Biology lecture.'),
	(3, 1, 'First Biology lecture.'),
	(4, 5, 'Java was originally developed by James Gosling at Sun Microsystems (which has since been acquired by Oracle) and released in 1995 as a core component of Sun Microsystems\' Java platform. The original and reference implementation Java compilers, virtual machines, and class libraries were originally released by Sun under proprietary licenses. As of May 2007, in compliance with the specifications of the Java Community Process, Sun had relicensed most of its Java technologies under the GPL-2.0-only license. Oracle offers its own HotSpot Java Virtual Machine, however the official reference implementation is the OpenJDK JVM which is free open-source software and used by most developers and is the default JVM for almost all Linux distributions.'),
	(5, 7, 'The Middle East is a geopolitical term that commonly refers to the region spanning the Levant, Arabian Peninsula, Anatolia (including modern Turkey and Cyprus), Egypt, Iran and Iraq. The term came into widespread usage as a replacement of the term Near East (as opposed to the Far East) beginning in the early 20th century. The term "Middle East" has led to some confusion over its changing definitions. The region includes the vast majority of the territories included in the closely associated definition of Western Asia, but without the Caucasus and including all of Egypt, and not just the Sinai Peninsula.'),
	(6, 8, 'The Roman Empire was the post-Republican period of ancient Rome. As a polity it included large territorial holdings around the Mediterranean Sea in Europe, Northern Africa, and Western Asia ruled by emperors. From the accession of Caesar Augustus to the military anarchy of the 3rd century, it was a principate with Italy as metropole of the provinces and the city of Rome as sole capital (27 BC – AD 286). After the military crisis, the empire was ruled by multiple emperors who shared rule over the Western Roman Empire and over the Eastern Roman Empire (also known as the Byzantine Empire).'),
	(7, 9, 'The history of the United States was preceded by the arrival of Native Americans in North America around 15,000 BC. Numerous indigenous cultures formed, and many disappeared in the 16th century. The arrival of Christopher Columbus in 1492 started the European colonization of the Americas. Most colonies were formed after 1600, and the United States was the first nation whose most distant origins are fully recorded. By the 1760s, the thirteen British colonies contained 2.5 million people along the Atlantic Coast east of the Appalachian Mountains. After defeating France, the British government imposed a series of taxes, including the Stamp Act of 1765, rejecting the colonists\' constitutional argument that new taxes needed their approval. Resistance to these taxes, especially the Boston Tea Party in 1773, led to Parliament issuing punitive laws designed to end self-government. Armed conflict began in Massachusetts in 1775.'),
	(8, 6, 'Python is a multi-paradigm programming language. Object-oriented programming and structured programming are fully supported, and many of its features support functional programming and aspect-oriented programming (including by metaprogramming and metaobjects (magic methods)). Many other paradigms are supported via extensions, including design by contract and logic programming.'),
	(9, 4, 'The earliest foundations of what would become computer science predate the invention of the modern digital computer. Machines for calculating fixed numerical tasks such as the abacus have existed since antiquity, aiding in computations such as multiplication and division. Algorithms for performing computations have existed since antiquity, even before the development of sophisticated computing equipment.');
/*!40000 ALTER TABLE `lecture_descriptions` ENABLE KEYS */;

-- Dumping data for table learn_app.verification_tokens: ~15 rows (approximately)
/*!40000 ALTER TABLE `verification_tokens` DISABLE KEYS */;
INSERT INTO `verification_tokens` (`token_id`, `value`, `account_id`, `expiration_time`) VALUES
	(24, '58aea899-a588-4c3a-831f-1c1fea958ab8', 33, '2021-09-19'),
	(25, '27ade7dc-6b84-4c77-b5f1-6a72ea279a65', 34, '2021-09-24'),
	(26, '27e5d7b8-5573-4b2d-84d7-919decc8e6da', 36, '2021-09-25'),
	(27, '77d0e8d8-7b4d-40fd-93e4-881ea5653502', 37, '2021-09-25'),
	(28, '68d63603-d461-4aca-a76b-94a148feb6d0', 38, '2021-09-25'),
	(29, '8befebe3-4676-4b13-82dd-8e15081e0b4b', 39, '2021-09-25'),
	(30, '2438a49c-5c36-4e95-adb1-5b36a7c30057', 40, '2021-10-01'),
	(31, '9442ce8e-0d06-4c40-aaa1-f085db771c73', 41, '2021-10-02'),
	(32, '780c7df0-2321-4f49-a653-6c95a4a8e374', 42, '2021-10-02'),
	(33, 'e48e9870-df9e-415d-b7a5-19b8b971072a', 43, '2021-10-08'),
	(34, 'c9a777c8-9c43-4efd-a7c5-c24891002ff6', 44, '2021-10-08'),
	(35, 'e0546638-d0f9-45ea-8f3e-8bfb009d6c0a', 45, '2021-10-08'),
	(36, 'b8eca206-a3fc-4d46-b0aa-d423004d60c6', 46, '2021-10-08'),
	(37, 'f13e04e2-a508-4edd-9e0d-8f6a7be14d56', 47, '2021-10-08'),
	(38, '32abc413-6682-4b23-ab5c-b0ad6cae5590', 48, '2021-10-08');
/*!40000 ALTER TABLE `verification_tokens` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
