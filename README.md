<h1>Virtual Teacher</h1>
<br>
Virtual Teacher is an online platform for tutoring. Users will be able to register as either a teacher or a student.

Students must be able to enroll for video courses, watch the available video lectures. After finishing each video within a course, students will be asked to submit an assignment, which will be graded by the teacher. After a successful completion of a course, the students must be able to rate it.

Teachers should be able to create courses and upload video lectures with assignments. Each assignment will be graded individually after submission from the students. Users can become teachers only after approval from administrator.
<br>
<p>Swagger documentation: http://localhost:8080/swagger-ui/index.html#/</p>

